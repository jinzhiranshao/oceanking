const app = getApp();
const pageS = require('../../utils/pageS.js');

Page(Object.assign({}, pageS, {
    _isPageCache: false,
    data: Object.assign({},  {
        apiAssets: app.config.apiAssets
    }),
    onReady: function (refresh = false) {
        var token = this._options.token;
        var that = this;
        app.Util.network.GET(that, {
            url: 'https://api.xunhupay.com/payment/wechat/token.html',
            params: {
                token:token
            },
            showLoading: 0,
            success: function (data) {
                if(data.is_paid){
                    wx.navigateBackMiniProgram({
                        extraData:{
                            hpj_jsapi_order_id: data.order_id,
                            hpj_jsapi_is_paid:true,
                            hpj_jsapi_token: token
                        }
                    });
                    return;
                }

                that.setData(data);
            }
        });
    },
    onConfirm(){
        var that = this;
        wx.requestPayment({
            timeStamp: that.data.jsapi.timeStamp.toString(),
            nonceStr: that.data.jsapi.nonceStr.toString(),
            package: that.data.jsapi.package,
            signType: that.data.jsapi.signType,
            paySign: that.data.jsapi.paySign,
            success() {
                wx.navigateBackMiniProgram({
                    extraData: {
                        hpj_jsapi_order_id: that.data.order_id,
                        hpj_jsapi_is_paid: true,
                        hpj_jsapi_token: that._options.token
                    }
                });
            },
            fail(response) {
                if (response.errMsg == 'requestPayment:fail cancel') {
                    wx.showModal({
                        title: '提示',
                        content: '您取消了支付！',
                        showCancel:false
                    });
                } else if (response.errMsg == 'requestPayment:fail') {
                    var msg = '支付失败:' + JSON.stringify(response);
                    if (response.err_desc) {
                        msg = '支付失败：' + response.err_desc;
                    }
                    wx.showModal({
                        title: '提示',
                        content: msg,
                        showCancel: false
                    });
                } else {
                    wx.showModal({
                        title: '提示',
                        content: '支付失败：未知错误！',
                        showCancel: false
                    });
                }

                wx.startPullDownRefresh({});
            }
        });
    },
    onCancel(){
        var that = this;
        wx.showModal({
            title: '提示',
            content: '确认要取消支付吗？',
            success(res) {
                if (res.confirm) {
                    wx.navigateBackMiniProgram({
                        extraData: {
                            hpj_jsapi_order_id: that.data.order_id,
                            hpj_jsapi_is_paid: false,
                            hpj_jsapi_token: that._options.token
                        }
                    });
                }
            }
        });
    }
}));