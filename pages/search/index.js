const app = getApp();
const pageS = require('../../utils/pageS.js');
const prolist = require('../../utils/apis/pro-list.js');

Page(Object.assign({}, pageS, {
    _cache_key: 'data:keywords:history',
    data:  {
        icon_loading_small: app.config.icons.icon_loading_small,
        apiAssets: app.config.apiAssets,
        keywordValue: null,
        inputShowClear: false,
        keywordLabel: null,
        pageState: 'hot'
    },
    onReady(refresh=false){
        var that = this;
        prolist.property = "query";
        prolist.prepareLoad = this.prepareLoad;
        var that = this;
        that._onLoad('wp/v1/store/search', that._options, refresh, function (that, data) {
            data.inputFocus = typeof that._options.focus != 'undefined' ? true : false;
            that.setData(data);
        });

        wx.getStorage({
            key: that._cache_key,
            success: function (res) {
                that.setData({
                    historyLabels: res.data
                });
            },
        });
        
        wx.createSelectorQuery().selectAll('#search-bar').boundingClientRect().exec(function (res) {
            var heightSearchbar = 0;

            if (res && res.length > 0 && res[0]) {

                for (var i in res[0]) {
                    var dom = res[0][i];
                    switch (dom.id) {
                        case 'search-bar':
                            heightSearchbar = dom.height;
                            break;
                    }
                }
            }

            that.setData({
                scrollTop: heightSearchbar
            });
        });
    },
    prepareLoad: function (options) {
        if (this.data.keywordValue && this.data.keywordValue.length >= 2) {
            options.s = this.data.keywordValue;
        } else {
            options.s = null;
        }
    },
    onSearchInput: function (e) {
        var keywords = e.detail.value;

        if (!keywords || keywords.length < 2) {
            this.setData({
                keywordValue: keywords,
                suggest: null,
                pageState: 'hot',
                inputShowClear: false
            });
            return;
        } 

        this.setData({
            keywordValue: keywords,
            inputShowClear: true
        });
        
        var that = this;
        if (that.data._isLoadingData) {
            return;
        }

        if (that.__lastSearchTask) {
            clearTimeout(that.__lastSearchTask);
        }
        that.__lastSearchTask = setTimeout(function () {
            that.setData({
                _isLoadingData: true
            });
            app.Util.network.POST(that,{
                url: app.getApi('wp/v1/store/search/data'),
                final: function () {
                    that.setData({
                        _isLoadingData: false
                    });
                },
                params: {
                    // keyword: keywords
                },
                showLoading: 0,
                success: function (data) {
                    if (data.items && data.items.length > 0) {
                        that.setData({
                            pageState: 'suggest',
                            suggest: data.items
                        });
                    }
                }
            });
        }, 800);

    },
    onDeleteKeyClick: function (e) {
        this.setData({
            keywordValue: '',
            pageState: 'hot',
            suggest: null,
            inputShowClear: false
        });
    },
    onSearchSubmit: function (e) {
        if (!e.detail.value.keywords || this.data.keywordValue.length < 2){
            return;
        }

        this.setData({
            pageState: 'search',
            inputShowClear: true,
            suggest: null
        });
        this.__onSearchClick(e.detail.value.keywords);
    },
    onSearchClick: function () {
        if (!this.data.keywordValue || this.data.keywordValue.length < 2){
            return;
        }
        
        this.setData({
            pageState: 'search',
            inputShowClear: true,
            suggest: null
        });
        this.__onSearchClick(this.data.keywordValue);
    },
    onFooterChange:function(e){
        prolist.onFooterChange(this,e);
    },
    __onSearchClick: function (keywords) {
        if(keywords){
            keywords = keywords.trim();
        }
        if (!keywords || keywords.length < 2) {
            return;
        }
        var historyLabels = this.data.historyLabels;
        if (!historyLabels) {
            historyLabels = [];
        }

        var has = false;
        for (var index in historyLabels) {
            if (historyLabels[index].label == keywords) {
                has = true;
                break;
            }
        }

        if (!has) {
            var newHistory = [];
            newHistory.push({
                id: 0,
                label: keywords
            });
            for (var index = 0; index < historyLabels.length && index < 19; index++) {
                newHistory.push({
                    id: index + 1,
                    label: historyLabels[index].label
                });
            }
            this.setData({
                historyLabels: newHistory
            });
            wx.setStorage({
                key: this._cache_key,
                data: newHistory
            });
        }

        prolist.onLoad(this);
    },
    onReachBottom(){
        prolist.onReachBottom(this);
    },
    onPullDownRefresh() {
        prolist.onPullDownRefresh(this);
    },
    onHistoryClear: function (e) {
        this.setData({
            historyLabels: []
        });
        wx.setStorage({
            key: this._cache_key,
            data: []
        });
    },
    onKeywordsClick: function (e) {
        var keywords = e.currentTarget.dataset.label;
        if (!keywords || keywords.length < 2) {
            return;
        }
        this.setData({
            keywordValue: keywords.trim(),
            pageState: 'search',
            inputShowClear: true,
            suggest: null
        });

        this.__onSearchClick(keywords);
    }
}));