const app = getApp();
const pageT = require('../../utils/pageT.js');
const cartApi = require("../../utils/cart.js");
Page(Object.assign({}, pageT, {
    _requiredAuth:true,
    __onShow(){
        this._onLoad('wp/v1/store/account', true);
      var that = this;
      cartApi.get(that, function (data) {
        that.setData({
          cart: data,
          isDataLoading: false
        });
      }, true);
    },
    onPullDownRefresh(){
        this._onLoad('wp/v1/store/account', true);
    }
}));