const app = getApp();
const pageT = require('../../utils/pageT.js');

Page(Object.assign({}, pageT, {
    onLoad: function (options) {
        if (options && typeof options.id != 'undefined') {
            options.cat_id = options.id;
        }
        this._options = options;
    },
    onReady(refresh = false) {
        this._onLoad('wp/v1/store/category2',refresh);
    }
}));