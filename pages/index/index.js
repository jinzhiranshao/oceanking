const app = getApp();
const pageT = require('../../utils/pageT.js');
const cartApi = require("../../utils/cart.js");

//import {Hupijiao} from '../../utils/hpj.js';
//var hupijiao = new Hupijiao();


Page(Object.assign({}, pageT, {
    onReady(refresh = false) {
        this._onLoad('wp/v1/store/index', refresh);
    },
    __onShow: function () {
      var that = this;
      cartApi.get(that, function (data) {
        that.setData({
          cart: data,
          isDataLoading: false
        });
      }, true);
    },
}));