const app = getApp();
const cartApi = require("../../utils/cart.js");
const pageS = require('../../utils/pageS.js');

Page(Object.assign({}, pageS, {
    _cart_include_product: true,
    data: {
        isDataLoading: true,
        placeholderItemList: [0, 1, 2, 3],
        apiAssets: app.config.apiAssets,
        icon_none_cart_items: app.config.icons.icon_none_cart_items,
        icon_cart_cut: app.config.icons.icon_cart_cut,
        icon_cart_add: app.config.icons.icon_cart_add
    },
    onReady() {
        var that = this;
        app.getSystemInfo(function(sys) {
            if (app.tmpCart && Object.keys(app.tmpCart).length) {
                sys.cart = app.tmpCart;
                sys.isDataLoading = false;
                that.setData(sys);
            }
        });
    },
    clearCart(){
        var that = this;
        wx.showModal({
            content: '确定要清空购物车吗？',
            success(res) {
                if (res.confirm) {
                    cartApi.clear(that ,function (res) {
                        wx.startPullDownRefresh({});
                    });
                }
            }
        });
    },
    onPullDownRefresh: function() {
        this.__onShow();
    },
    __onShow: function() {
        var that = this;
        cartApi.get(that, function(data) {
            that.setData({
                cart: data,
                isDataLoading: false
            });
        }, true);
    },
    __getCartItemByKey: function(key) {
        if (!this.data.cart || !this.data.cart.items) {
            return null;
        }

        for (var id in this.data.cart.items) {
            if (this.data.cart.items[id].key == key) {
                return id;
            }
        }
        return null;
    },
    onQuantityAdd: function(e) {
        var itemkey = e.currentTarget.dataset.key;
        var cartItemId = this.__getCartItemByKey(itemkey);
        if (!cartItemId) {
            return;
        }
        var cartItem = this.data.cart.items[cartItemId];
        if (!cartItem) {
            return;
        }

        var quantity = cartItem.quantity;
        if (cartItem.product.max_qty !== '' && cartItem.product.max_qty >= 0 && quantity >= cartItem.product.max_qty) {
            return;
        }

        quantity++;
        if (quantity < cartItem.product.min_qty) {
            quantity = cartItem.product.min_qty;
        }
        if (cartItem.product.max_qty !== '' && cartItem.product.max_qty >= 0 && quantity > cartItem.product.max_qty) {
            quantity = cartItem.product.max_qty;
        }
        var that = this;
        cartApi.set_quantity(that, itemkey, quantity, function(res) {
            that.setData({
                cart: res
            });
        });
    },
    onQuantityCut: function(e) {
        var itemkey = e.currentTarget.dataset.key;
        var cartItemId = this.__getCartItemByKey(itemkey);
        if (!cartItemId) {
            return;
        }
        var cartItem = this.data.cart.items[cartItemId];
        if (!cartItem) {
            return;
        }

        var quantity = cartItem.quantity;
        if (quantity && quantity == 1) {
            return;
        }
        quantity--;
        if (quantity < cartItem.product.min_qty) {
            quantity = cartItem.product.min_qty;
        }
        if (cartItem.product.max_qty !== '' && cartItem.product.max_qty >= 0 && quantity > cartItem.product.max_qty) {
            quantity = cartItem.product.max_qty;
        }
        var that = this;
        cartApi.set_quantity(that, itemkey, quantity, function(res) {
            that.setData({
                cart: res
            });
        });
    },
    onQuantityChange: function(e) {
        var itemkey = e.currentTarget.dataset.key;
        var cartItemId = this.__getCartItemByKey(itemkey);
        if (!cartItemId) {
            return;
        }
        var quantity = e.detail.value;
        var cartItem = this.data.cart.items[cartItemId];
        if (!cartItem || quantity == '') {
            return;
        }

        var quantity = e.detail.value;
        if (quantity < cartItem.product.min_qty) {
            quantity = cartItem.product.min_qty;
        }

        if (cartItem.product.max_qty !== '' && cartItem.product.max_qty >= 0 && quantity > cartItem.product.max_qty) {
            quantity = cartItem.product.max_qty;
        }
        var that = this;
        cartApi.set_quantity(that, itemkey, quantity, function(res) {
            that.setData({
                cart: res
            });
        });
    },
    onRemoveCartItemClick: function(e) {
        var that = this;
        var itemkey = e.currentTarget.dataset.key;
        console.log(itemkey);
        var cartItemId = this.__getCartItemByKey(itemkey);
        if (!cartItemId) {
            return;
        }

        wx.showModal({
            content: '确定要删除此商品吗？',
            success(res) {
                if (res.confirm) {
                    cartApi.remove(that, itemkey, function(res) {
                        console.log(res);
                        that.setData({
                            cart: res
                        });
                    });
                }
            }
        });

    },
    __redirecting: false,
    onConfirmPay: function(e) {
        var formId = e.detail.formId;
        if (formId && !formId.startsWith('requestFormId:fail')) {
            app.formIds[formId] = Math.round((new Date()).getTime() / 1000);
        }

        var form = e.detail.value;
      
        var that = this;
        that.__stopOnShowEvent__ = true;
        app.Util.network.POST(that, {
            url: app.getApi('product/v1/checkout/pre'),
            params: form,
            showLoading: 1,
            success: data => {
                that.__nav_to__('navigate','/package_a/pages/checkout/index');
            },
            final: function () {
                that.__stopOnShowEvent__ = false;
            }
        });
    }
}))