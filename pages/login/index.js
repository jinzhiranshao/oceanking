const app = getApp();
const pageT = require('../../utils/pageT.js');
const navShow = require('../../components/nav/nav-show.js');
Page(Object.assign({}, pageT, {
    onReady(refresh = false) {
        this._onLoad('wp/v1/store/login', refresh);
    },
    __onPageScroll(e) {
        var that = this;
        navShow.onPageScroll(that, e.scrollTop);
    }
}));