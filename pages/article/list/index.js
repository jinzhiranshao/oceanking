const app = getApp();
const artlist = require('../templates/article-list.js');
const pageS = require('../../../utils/pageS.js');
Page(Object.assign({}, pageS, artlist, {
    _isPageCache: true,
    _selectedBarAni: null,
    data: {
        apiAssets: app.config.apiAssets,
        currentCate: null,
        page: 0,
        cart_index: 0,
        articleList: [],
        isLoadAll: false,
        isLoading: false
    },
    onReady(refresh=false) {
        this._selectedBarAni = wx.createAnimation({
            duration: 250,
            transformOrigin: "50% 50%",
            timingFunction: 'ease'
        });

        var that = this;
        var options = this._options;

        //加载分类
        that.getCate(options, (data) => {
            data.cart_index = 0;
            data.currentCate = data.catList && data.catList.length > 0 ? data.catList[data.cart_index].key : null;
            data.isLoading = true ;
            that.setData(data, function() {
                that.__onPostLoad();
            });
        }, refresh);
    },
    __onPostLoad: function() {
        var cate = this.data.currentCate;
        var params = {
            page: 1,
            cate: cate
        };
        this.onLoadByFirst(params);
    },
    /**
     * 分类导航改变当前显示分类
     */
    onChangeCate: function(e) {
        var index = e.currentTarget.dataset.index;
        var cat = this.data.catList ? this.data.catList[index] : null;
        if (!cat) {
            return;
        }

        this._selectedBarAni.translateX(e.currentTarget.offsetLeft + 'px').step();

        var that = this;
        this.setData({
            cat_name: cat.title,
            cart_index: index,
            cat_id: cat.key,
            currentCate: cat.key,
            current_cat_index: index,
            isLoadAll: false,
            isLoading: true,
            articleList: null,
            selectedBarAni: this._selectedBarAni.export()
        }, function() {
            var params = {
                page: 1,
                cate: cat.key
            };
            that.onLoadByFirst(params);
        });
    },

    /**
     * 加载分类
     */
    getCate: function(options, callback,refresh=false) {
        var that = this;
        if (!options) {
            options = {};
        }

        options.v = '1.0.1';
        that._onLoad('wp/v1/article/cate', options, refresh, function(that, data) {
            callback(data);
        });
    },

    /**
     * 模拟根据返回的不同的分类，加载对应数据
     */
    getDataByParams: function(params, callback) {
        var that = this;
        app.Util.network.POST(that,{
            url: app.getApi('wp/v1/article/list'),
            params: params,
            showLoading: 0,
            success: function(data) {
                callback(data);
            }
        });
    }
}))