const app = getApp();
const artlist = require('../templates/article-list.js');
const pageS = require('../../../utils/pageS.js');
Page(Object.assign({}, pageS,artlist, {
    data: {
        keywords: '',
        isPass: false,
        showDelKdIcon: false,
        hotTags: [],
        hisTags: [],
        isSearch: true,
        page: 0,
        articleList: [],
        isLoadAll: false,
        isLoading: false,
        apiAssets: app.config.apiAssets,
    },
    onReady(){
        var that = this;
        that.onLoadDataByTags((res) => {
            var hisTags = wx.getStorageSync('artHisTags')
            that.setData({
                hotTags: res,
                hisTags: hisTags ? hisTags : []
            });
        });
    },
    /**
     * 先获取热门搜索，在获取历史搜索
     */
    onLoadDataByTags: function (callback) {
        app.Util.network.GET(this,{
            url: app.getApi('wp/v1/article/tags'),
            params: null,
            showLoading: 0,
            success: function (res) {
                callback && callback(res.data);
            }
        });
    },

    /**
     * 根据标签关键字搜索
     */
    searchBytag: function (e) {
        var keywords = e.currentTarget.dataset.keywords;
        this.setData({
            keywords: keywords,
            isPass: true
        });
        this.submitFormBySearch();
    },

    /**
     * 提交表单
     */
    submitFormBySearch: function (e) {
        var that = this;
        var isPass = that.data.isPass;
        if (isPass) {
            var keywords = that.data.keywords;
            //设置历史搜索缓存
            that.setHisTags();
            that.setData({
                isSearch: false,
                showDelKdIcon: true
            });
            var params = {
                page: 1,
                keywords: that.data.keywords
            };
            that.onLoadByFirst(params);
        }
        return;
    },

    /**
     * 设置关键词
     */
    setKeywords: function (e) {
        var keywords = e.detail.value;
        var isPass = true;
        var showDelKdIcon = false;
        if (!keywords || keywords.length < 2) isPass = false;
        if (keywords) showDelKdIcon = true
        this.setData({
            keywords: keywords,
            isPass: isPass,
            showDelKdIcon: showDelKdIcon
        });
    },

    /**
     * 删除关键字
     */
    delKeywords: function () {
        this.setData({
            keywords: '',
            isPass: false,
            showDelKdIcon: false,
            isSearch: true,
            page: 0,
            articleList: [],
            isLoadAll: false,
            isLoading: false,
            loadingTxt: ''
        });
    },

    /**
     * 设置历史搜索缓存数据
     * 当搜索关键词没有时，添加到头部
     * 当搜索关键词存在时，提取到头部再存入缓存
     */
    setHisTags: function () {
        var that = this;
        var hisTags = that.data.hisTags;
        var keywords = that.data.keywords;
        var _hisTags = that.data.hisTags;
        if (hisTags.length > 0) {
            //判断是否存在
            if (_hisTags.indexOf(keywords) >= 0) {
                //存在
                var index = _hisTags.indexOf(keywords);
                keywords = _hisTags[index];
                _hisTags.splice(index, 1);
            }
            _hisTags.unshift(keywords);
        } else {
            _hisTags.push(keywords);
        }
        wx.setStorageSync('artHisTags', _hisTags);
        this.setData({
            hisTags: _hisTags
        });
        return;
    },

    /**
     * 清楚历史搜索记录
     */
    delHisTags: function () {
        wx.setStorageSync('artHisTags', []);
        this.setData({
            hisTags: []
        });
    },

    /**
     * 根据关键字获取数据
     */
    getDataByParams: function (params, callback) {
        app.Util.network.POST(this,{
            url: app.getApi('wp/v1/article/list'),
            params: params,
            showLoading: 0,
            success: function (data) {
                callback && callback(data);
            }
        });
    }

}))