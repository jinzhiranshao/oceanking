const app = getApp();
var config = function() {
    return {
        /**
         * 首次加载
         */
        onLoadByFirst: function(params) {
            var that = this;
            that.getDataByParams(params, (info) => {
                //判断是否有数据
                //判断是否加载完全部
                var isLoadAll = false;
                var isLoading = false;
                if (info.page == info.pageCount) {
                    isLoadAll = true;
                }

                that.setData({
                    page: 1,
                    articleList: info.data,
                    isLoadAll: isLoadAll,
                    isLoading: isLoading
                });
            });
            return;
        },

        /**
         * 上拉加载更多
         */
        onReachBottom: function() {
            //首先判断是否加载所有
            if (this.data.isLoadAll) return;
            //可以加载更多
            this.setData({
                isLoading: true
            });
            var params = {};
            params.page = this.data.page + 1;
            if (this.data.currentCate) params.cate = this.data.currentCate;
            if (this.data.keywords) params.keywords = this.data.keywords;
            this.onLoadData(params);
        },

        /**
         * 上拉加载更多
         */
        onLoadData: function(params) {
            var that = this;
            var _data = that.data.articleList; //当前已有数据
            that.getDataByParams(params, (info) => {
                //判断是否最后一页
                var isLoadAll = false;
                var isLoading = false;
                if (info.page == info.pageCount) {
                    isLoadAll = true;
                }

                var __data = {
                    page: params.page,
                    isLoadAll: isLoadAll,
                    isLoading: isLoading
                };

                var _l = _data ? _data.length:0;
                var data = info.data;
                var l = data.length;

                if (info.page == 1) {
                    __data["articleList"] = data;
                } else {
                    for (var i = 0; i < l; i++) {
                        __data["articleList[" + (_l + i) + "]"] = data[i];
                    }
                }
                that.setData(__data);

            });
            return;
        }
    };
};
module.exports = config();