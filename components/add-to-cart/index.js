const app = getApp();
const cartApi = require('../../utils/cart.js');
const route = require('../../utils/route.js');
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        productInfo:Object,
        cart:Object
    },
    options: {
        addGlobalClass: true
    },
    data: Object.assign({}, {
        apiAssets:app.config.apiAssets,
        icons: app.config.icons
    }),

    /**
     * 组件的方法列表
     */
    methods: Object.assign({}, route, {
        onQuantityAdd:function(){
            var that = this;
            var cart = this.data.cart;
            var productInfo = this.data.productInfo;

            if (productInfo.type!='simple'){
                return;
            }

            var onSuccess = function (res) {             
                that.triggerEvent('onCartChange', res, {
                    bubbles: true,
                    composed: true
                });
            };

            if (cart && cart.items && cart.items[productInfo.id]){
                var quantity = cart.items[productInfo.id].quantity;
                quantity++;
                if(quantity<productInfo.min_qty){
                    quantity = productInfo.min_qty;
                }
                if(productInfo.max_qty>=0&&quantity>productInfo.max_qty){
                    quantity = productInfo.max_qty;
                }

                cartApi.set_quantity(that,cart.items[productInfo.id].key, quantity, onSuccess);
            }else{
                var quantity=1;
                if (quantity < productInfo.min_qty) {
                    quantity = productInfo.min_qty;
                }
                if (productInfo.max_qty!==''&& productInfo.max_qty >= 0 && quantity > productInfo.max_qty) {
                    quantity = productInfo.max_qty;
                }

                cartApi.add(that, productInfo.id, 0, quantity, {}, onSuccess);
            }
        },
        onQuantityChange:function(e){
            var quantity = e.detail.value;
            var that = this;
            var cart = this.data.cart;
            var productInfo = this.data.productInfo;

            if (productInfo.type != 'simple') {
                return;
            }

            var onSuccess = function (res) {
                that.triggerEvent('onCartChange', res, {
                    bubbles: true,
                    composed: true
                });
            };

            if (quantity == ''){
                return;
            }

            if (quantity < productInfo.min_qty) {
                quantity = 0;
            }
            if (productInfo.max_qty !== '' &&productInfo.max_qty >= 0 && quantity > productInfo.max_qty) {
                quantity = productInfo.max_qty;
            }
            if (cart && cart.items && cart.items[productInfo.id]) {
                cartApi.set_quantity(that,cart.items[productInfo.id].key, quantity, onSuccess);
            }else{
                cartApi.add(that,productInfo.id,null,quantity,{},onSuccess);
            }
        },
        onQuantityCut:function(){
            var that = this;
            var cart = this.data.cart;
            var productInfo = this.data.productInfo;

            if (productInfo.type != 'simple') {
                return;
            }

            var onSuccess = function (res) {
                that.triggerEvent('onCartChange', res, {
                    bubbles: true,
                    composed: true
                });
            };

            if (cart && cart.items && cart.items[productInfo.id]) {
                var quantity = cart.items[productInfo.id].quantity;
                quantity--;
                if (quantity < 0) { quantity=0;}
                if (quantity < productInfo.min_qty ) {
                    quantity = 0;
                }
                if (productInfo.max_qty !== '' &&productInfo.max_qty >= 0 && quantity > productInfo.max_qty) {
                    quantity = productInfo.max_qty;
                }

                cartApi.set_quantity(that,cart.items[productInfo.id].key, quantity, onSuccess);
            }
        }
    })
})
