const app = getApp();
var config = function() {
    return {
        onPageScroll(page, scrollTop) {
            this.__onNavbarChange__(page,scrollTop, function(ani) {
                page.setData({
                    navbarShowAni: ani
                });
            });
        },
        __onNavbarChange__(that,scrollTop, callback, refresh = false) {
            if (that.__tmp_is_navbar_show__ && scrollTop >= 200) {
                return;
            }

            app.getSystemInfo(function(sys) {
                if (scrollTop >= (sys.navbarHeight + sys.statusBarHeight)) {
                    if (that.__tmp_is_navbar_show__ && !refresh) {
                        return;
                    }
                    that.__tmp_is_navbar_show__ = true;
                    callback(true);
                } else {
                    if (!that.__tmp_is_navbar_show__ && !refresh) {
                        return;
                    }

                    that.__tmp_is_navbar_show__ = false;
                    callback(false);
                }
            });
        }
    }
};
module.exports = config();