// components/cat-filter/index.js
const app = getApp();
const route = require('../../utils/route.js');
Component({
    _btnHeight: 32,
    data: Object.assign({}, app.config.theme, {
        apiAssets: app.config.apiAssets
    }),

    properties: {
        title: String,
        cls: String,
        showHomeBar: {
            type: Boolean,
            value: true
        },
        hideAni: Object,
        stl: String,
        hidden: Boolean,
        showAni: Boolean,
        data:Object,
        custom:Boolean
    },
    observers: {
        'showAni': function (showAni) {
            if (!this.data.hidden){
                return;
            }
            var that = this;
            if (!that.__ani__) {
                that.__ani__ = wx.createAnimation({
                    duration: 400,
                    timingFunction: 'ease',
                });
                return;
            }

            if (!showAni) {
                that.__ani__.opacity(0).step();
            } else{
                that.__ani__.opacity(1).step();
            }

            that.setData({
                ani: that.__ani__.export()
            });
        }
    },
    options: {
        addGlobalClass: true
    },
    ready: function() {
        var that = this;
        app.getSystemInfo(function(sys) {
            var isBack = false;
            var pages = getCurrentPages();
            if (pages && pages.length > 1) {
                isBack = true;
            }
            that.setData({
                statusBarHeight: sys.statusBarHeight,
                navbarHeight: sys.navbarHeight,
                navHeight: sys.navHeight,
                isIphoneX: sys.isIphoneX,
                isBack: isBack,
                windowWidth: sys.windowWidth
            });
        });
    },
    methods:Object.assign({}, route,{
        onBackToLast() {
            if (this.data.custom){
                this.triggerEvent('backtolast', {}, {
                    bubbles: true,
                    composed: true
                });
                return;
            }
            this.__go_back__();
        }
    })
})