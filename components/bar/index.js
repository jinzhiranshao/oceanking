const app = getApp();
const route = require('../../utils/route.js');
Component({
    data: Object.assign({}, {
        apiAssets: app.config.apiAssets,
    }),
    properties: {
        bottom: Number,
        show: Boolean,
        cart: Object,
        hideCart: {
            type: Boolean,
            value: false
        },
        hideHome: {
            type: Boolean,
            value: false
        },
        size: {
            type: Number,
            value: 70
        }
    },
    options: {
        addGlobalClass: true
    },
    __ani_home__: null,
    __ani__top__: null,
    observers: {
        'show': function(show) {
            var that = this;
            if (!that.__ani_home__) {
                that.__ani_home__ = wx.createAnimation({
                    duration: 250,
                    timingFunction: 'ease',
                });
            }

            if (!that.__ani__top__) {
                that.__ani__top__ = wx.createAnimation({
                    duration: 250,
                    timingFunction: 'ease',
                });
                return;
            }

            app.getSystemInfo(function(sys) {
                if (show) {
                    that.__ani_home__.translateY(-((that.data.size + 15) * sys.windowWidth / 750)).step();
                    that.__ani__top__.height(that.data.size + 'rpx').width(that.data.size + 'rpx').step();
                } else {
                    that.__ani_home__.translateY(0).step();
                    that.__ani__top__.height('0rpx').width('0rpx').step();
                }

                that.setData({
                    ani_home: that.__ani_home__.export(),
                    ani_top: that.__ani__top__.export()
                });
            });
        }
    },
    ready() {
        var pages = getCurrentPages();
        var that = this;
        app.getSystemInfo(function(sys) {
            that.setData({
                isIphoneX: sys.isIphoneX,
                isHomeShow: pages && pages.length > 1
            });
        });
    },
    methods: Object.assign({}, route, {
        onScrollToHome() {
            this.__go_back_to_home__();
        },
        onScrollToTop() {
            wx.pageScrollTo({
                scrollTop: 0,
                duration: 0
            });
        }
    })
})