// components/cat-filter/index.js
const app = getApp();
Component({
    data: Object.assign({}, {
        apiAssets: app.config.apiAssets,
        isOpen: false,
        filter: {}
    }),
    _filterDialogAni: null,
    options: {
        addGlobalClass: true,
        multipleSlots: true
    },
    /**
     * 组件的属性列表
     */
    properties: {
        sort: {
            type: Number,
            value: 0
        },
        cat_id: {
            type: Number,
            value: 0
        },
        is_on_sale: Boolean,
        is_feature: Boolean,
        sortArr: Array,
        filterArr: Object,
        scrollTop: {
            type: Number,
            value: 0
        }
    },
    ready() {
        var that = this;
        app.getSystemInfo(function(sys) {
            that.setData({
                windowHeight: sys.windowHeight
            });
        });

        this._filterDialogAni = wx.createAnimation({
            duration: 250,
            timingFunction: 'ease'
        });
    },
    /**
     * 组件的方法列表
     */
    methods: {
        onFeatureChange: function(e) {
            var that = this;
            this.setData({
                isLoadingData: false,
                isNoneData: false,
                is_feature: !this.data.is_feature
            }, function() {
                that.__trigger__();
            });
        },
        onSaleChange: function(e) {
            var that = this;
            this.setData({
                isLoadingData: false,
                isNoneData: false,
                is_on_sale: !this.data.is_on_sale
            }, function() {
                that.__trigger__();
            });
        },
        onSortChange: function(e) {
            var that = this;
            this.setData({
                isLoadingData: false,
                isNoneData: false,
                sort: e.detail.value
            }, function() {
                that.__trigger__();
            });
        },
        __trigger__: function() {
            var res = {
                sort: this.data.sort,
                is_on_sale: this.data.is_on_sale ? 'yes' : 'no',
                is_feature: this.data.is_feature ? 'yes' : 'no'
            };

            if (this.data.filter) {
                for (var p in this.data.filter) {
                    if (this.data.filter[p]) {
                        var options = null;
                        for (var key2 in this.data.filter[p]) {
                            if (this.data.filter[p][key2]) {
                                if (!options) {
                                    options = {};
                                }
                                options[key2] = this.data.filter[p][key2];
                            }
                        }
                        if (options) {
                            res[p] = JSON.stringify(options);
                        }
                    }
                }
            }

            this.triggerEvent('filter', res, {
                bubbles: true,
                composed: true
            });

            if (this.data.scrollTop) {
                wx.pageScrollTo({
                    scrollTop: this.data.scrollTop,
                    duration:0
                });
            }
        },
        onFilterToggle() {
            this.onFilterOpen(!this.data.isOpen);
        },
        onFilterOpen: function(isOpen = false) {
            var that = this;
            if (!isOpen) {
                that._filterDialogAni.height('0rpx').step();
                that.setData({
                    isOpen: false,
                    filterAni: that._filterDialogAni.export()
                });
                return;
            }

            var height = 100;
            var filterHeight = 260;

            var scrollViewHeight = 0;
            if (that.data.filterArr) {
                for (var p in that.data.filterArr) {
                    scrollViewHeight += filterHeight;
                }
            }

            if (scrollViewHeight > 600) {
                scrollViewHeight = 600;
            }

            that._filterDialogAni.height(scrollViewHeight + height + 'rpx').step();
            that.setData({
                isOpen: true,
                scrollViewHeight: scrollViewHeight,
                filterAni: that._filterDialogAni.export()
            }, function() {
                if (that.data.scrollTop) {
                    wx.pageScrollTo({
                        scrollTop: that.data.scrollTop,
                        duration:0
                    });
                }
            });
        },
        onInput: function(e) {
            var key1 = e.currentTarget.dataset.key1;
            var key2 = e.currentTarget.dataset.key2;
            var value = e.detail && e.detail.value ? e.detail.value : null;

            var filter = this.data.filter;
            if (!filter) {
                filter = {};
            }

            if (!filter[key1]) {
                filter[key1] = {};
            }

            filter[key1][key2] = value;

            var that = this;

            var filterCount = 0;
            for (var p in filter) {
                if (filter[p]) {
                    for (var p1 in filter[p]) {
                        if (filter[p][p1]) {
                            filterCount++;
                        }
                    }
                }
            }

            this.setData({
                filterCount: filterCount,
                filter: filter
            });
        },
        onSelect: function(e) {
            var key1 = e.currentTarget.dataset.key1;
            var key2 = e.currentTarget.dataset.key2;

            var filter = this.data.filter;
            if (!filter) {
                filter = {};
            }

            if (!filter[key1]) {
                filter[key1] = {};
            }

            if (filter[key1] && filter[key1][key2]) {
                filter[key1][key2] = null;
            } else {
                if (!filter[key1]) {
                    filter[key1] = {};
                }

                filter[key1][key2] = true;
            }

            var that = this;
            var filterCount = 0;
            for (var p in filter) {
                if (filter[p]) {
                    for (var p1 in filter[p]) {
                        if (filter[p][p1]) {
                            filterCount++;
                        }
                    }
                }
            }
            this.setData({
                filterCount: filterCount,
                filter: filter
            });
        },
        onReset: function() {
            this.setData({
                is_feature: false,
                is_on_sale: false,
                sort: 0,
                filterCount: 0,
                filter: {}
            });
        },
        onSubmit: function(e) {
            var formId = e.detail.formId;
            if (formId && !formId.startsWith('requestFormId:fail')) {
                app.formIds[formId] = Math.round((new Date()).getTime() / 1000);
            }
            var that = this;
            that.setData({
                isLoadingData: false,
                isNoneData: false
            }, function() {
                that.onFilterOpen(false);
                that.__trigger__();
            });
        }
    }
})