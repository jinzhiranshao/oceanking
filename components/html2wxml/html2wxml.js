var html2wxml = require('html2wxml-main.js');
const route = require('../../utils/route.js');
const app = getApp();
Component({
    data: {
        apiAssets:app.config.apiAssets
    },
    options: {
        addGlobalClass: true
    },
    properties: {
        nodes: {
            type: Array,
            value: {},
            observer: function(newVal, oldVal) {
                html2wxml.html2wxml(newVal, this, this.data.padding);
            }
        },
        type: {
            type: String,
            value: 'html'
        },
        highlight: {
            type: Boolean,
            value: true,
        },
        highlightStyle: {
            type: String,
            value: 'darcula'
        },
        linenums: {
            type: Boolean,
            value: true,
        },
        padding: {
            type: Number,
            value: 0
        },
        imghost: {
            type: String,
            value: null
        },
        showLoading: {
            type: Boolean,
            value: true
        },
        cls:String
    },
    methods: Object.assign({}, route, {
        wxmlTagATap: function(e) {
            this.triggerEvent('WxmlTagATap', {
                src: e.currentTarget.dataset.src
            });
        }
    })
})