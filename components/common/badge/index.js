// components/common/badge/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        num:{
            type: Number,
            value:null
        },
        top: {
            type: Number,
            value: -10
        },
        right: {
            type: Number,
            value: -20
        },
        text:String
    },
    options: {
        addGlobalClass: true
    }
})
