// components/search/index.js
const app = getApp();
const route = require('../../../utils/route.js');
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        placeholder: String,
        cat:{
            type:Boolean,
            value:true
        },
        sticky:Boolean,
        url: {
            type: String,
            value: '/pages/search/index?focus=1',
        }
    },
    options: {
        addGlobalClass: true
    },

    /**
     * 组件的初始数据
     */
    data: Object.assign({},{
        apiAssets: app.config.apiAssets
    }),
    /**
     * 组件的方法列表
     */
    methods: Object.assign({}, route, {

    })
})