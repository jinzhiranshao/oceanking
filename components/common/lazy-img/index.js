const app = getApp();
Component({
    __observer: null,
    properties: {
        'class': String,
        style: String,
        mode: String,
        width: Number,
        height: Number,
        src: String
    },
    // data: {
    //     showsrc: null
    // },
    // observers: {
    //     'src': function(src) {
    //         if (this._showsrc && this._showsrc!=src){
    //             this._showsrc = src;
    //             this.setData({
    //                 showsrc: this._showsrc
    //             });
    //         }
    //     }
    // },
    // ready: function() {
    //     var that = this;
    //     if (!that.__observer) {
    //         that.__observer = that.createIntersectionObserver();
    //     }
    //     that._showsrc = null;

    //     that.__observer.relativeToViewport({
    //         bottom: -40,
    //     }).observe('#lazy-img', (res) => {
    //         if ((res.intersectionRatio > 0 && that._showsrc) ||
    //             (res.intersectionRatio <= 0 && !that._showsrc)) {
    //             return;
    //         }
    //         that._showsrc = res.intersectionRatio > 0 ? that.data.src : null;
    //         that.setData({
    //             showsrc: that._showsrc
    //         });
    //     });
    // },
    // detached: function() {
    //     if (this.__observer) {
    //         this.__observer.disconnect();
    //         this.__observer = null;
    //     }
    // },
    // methods: {

    // }
})