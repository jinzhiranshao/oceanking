// components/common/navigation/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        title: String,
        cls:String,
        hovercls:{
            type: String,
            value:'none'
        },
        subtitle: String,
        icon: String,
        border_top:Boolean,
        border_bottom: Boolean,
        mulsub:Boolean,
        small:Boolean
    },
    options: {
        addGlobalClass: true
    }
})