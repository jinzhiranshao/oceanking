// components/common/title/index.js
Component({
    properties: {
        title: String,
        cls:String,
        border_top: Boolean,
        border_bottom: Boolean,
        small:Boolean
    },
    options: {
        addGlobalClass: true
    }
})