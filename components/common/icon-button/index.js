// components/common/icon-button/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        icon: String,
        title: String
    },
    options: {
        addGlobalClass: true
    }
})