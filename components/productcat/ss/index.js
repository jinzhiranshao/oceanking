const app = getApp();
const route = require('../../../utils/route.js');
Component({
    _touch_bar:0,
    _current_group: null,
    properties: {
        catObj: Object,
        height: Number
    },
    options: {
        addGlobalClass: true
    },
    ready(){
        var that = this;
        this.createSelectorQuery().select('#touch-bar').boundingClientRect().exec(function (res) {
            if (res && res.length > 0 && res[0]) {
                that._touch_bar = res[0].top
            }

        });
    },
    /**
     * 组件的方法列表
     */
    methods: Object.assign({}, route, {
        onTouchStart (e) {
            this.__onTouch(e.currentTarget.dataset.group);
        },
        onTouchEnd(e){
            this.setData({
                dialogShow: false
            });
        },
        onTouchMove(e){
            var that = this;
            if (!that.data.catObj || !that.data.catObj.length){
                return;
            }
           
            var now = e.touches[0].pageY - that._touch_bar;
            if(now<=0){
                that.__onTouch(that.data.catObj[0].group);
                return;
            }
            app.getSystemInfo(function(sys){
                var current = Math.ceil(now / (40 * sys.windowWidth/750));
                current = current >= that.data.catObj.length ? (that.data.catObj.length - 1):current ;
                that.__onTouch(that.data.catObj[current].group);
            });
           
        },
        __onTouch(group,show=true){
            if (this._current_group && this._current_group==group){
                return;
            }
            this._current_group = group;
            this.setData({
                current_group: group,
                dialogShow: show
            });
            wx.vibrateShort({});
        }
    })
})
