// components/productcat/dj/index.js
const catapi = Object.assign({}, require('../../../utils/apis/pro-list.js'));
const app = getApp();
Component({
    options: {
        addGlobalClass: true
    },
    data:{
        apiAssets: app.config.apiAssets,
        current_cat_index:0
    },
    properties: {
        footer:Object,
        cart: Object,
        height: Number
    },
    ready(){
        var that = this;
        catapi.prepareLoad = function (options) {
            options.modal = 'detail';
            if (that.data.footer&&that.data.footer._cats && that.data.footer._cats.items1){
                options.cat_id = that.data.footer._cats.items1[that.data.current_cat_index].term_id;
            }
        }
        catapi.onLoad(this);
    },
    methods: {
        onAddedToCart: function (e) {
            this.setData({
                cart: e.detail
            });
        },
        leftMenuAction:function(e){
            var current_cat_index = e.currentTarget.dataset.index;
            var that = this;
            that.setData({
                'current_cat_index': current_cat_index
            },function(){
                catapi.onPullDownRefresh(that);
            });
        },
        onScrollEnd:function(e){
            catapi.onReachBottom(this);
        },
        onCartChange(res) {
            this.triggerEvent('onCartChange', res.detail, {
                bubbles: true,
                composed: true
            });
        }
    }
})
