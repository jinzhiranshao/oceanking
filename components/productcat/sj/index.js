const app = getApp();
const route = require('../../../utils/route.js');
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        catObj:Object,
        height: Number
    },
    options: {
        addGlobalClass: true
    },
    data: Object.assign({},  {
        current_cat_index:0
    }),

    /**
     * 组件的方法列表
     */
    methods: Object.assign({}, route, {
        onLeftMenuTap:function(e){
            this.setData({
                ["current_cat_index"]: e.currentTarget.dataset.index
            });
        }
    })
})
