const app = getApp();
const route = require('../../../utils/route.js');
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        items: Array,
        cart: Object,
        small: Boolean
    },
    options: {
        addGlobalClass: true
    },

    /**
     * 组件的方法列表
     */
    methods: Object.assign({}, route, {
        onCartChange(res) {
            this.triggerEvent('onCartChange', res.detail, {
                bubbles: true,
                composed: true
            });
        }
    })
})