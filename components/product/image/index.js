const app = getApp();
const route = require('../../../utils/route.js');
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        item:Object,
        width:Number,
        height:{
            type:Number,
            value:0
        },
        has_flug:{
            type: Boolean,
            value:true
        }
    }, options: {
        addGlobalClass: true
    },
    data:{
        icon_is_on_sale: app.config.icons.icon_is_on_sale,
        icon_is_featured: app.config.icons.icon_is_featured,
        icon_is_new: app.config.icons.icon_is_new
    },
    /**
     * 组件的方法列表
     */
    methods: Object.assign({}, route, {

    })
})
