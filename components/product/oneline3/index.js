const app = getApp();
const route = require('../../../utils/route.js');
const _cart = require('../../../utils/_cart.js');
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        data: Object,
        cart: Object
    }, options: {
        addGlobalClass: true
    },
    data: {
        theme_size: app.config.theme.theme_size
    },
    /**
     * 组件的方法列表
     */
    methods: Object.assign({}, route, _cart,{

    })
})