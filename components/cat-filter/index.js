// components/cat-filter/index.js
const app = getApp();
Component({
    data: Object.assign({}, {
        apiAssets: app.config.apiAssets,
        isOpen: false,
        current_cat_index:0,
        filter: {}
    }),
    options: {
        addGlobalClass: true,
        multipleSlots: true
    },
    observers: {
        'catObj': function(catObj) {
            var data = null;
            var cat_id = this.data.cat_id;

            if (cat_id) {
                var hasCat = false;
                if (catObj.items1) {
                    for (var index in catObj.items1) {
                        var cat = catObj.items1[index];
                        if (cat.term_id == cat_id) {
                            hasCat = true;
                            if (index != this.data.cat_index) {
                                if (!data) {
                                    data = {};
                                }
                                data['cat_index'] = index;
                                data['current_cat_index'] = index;
                                data['cat_name'] = cat.name;
                                break;
                            }
                        }
                    }
                }
                if (!hasCat && cat_id) {
                    if (!data) {
                        data = {};
                    }
                    data['cat_id'] = 0;
                }
            } else if (this.data.cat_index) {
                if (!data) {
                    data = {};
                }
                data['cat_id'] = 0;
            }

            if (data) {
                this.setData(data);
            }
        }
    },
    /**
     * 组件的属性列表
     */
    properties: {
        catObj: Object,
        sort: {
            type: Number,
            value: 0
        },
        cat_id: {
            type: Number,
            value: 0
        },
        is_on_sale: Boolean,
        is_feature: Boolean,
        cat_index: {
            type: Number,
            value: 0
        },
        sortArr: Array,
        filterArr: Object,
        scrollTop: {
            type: Number,
            value: 0
        }
    },
    _selectedBarAni: null,
    _filterDialogAni: null,
    ready: function() {
        this._selectedBarAni = wx.createAnimation({
            duration: 250,
            transformOrigin: "50% 50%",
            timingFunction: 'ease'
        });

        this._filterDialogAni = wx.createAnimation({
            duration: 250,
            timingFunction: 'ease'
        });

        var that = this;
        app.getSystemInfo(function(sys){
            that.setData({
                windowHeight: sys.windowHeight
            });
        });
    },
    /**
     * 组件的方法列表
     */
    methods: {
        onCatSelected: function(e) {
            var index = e.currentTarget.dataset.index;
            var cat = this.data.catObj && this.data.catObj.items1 ? this.data.catObj.items1[index] : null;
            if (!cat) {
                return;
            }
            this._selectedBarAni.translateX(e.currentTarget.offsetLeft + 'px').step();

            var that = this;
            this.setData({
                cat_name: cat.name,
                isLoadingData: false,
                isNoneData: false,
                cat_id: cat.term_id,
                current_cat_index: index,
                selectedBarAni: this._selectedBarAni.export()
            }, function() {
                //that.onFilterOpen(false);
                that.__trigger__();
            });
        },
        onFeatureChange: function(e) {
            var that = this;
            this.setData({
                isLoadingData: false,
                isNoneData: false,
                is_feature: !this.data.is_feature
            }, function() {
               // that.onFilterOpen(false);
                that.__trigger__();
            });
        },
        onSaleChange: function(e) {
            var that = this;
            this.setData({
                isLoadingData: false,
                isNoneData: false,
                is_on_sale: !this.data.is_on_sale
            }, function() {
                //that.onFilterOpen(false);
                that.__trigger__();
            });
        },
        onSortChange: function(e) {
            var that = this;
            this.setData({
                isLoadingData: false,
                isNoneData: false,
                sort: e.detail.value
            }, function() {
                //that.onFilterOpen(false);
                that.__trigger__();
            });
        },
        __trigger__: function() {
            var res = {
                cat_id: this.data.cat_id,
                sort: this.data.sort,
                is_on_sale: this.data.is_on_sale,
                is_feature: this.data.is_feature
            };

            if (this.data.filter) {
                for (var p in this.data.filter) {
                    if (this.data.filter[p]) {
                        var options = null;
                        for (var key2 in this.data.filter[p]) {
                            if (this.data.filter[p][key2]) {
                                if (!options) {
                                    options = {};
                                }
                                options[key2] = this.data.filter[p][key2];
                            }
                        }
                        if (options) {
                            res[p] = JSON.stringify(options);
                        }
                    }
                }
            }
            this.triggerEvent('filter', res, {
                bubbles: true,
                composed: true
            });

            if (this.data.scrollTop) {
                wx.pageScrollTo({
                    scrollTop: this.data.scrollTop,
                    duration:0
                });
            }
        },
        onFilterToggle() {
            this.onFilterOpen(!this.data.isOpen);
        },
        onFilterOpen: function(isOpen = false) {
            var that = this;
            if (!isOpen) {
                that._filterDialogAni.height('0rpx').step();
                that.setData({
                    isOpen: false,
                    filterAni: that._filterDialogAni.export()
                });
                return;
            }

            var height = 200;
            var filterHeight = 260;

            var scrollViewHeight = 0;
            if (that.data.filterArr) {
                for (var p in that.data.filterArr) {
                    scrollViewHeight += filterHeight;
                }
            }

            if (scrollViewHeight > 500) { scrollViewHeight=500;}

            that._filterDialogAni.height(scrollViewHeight + height + 'rpx').step();
            that.setData({
                isOpen: true,
                scrollViewHeight: scrollViewHeight,
                filterAni: that._filterDialogAni.export()
            },function(){
                if (that.data.scrollTop) {
                    wx.pageScrollTo({
                        scrollTop: that.data.scrollTop,
                        duration:0
                    });
                }
            });
        },
        onInput: function(e) {
            var key1 = e.currentTarget.dataset.key1;
            var key2 = e.currentTarget.dataset.key2;
            var value = e.detail && e.detail.value ? e.detail.value : null;

            var filter = this.data.filter;
            if (!filter) {
                filter = {};
            }

            if (!filter[key1]) {
                filter[key1] = {};
            }

            filter[key1][key2] = value;

            var that = this;
            this.setData({
                filter: filter
            });
        },
        onSelect: function(e) {
            var key1 = e.currentTarget.dataset.key1;
            var key2 = e.currentTarget.dataset.key2;

            var filter = this.data.filter;
            if (!filter) {
                filter = {};
            }

            if (!filter[key1]) {
                filter[key1] = {};
            }

            if (filter[key1] && filter[key1][key2]) {
                filter[key1][key2] = null;
            } else {
                if (!filter[key1]) {
                    filter[key1] = {};
                }

                filter[key1][key2] = true;
            }

            var that = this;
            this.setData({
                filter: filter
            });
        },
        onReset: function() {
            this.setData({
                is_feature: false,
                is_on_sale: false,
                sort: 0,
                filter: {}
            });
        },
        onSubmit: function(e) {
            var formId = e.detail.formId;
            if (formId && !formId.startsWith('requestFormId:fail')) {
                app.formIds[formId] = Math.round((new Date()).getTime() / 1000);
            }
            var that = this;
            that.setData({
                isLoadingData: false,
                isNoneData: false
            }, function() {
                that.onFilterOpen(false);
                that.__trigger__();
            });
        }
    }
})