// components/coupon/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        coupon: Object,
        cls :String,
        tb:String
    },
    options: {
        addGlobalClass: true
    }
})