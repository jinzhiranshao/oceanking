const app = getApp();
Component({
    data: Object.assign({}, {
        isVideoPlay:false,
        apiAssets: app.config.apiAssets,
        is_sale_out: app.config.icons.icon_is_sale_out
    }),
    properties: {
        productInfo: Object
    },
    options: {
        addGlobalClass: true
    },
    methods: {
        showSliderImg: function(e) {
            if (!this.data.productInfo){return;}
            var r = e.currentTarget.dataset.idx;
            var urls = [];
            for (var index in this.data.productInfo.gallerys){
                urls.push(this.data.productInfo.gallerys[index].full.url);
            }
            wx.previewImage({
                current: urls[r],
                urls: urls
            });
        },
        onBtnPlayerClick:function(){
            var that = this;
            this.setData({
                isVideoPlay:true
            },function(){
                that.triggerEvent('play', true, {
                    bubbles: true,
                    composed: true
                });
            });
        },
        onBtnCloseVideoClick:function(){
            var that = this;
            this.setData({
                isVideoPlay: false
            }, function () {
                that.triggerEvent('play', false, {
                    bubbles: true,
                    composed: true
                });
            });
        }
    }
});