// components/productDetail/productAttribute/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        productInfo: Object
    },
    options: {
        addGlobalClass: true
    },
    observers: {
        'productInfo': function (productInfo) {
            var height = 0;
            var count = 0;
            if (productInfo.attributes1) {
                for (var g in productInfo.attributes1) {
                    count++;
                    height += 28;
                    if (productInfo.attributes1[g].options) {
                        var i = 0;
                        for (var p in productInfo.attributes1[g].options) {
                            if (i++ % 3 == 0) {
                                height += 100;
                            }
                        }
                    }
                }
            }

            this.setData({
                count:count,
                height: count < 2 ? 0 : height
            });
        }
    },
    methods: {
        onAttClick: function(e) {
            if (!this.data.productInfo.attributes1) {
                return;
            }

            var index = e.currentTarget.dataset.index;
            var option = e.currentTarget.dataset.option;
            var parent = e.currentTarget.dataset.parent;
            var current = this.data.productInfo.attributes1[option]['options'][index];
            if (current.isSellOut) {
                return;
            }

            var selected_attributes = this.data.selected_attributes;
            if (!selected_attributes) {
                selected_attributes = {};
            }
            var data = {
                scrolltoattid: 'scroll-att-' + parent
            };
            if (selected_attributes[option] == index) {
                delete selected_attributes[option];
                data["selected_attributes." + option] = null;
            } else {
                selected_attributes[option] = index;
                data["selected_attributes." + option] = index;
            }

            var available_variations = this.data.productInfo.available_variations;
            if (!available_variations || available_variations.length == 0) {
                available_variations={};
            }

            for (var _name in this.data.productInfo.attributes1) {
                var _options = this.data.productInfo.attributes1[_name]['options'];
                if (!_options) {
                    continue;
                }

                //copy selected att
                var new_select_attributes = {};
                for (var _selected_name in selected_attributes) {
                    if (null === selected_attributes[_selected_name]) {
                        continue;
                    }
                    new_select_attributes[_selected_name] = selected_attributes[_selected_name];
                }
                
                for (var _option in _options) {
                    new_select_attributes[_name] = _option;
                    //排查含属性

                    var isSellOut = true;
                    for (var _index in available_variations) {
                        var ava_attributes = available_variations[_index].attributes;
                        if (!ava_attributes) {
                            continue;
                        }

                        var isCurrentAvaAttHas = true;
                        for (var _select__name in new_select_attributes) {
                            var _c = ava_attributes["attribute_" + encodeURIComponent(_select__name).toLowerCase()];
                            if (_c !== '' && _c !== null&& new_select_attributes[_select__name] != _c) {
                                isCurrentAvaAttHas = false;
                                break;
                            }
                        }

                        if (isCurrentAvaAttHas) {
                            isSellOut = false;
                            break;
                        }
                    }
                    if (isSellOut != _options[_option].isSellOut) {
                        data["productInfo.attributes1." + _name + ".options." + _option + ".isSellOut"] = isSellOut;
                    }

                }
            }
            var that = this;
            this.setData(data, function() {
                that.__trigger_event();
            });
        },
        __trigger_event_data: function () {
            var selected_attributes = this.data.selected_attributes;
            if (!selected_attributes) {
                return false;
            }

            //rebuild_selected_attributes
            var new_selected_attributes = {};
            for (var pop in selected_attributes) {
                if (selected_attributes[pop] === null){
                    return false;
                }
                new_selected_attributes['attribute_' + encodeURIComponent(pop).toLowerCase()] = selected_attributes[pop];
            }

            if (Object.keys(new_selected_attributes).length != Object.keys(this.data.productInfo.attributes1).length) {
                return false;
            }
            return new_selected_attributes;
        },
        __trigger_event: function() {
            var atts = this.__trigger_event_data();
            this.triggerEvent('filter', atts, {
                bubbles: true,
                composed: true
            });
        }
    }
})