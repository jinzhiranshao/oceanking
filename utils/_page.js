const app = getApp();
const route = require('route.js');
const utilMd5 = require('md5.js');
const _cart = require('_cart.js');
const cartApi = require('cart.js');

var config = function() {
    return Object.assign({}, route, _cart,{
        _isPageCache: false,
        _requiredAuth: false,
        onShareAppMessage: function(res) {

        },
        onLoad: function(options) {
            if (!options) {
                options = {};
            }
            this._options = options;
        },
        __preDataLoadCacheKey(route,request){
            var cache_key = route;
            try {
                for (var p in request) {
                    if (request[p] && (typeof request[p] == 'number' || typeof request[p] == 'string' || typeof request[p] == 'boolean')) {
                        cache_key += ':' + request[p];
                    }
                }
            } catch (e) {
                console.error(e);
                refresh = false;
            }
            cache_key = 'pages:' + utilMd5.hexMD5(cache_key);
            return cache_key;
        },
        _pageDataLoad: function(route, options, refresh, onDataLoaded) {
            var that = this;

            if (!options) {
                options = {};
            }

            var request =options;
          
            if (this._requiredAuth && !refresh) {
                var lastToken = app.TOKEN;
                if (!app.TOKEN || (this.__tmp_last_token__ && this.__tmp_last_token__ != lastToken)) {
                    refresh = true;
                    this.__tmp_last_token__ = app.TOKEN;
                }
                if (!refresh && !app.isAuthorized()) {
                    refresh = true;
                }
            }

            if(request){
                //如果加载的是主题或scene，那么需要强制刷新下页面
                for (var param in request){
                    if (param == 'theme' || param =='scene'){
                        refresh = true;
                    }
                }
            }
            var cache_key = this.__preDataLoadCacheKey(route,request);

            var isTplLoaded = false;
            if (!refresh && that._isPageCache) {
                if (that.__tmp_p_tpls__) {
                    res = that.__tmp_p_tpls__;
                } else {
                    var res = wx.getStorageSync(cache_key);
                    if (res) {
                        that.__tmp_p_tpls__ = res;
                    }
                }

                if (res) {
                    onDataLoaded(that, res);
                    isTplLoaded = true;
                }
            }

            app.Util.network.GET(that, {
                url: app.getApi(route),
                params: request,
                showLoading: 0,
                success: function(data) {
                    if (!isTplLoaded) {
                        onDataLoaded(that, data);
                    }

                    if (that._isPageCache) {
                        wx.setStorage({
                            key: cache_key,
                            data: data
                        });
                    }
                }
            });
        },
        __onPageScroll(e) {},
        onPageScroll(e) {
            var that = this;
            that.__onPageScroll(e);


            that.__onRightbarChange__(e.scrollTop, function(ani) {
                that.setData({
                    rightBarShow: ani
                });
            });
        },
        //右侧滑动按钮组
        __onRightbarChange__(scrollTop, callback, refresh = false) {
            var that = this;
            app.getSystemInfo(function(sys) {
                if (scrollTop >= sys.windowHeight) {
                    if (that.__tmp_is_rightbar_show__ && !refresh) {
                        return;
                    }
                    that.__tmp_is_rightbar_show__ = true;
                    callback(that.__tmp_is_rightbar_show__);

                } else {
                    if (!that.__tmp_is_rightbar_show__ && !refresh) {
                        return;
                    }
                    that.__tmp_is_rightbar_show__ = false;
                    callback(that.__tmp_is_rightbar_show__);
                }
            });
        },
        __on_loading_close__() {
            this.setData({
                __loading_close__: true
            });
        },
        __onShow() {

        },
        onShow() {
            var that = this;
            cartApi.get(that, function(data) {
                that.setData({
                    cart: data
                });
            });

            this.__onShow();
        }
    });
}
module.exports = config();