// pages/utils/util.js

/**
 * Project: WooCommerce微信小程序
 * Description: 将WooCommerce商城接入微信小程序
 * Author: xunhuweb
 * Organization: xunhuweb (https://www.wpweixin.net)
 */

// 网络请求

// GET请求
function GET(page, requestHandler) {
    request(page, 'GET', requestHandler)
}
// POST请求
function POST(page, requestHandler) {
    request(page, 'POST', requestHandler)
}

function PUT(page, requestHandler) {

    var app = getApp();
    app = app ? app : requestHandler.app;
    wx.showLoading({
        title: '上传中...',
    });

    if (typeof app.TOKEN == 'undefined') {
        try {
            var cached = wx.getStorageSync('system:token');
            app.TOKEN = cached ? cached : false;
        } catch (e) {
            app.TOKEN = false;
            //ignore
        }
    }

    const final = function() {
        wx.hideLoading();

        if (requestHandler.final) {
            requestHandler.final();
        }
    }
    var res = requestHandler.params;
    if (!res) {
        res = {};
    }

    var params = Object.assign({}, res);
    params.version = app.config.version;
    params.sdk = app.config.sdk;

    for (var p in params) {
        if (typeof params[p] == 'undefined' || params[p] === null) {
            delete params[p];
        } else {
            if (typeof params[p]=='object') {
                params[p] = JSON.stringify(params[p]);
            } else if (typeof params[p] == 'boolean') {
                params[p] = params[p]?'yes':'';
            }
        }
    }

    wx.uploadFile({
        url: requestHandler.url,
        filePath: requestHandler.filePath,
        name: requestHandler.name ? requestHandler.name : 'file',
        formData: params,
        header: {
            'content-type': 'application/x-www-form-urlencoded',
            'wrest-token': app.TOKEN
        },
        success: res => {
            final();

            //Unauthorized
            if (res.statusCode == 401 || res.statusCode == 403) {
                app.logout();
                wx.redirectTo({
                    url: '/package_c/pages/login/index'
                });
                return;
            }

            if (typeof res.data == 'string') {
                res.data = JSON.parse(res.data);
            }

            if (!res.data || typeof res.data == 'string' || typeof res.data.code != 'undefined' || res.statusCode != 200) {
                var msg = '服务异常！';

                if (res.statusCode == 404) {
                    msg = '请求服务未找到！';
                } else if (!res.data) {
                    msg = '服务异常！';
                } else if (res.data.message && Array.isArray(res.data.message)) {
                    msg = res.data.message[0];
                } else if (typeof res.data.message == 'string') {
                    msg = res.data.message;
                }
                if (!msg) {
                    msg = '服务异常！';
                }
                wx.showToast({
                    title: msg,
                    icon: 'none',
                    duration: 3000
                });
                page.setData({
                    fail_msg: msg,
                    ['__loading_close__']: true
                });
                if (requestHandler.fail) {
                    requestHandler.fail(msg);
                }
                return;
            }

            if (requestHandler.success) requestHandler.success(res.data);
        },
        fail: (re) => {
            final();

            wx.showToast({
                title: '加载失败，请尝试刷新',
                icon: 'none',
                duration: 3000
            });
        },
        complete: (res) => {
           // console.log('请求' + requestHandler.url + ': (参数 ' + JSON.stringify(requestHandler.params) + ')', res);

            if (requestHandler.complete) requestHandler.complete();
        }
    });
}

function request(page, method, requestHandler) {
    var app = getApp();
    app = app ? app : requestHandler.app;
    if (requestHandler.showLoading == 0) {
        if (page.__final_timer__) {
            clearTimeout(page.__final_timer__);
            delete page.__final_timer__;
        }

        page.setData({
            ['__nav_bar__.loading']: true
        });
        wx.showNavigationBarLoading();
    }

    if (requestHandler.showLoading == 1) {
        wx.showLoading({
            title: '加载中...',
        });
    }

    if (typeof app.TOKEN == 'undefined') {
        try {
            var cached = wx.getStorageSync('system:token');
            app.TOKEN = cached ? cached : false;
        } catch (e) {
            app.TOKEN = false;
            //ignore
        }
    }

    var final = function() {
        if (requestHandler.showLoading == 0) {
            wx.hideNavigationBarLoading();
            if (page.__final_timer__) {
                clearTimeout(page.__final_timer__);
            }

            page.__final_timer__ = setTimeout(function() {
                delete page.__final_timer__;
                page.setData({
                    ['__nav_bar__.loading']: false
                });
            }, 800);
        }

        if (requestHandler.showLoading == 1) {
            wx.hideLoading();
        }
        if (requestHandler.final) {
            requestHandler.final();
        }
    }
    var res = requestHandler.params;
    if (!res) {
        res = {};
    }

    var params = Object.assign({}, res);

    for (var p in params) {
        if (typeof params[p] == 'undefined' || params[p] === null) {
            delete params[p];
        }else{
            if (typeof params[p] == 'object') {
                params[p] = JSON.stringify(params[p]);
            } else if (typeof params[p] == 'boolean') {
                params[p] = params[p] ? 'yes' : '';
            }
        }
    }

    app.getSystemInfo(function(sys) {

        params.version = app.config.version;
        params.sdk = app.config.sdk;
        params.windowHeight = sys.screenHeight;
        params.windowWidth = sys.screenWidth;

        params.screenHeight = sys.screenHeight;
        params.screenWidth = sys.screenWidth;

        params.isIphoneX = sys.isIphoneX;

        wx.request({
            url: requestHandler.url,
            data: params,
            method: method, // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            header: {
                'content-type': 'application/x-www-form-urlencoded',
                'wrest-token': app.TOKEN
            },
            dataType: 'json',
            success: res => {
                final();
                //Unauthorized
                if (res.statusCode == 401 || res.statusCode == 403) {
                    app.logout();
                    wx.navigateTo({
                        url: '/package_c/pages/login/index'
                    });
                    return;
                }

                if (!res.data || typeof res.data == 'string' || typeof res.data.code != 'undefined' || res.statusCode != 200 || typeof res == 'string') {
                    var msg = '服务异常！';

                    if (typeof res == 'string') {
                        msg = '系统内部错误，请尝试下拉刷新！';
                    } else if (res.statusCode == 404) {
                        msg = '请求服务未找到！';
                    } else if (!res.data) {
                        msg = '服务异常！';
                    } else if (res.data.message && Array.isArray(res.data.message)) {
                        msg = res.data.message[0];
                    } else if (typeof res.data.message == 'string') {
                        msg = res.data.message;
                    }
                    if (!msg) {
                        msg = '服务异常！';
                    }
                    wx.showToast({
                        title: msg,
                        icon: 'none',
                        duration: 3000
                    });
                    page.setData({
                        fail_msg: msg,
                        ['__loading_close__']: true
                    });
                    if (requestHandler.fail) {
                        requestHandler.fail(msg);
                    }
                    return;
                }


                if (res.data && typeof res.data == 'object') {
                    for (var p in sys) {
                        res.data[p] = sys[p];
                    }
                }
                if (requestHandler.success) requestHandler.success(res.data);
            },
            fail: (re) => {
                final();

                wx.showToast({
                    title: '加载失败，请尝试刷新',
                    icon: 'none',
                    duration: 3000
                });
            },
            complete: (res) => {
                wx.stopPullDownRefresh();
               // console.log(method + '请求' + requestHandler.url + ': (参数 ' + JSON.stringify(requestHandler.params) + ')', res);

                if (requestHandler.complete) requestHandler.complete();
            }
        });
    });
}

module.exports.network = {
    GET: GET,
    PUT: PUT,
    POST: POST
}