const _page = require('_page.js');
const utilMd5 = require('md5.js');
const app = getApp();

var config = function() {
    return Object.assign({}, _page,  {
        _isPageCache: true,
        _options: {},
        apis: {
            'cat:list': require('./apis/pro-list.js'),
            'pro:list': require('./apis/pro-list.js'),
        },
        _onLoad: function(route, refresh = false) {
            var that = this;
            that._pageDataLoad(route, that._options, refresh, function(that, request) {
                if (typeof request.requiredAuth != 'undefined' && request.requiredAuth) {
                    //that._requiredAuth = request.requiredAuth;
                    that.__refresh_code__();
                }

                if (typeof that.prePageLoad == 'function') {
                    that.prePageLoad(request);
                }

                that.__tmp_api__ = request.footer && that.apis[request.footer.api] ?
                    that.apis[request.footer.api] :
                    null;
                if (that.__tmp_api__) {
                    that.__tmp_api__.property='footer';
                    if (request.footer.modal){
                        that.__tmp_api__.modal = request.footer.modal;
                    }
                    that.__tmp_api__.prepareLoad = that.preApiLoad;
                }

                that.templates = request.templates;

                var setTheTpls = function(that, end, theEndFunc) {
                    var size = 8;
                    if (!that.templates || !that.templates.length) {
                        if (theEndFunc) {
                            theEndFunc(that);
                        }
                        return;
                    }

                    var start = end;
                    end = end + size;

                    if (start >= that.templates.length) {
                        if (theEndFunc) {
                            theEndFunc(that);
                        }
                        return;
                    }

                    var tmp = [];
                    for (var index = start; index < end && index < that.templates.length; index++) {
                        tmp.push(that.__dialog__(that.templates[index]));
                    }

                    var tpls = {};
                    if (start === 0) {
                        that.__temp_t_ = {};
                        if (typeof request.pageNavTitle != 'undefined') {
                            tpls['pageNavTitle'] = request.pageNavTitle;
                        }

                        tpls['config'] = {
                            navHeight: request.navHeight,
                            apiAssets: app.config.apiAssets,
                            isAuthorized: app.isAuthorized()
                        };

                        tpls["templates"] = tmp;
                        tpls["bg"] = null;
                        tpls["footer"] = null;
                    } else {
                        for (var index = start; index < end && index < that.templates.length; index++) {
                            tpls["templates" + "[" + (index) + "]"] = that.__dialog__(that.templates[index]);
                        }
                    }

                    if (end >= (that.templates.length - 1) && request.bg) {
                        tpls["bg"] = request.bg;
                    }

                    that.setData(tpls, function() {
                        setTheTpls(that, end, theEndFunc);
                    });
                }

                setTheTpls(that, 0, function() {
                    that.__theFooter__(request);
                });
            });
        },
        preApiLoad(opts) {
            if (this._options) {
                Object.assign(opts, this._options);
            }
        },
        __dialog__(tpl) {
            if (this.__tmp_dialog_tpl__) {
                return tpl;
            }

            if (tpl.type != 'wrest_template_dialog') {
                return tpl;
            }

            this.__tmp_dialog_tpl__ = tpl;
            var cache_key = 'tpl:dialog:' + utilMd5.hexMD5(this.route);
            switch (tpl.showTime) {
                case 'none':
                    break;
                case 'one':
                    var obj = wx.getStorageSync('tpl:dialog:' + utilMd5.hexMD5(this.route));
                    if (obj && typeof obj == 'object') {
                        if (obj.times && obj.times >= 1) {

                            return tpl;
                        }
                    }

                    wx.setStorage({
                        key: cache_key,
                        data: {
                            time: (new Date()).getTime(),
                            times: 1
                        },
                    });
                    break;
                case 'every-day':
                    var now = (new Date()).getTime();
                    var obj = wx.getStorageSync('tpl:dialog:' + utilMd5.hexMD5(this.route));
                    if (obj && typeof obj == 'object') {
                        if (obj.time && (now - obj.time) <= 60 * 60 * 24 * 1000) {
                            return tpl;
                        }
                    }

                    wx.setStorage({
                        key: cache_key,
                        data: {
                            time: now,
                            times: 1
                        },
                    });
                    break;
            }

            tpl.show = true;


            return tpl;
        },
        __theFooter__: function(request) {
            var that = this;
            if (!request.footer) {
                return;
            }

            wx.createSelectorQuery().selectAll('#body,#bg').boundingClientRect().exec(function(res) {
                var heightBody = 0,
                    heightBg = 0;

                if (res && res.length > 0 && res[0]) {

                    for (var i in res[0]) {
                        var dom = res[0][i];
                        switch (dom.id) {
                            case 'body':
                                heightBody = dom.height;
                                break;
                            case 'bg':
                                heightBg = dom.height;
                                break;
                        }
                    }
                }
                var navHeight = 0;
                var windowHeight = request.windowHeight;
                if (!that.isTab()){
                    windowHeight = request.screenHeight - request.navHeight;
                }
                request.footer._height = windowHeight - (navHeight + heightBody + heightBg);
                if (request.footer._height<=0){
                    request.footer._height = windowHeight;
                }
                request.footer._scrollTop = heightBody ;

                var dataArray = {};
                dataArray['footer'] = request.footer;
                that.setData(dataArray, function() {
                    if (that.__tmp_api__ && typeof that.__tmp_api__.onLoad == 'function') {
                        that.__tmp_api__.onLoad(that);
                    }
                });
            });
        },
        __tapOnVideoShow__(e) {
            var dataArray = {};
            var p = e.currentTarget.dataset.index;
            dataArray["templates[" + p + "].showVideo"] = 'Y';
            this.setData(dataArray);
        },
        __tapOnDialogClose__(e) {
            var dataArray = {};
            var p = e.currentTarget.dataset.index;
            dataArray["templates[" + p + "].show"] = false;
            this.setData(dataArray);
        },
        onReachBottom: function() {
            var that = this;
            if (that.__tmp_api__ && typeof that.__tmp_api__.onReachBottom == 'function') {
                that.__tmp_api__.onReachBottom(that);
            }
        },
        onPullDownRefresh: function() {
            this.onReady(true);
        },
        onFooterChange: function(e) {
            var that = this;
            if (that.__tmp_api__ && typeof that.__tmp_api__.onFooterChange == 'function') {
                that.__tmp_api__.onFooterChange(that, e);
            }
        }
    });
};
module.exports = config();