class Hupijiao {
    _start = false;
    _callback=null;
    _token=null;

    constructor() {
        var that = this;
        wx.onAppShow(function(res) {
            if (!that._start){
                return;
            }

            if (res.referrerInfo && res.referrerInfo.extraData) {
                if (res.referrerInfo.extraData.hpj_jsapi_token!=that._token){
                    return;
                }

                if (that._callback) {
                    console.log(res.referrerInfo.extraData);
                    that._callback({
                        is_paid: res.referrerInfo.extraData.hpj_jsapi_is_paid,
                        order_id: res.referrerInfo.extraData.hpj_jsapi_order_id
                    });
                }
                that.__stop();
            }
        });
    }

    __start(token,callback) {
        this._token = token;
        this._callback = callback;
        this._start = true;
    }

    __stop() {
        this._token = null;
        this._callback=null;
        this._start = false;
    }

    pay(token,callback){
        this.__start(token,callback);
        wx.navigateToMiniProgram({
            appId: 'wx402faa5bd5eda155',
            path: 'pages/pay/index?token=' + this._token
        });
    }
}

export {
    Hupijiao
};