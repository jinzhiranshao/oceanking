const cartApi = require('cart.js');
const app = getApp();
var config = function () {
    return { 
        //标记是否只读取简短购物车信息
        _cart_include_product:false,
        //标记是否强制刷新购物车内容
        _cart_include_checkout:false,
        //已加入购物车回调
        onAddedToCart: function(e) {
            this.setData({
                cart: e.detail
            });
        },
        onCartChange(e){
            this.setData({
                cart: e.detail
            });
        },
    };
};
module.exports = config();