const _page = require('_page.js');
var config = function() {
    return Object.assign({}, _page, {
        _isPageCache: false,
        _options: {},
        _onLoad: function(route, options, refresh = false, onLoaded) {
            this._options = options;

            if (!onLoaded) {
                onLoaded = function(that, data) {
                    that.setData(data);
                };
            }

            this._pageDataLoad(route, options, refresh, function(that,data) {
                onLoaded(that, data);
            });
        },
        onPullDownRefresh: function() {
            this.onReady(true);
        }
    });
};
module.exports = config();