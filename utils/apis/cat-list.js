const _api = require('_api.js');
var config = function(){
    return Object.assign({}, _api,{
        onLoad: function (page) {

        },
        onReachBottom: function (page) {

        },
        onPullDownRefresh: function (page) {

        },
        onFooterChange: function (page, e) {

        }
    });
};

module.exports = config();