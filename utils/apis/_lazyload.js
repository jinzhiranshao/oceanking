const app = getApp();
var config = function() {
    return {
        templates:[],
        page:null,
        pop_data:'templates',
        id_call:function(id){
            return 'section-id';
        },
        init:function(page,templates,pop_data,id_call=null){
            this.page = page;
            this.templates = templates;
            this.pop_data = pop_data;
            if (id_call){
                this.id_call = id_call;
            }
        },
        dataBind: function (start,end){
            var that = this;
            if (!that.templates){
                return;
            }

            var theApi = this;
            for (var index = start; index < end && index < that.templates.length; index++) {
                if (!that.templates[index]._sectioninfo_) {
                    var query = wx.createSelectorQuery()
                    query.select('#' + that.id_call(index)).boundingClientRect();
                    query.selectViewport();
                    query.exec(function (res) {
                        if (!res || !res.length || !res[0]) {
                            return;
                        }

                        var index = res[0].dataset.index;
                        if (typeof index == 'undefined' || !that.templates[index]) {
                            return;
                        }

                        that.templates[index]._sectioninfo_ = {
                            width: res[0].width,
                            height: res[0].height,
                            top: res[0].top,
                            bottom: res[0].bottom
                        };
                    });
                }

            }
        },
        onScroll: function(current_top) {
            var that = this;
            if (!that.templates) {
                return;
            }

            app.getSystemInfo(function(systemInfo) {
                var tplData = null;

                for (var index = 0; index < that.templates.length; index++) {
                    var tpl = that.templates[index];
                    if (!tpl._sectioninfo_) {
                        continue;
                    }

                    var skipPage = 2;
                    //如果当前元素已被隐藏 20以上
                    if (
                        (current_top - (tpl._sectioninfo_.top + tpl._sectioninfo_.height)) < systemInfo.windowHeight * skipPage
                         &&
                        (tpl._sectioninfo_.top - current_top) <= (systemInfo.windowHeight + systemInfo.windowHeight * skipPage)
                    ) {
                        if (tpl._sectioninfo_.hidden) {
                            tpl._sectioninfo_.hidden = false;
                            if (!tplData) {
                                tplData = {};
                            }
                            tplData[that.pop_data+"[" + index + "]._hidden_"] = false;
                        }

                    } else {
                        if (!tpl._sectioninfo_.hidden) {
                            if (!tplData) {
                                tplData = {};
                            }
                            tpl._sectioninfo_.hidden = true;
                            tplData[that.pop_data + "[" + index + "]._hidden_"] = true;
                            if (!tpl._sectioninfo_.tagHeight) {
                                tpl._sectioninfo_.tagHeight = true;
                                tplData[that.pop_data + "[" + index + "]._section_height_"] = tpl._sectioninfo_.height;
                            }

                        }
                    }
                }

                if (tplData) {
                    if (!that.__pre_data__) {
                        that.__pre_data__ = {};
                    }

                    for (var p in tplData) {
                        that.__pre_data__[p] = tplData[p];
                    }

                    setTimeout(function() {
                        var data = that.__pre_data__;
                        that.__pre_data__ = {};
                        var isObjEmpty = true;
                        for (var p in data){
                            isObjEmpty=false;
                        }

                        if (!isObjEmpty){
                            that.page.setData(data);
                        }
                    }, 100);

                }
            });
        }
    };
}
module.exports = config();