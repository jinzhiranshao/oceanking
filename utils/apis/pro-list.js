const _api = require('_api.js');
const app = getApp();
var config = function() {
    return Object.assign({}, _api, {
        property:'footer',
        modal:'small',
        prepareLoad:null,//func
        _isLoading: false,
        _isLoadAll: false,
        _nextPageIndex: 0,
       _options:{},
        
        onLoad: function(page) {
            var that = this;

            that.__load(page, {
                pageIndex: 1
            }, 

            function(res) {
                that._nextPageIndex = res.pageIndex+1;
                that._isLoading = false;
                that._isLoadAll = res.pageIndex >= res.totalPage;
            });
        },
        onReachBottom: function(page) {
            var that = this;

            if (that._isLoading || that._isLoadAll || !that._nextPageIndex) {
                return;
            }

            if(!that._options){
                that._options={}; 
            }

            that._options.pageIndex = that._nextPageIndex;

            that.__load(page, that._options , function(res) {
                that._nextPageIndex = res.pageIndex + 1;
                that._isLoading = false;
                that._isLoadAll = res.pageIndex >= res.totalPage;
            });
        },
        onPullDownRefresh: function(page) {
            this.onLoad(page);
        }, 
        onFooterChange: function(page, e) {
            var that = this;
           
            var options = {
                pageIndex: 1 ,
            };

            if (e.detail){
                for(var p in e.detail){
                    options[p] = e.detail[p];
                }
            }

            that.__load(page, options, function(res) {
                that._nextPageIndex = res.pageIndex + 1;
                that._isLoading = false;
                that._isLoadAll = res.pageIndex >= res.totalPage;
            });
        },
        __load: function(page, options = {}, callback) {
            var that = this;

            if (that._isLoading) { return; }
            that._isLoading = true;
            that._isLoadAll = false;
            this._options = options;

            if (that.modal) {
                options.modal = that.modal;
            }

            if (this.prepareLoad){
                this.prepareLoad(options);
            }

            for (var p in options) {
                if (options[p]&&typeof options[p] == 'object') {
                    options[p] = JSON.stringify(options[p]);
                }
            }

            var data = {
                [that.property+'._isNoneData']: false,
                [that.property +'._pageIndex']: options.pageIndex,
                [that.property +'._isDataLoading']: true
            };

            if (options.pageIndex==1){
                data[that.property +'.items'] = null;
            }

            page.setData(data);
           
            app.Util.network.POST(page,{
                url: app.getApi('wp/v1/store/category/data'),
                params: options,
                showLoading: 0,
                success: function(response) {
                    if (options.pageIndex == 1) {
                        page.setData({
                            [that.property +'.items']: response.items,
                            [that.property +'._isNoneData']: options.pageIndex == 1&&response.totalCount == 0,
                            [that.property +'._isDataLoading']: false
                        });
                    } else {
                        var data = {};
                        var start = page.data[that.property]&&page.data[that.property].items ? page.data[that.property].items.length : 0;
                        if (response.items) {
                            var index = 0;
                            for (var p in response.items) {
                                data[that.property +'.items[' + (index++ + start) + "]"] = response.items[p];
                            }
                        }
                        data[that.property +'._isNoneData'] = options.pageIndex == 1 &&response.totalCount == 0;
                        data[that.property +'._isDataLoading'] = false;
                        page.setData(data);
                    }
                    callback(response);
                },
                complete: function() {
                    if (that._isLoading){
                        that._isLoading = false;
                        page.setData({
                            [that.property + '._isDataLoading']: false
                        });
                    }
                },
            });
        }
    });
};

module.exports = config();