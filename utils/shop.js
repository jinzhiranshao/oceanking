var config = function () {
    return {
        add_to_cart:function(e){
        	var app = getApp();
        	var product_id =  typeof e.currentTarget.dataset.product_id=='undefined'?'':e.currentTarget.dataset.product_id;
        	var variation_id =  typeof e.currentTarget.dataset.variation_id=='undefined'?'':e.currentTarget.dataset.variation_id;
        	var that = this;
        	
            app.Util.network.POST(that,{
                url: getApi('product/v1/cart/add'),
                showLoading:1,
                params: {
                  encryptedData: e.detail.encryptedData,
                  iv: e.detail.iv,
                  rawData: e.detail.rawData,
                  signature: e.detail.signature,
                  code: res.code
                },
                success: data => {
                	if(!data){return;}
                	
                	app.Cart.set(data);
                	that.setData({
                		cart:data
                	});
                }
            });
        }
    };
};

module.exports = config();