const app = getApp();
var config = function() {
    return {
        __nav__: function(event) {
            var url = event.currentTarget.dataset.url;
            var action = event.currentTarget.dataset.action;
            this.__nav_to__(action, url);
        },
        __nav_to__: function(action, url, complete = null) {
            if (!url) {
                return;
            }

            if (!action) {
                action = 'navigate';
            }

            var that = this;
            if (that.__redirecting__) {
                if (complete) {
                    complete();
                }
                return;
            }

            if (!url.startsWith('/')) {
                url = '/' + url;
            }

            that.__redirecting__ = true;
            var urls = url.split('?');
            var pre = urls[0];
            if (pre.startsWith('/')) {
                pre = pre.substr(1);
            }

            if (pre == 'pages/login/index') {
                pre = '/package_c/pages/login/index';
            }

            if (app.config.menus && typeof app.config.menus[pre] != 'undefined') {
                action = 'switchTab';
            }

            var exit = app.apply_filters('nav_to', false, that, {
                action: action,
                pre: pre,
                url: url,
                complete: complete
            });
            if (exit) {
                return;
            }

            app.do_action('nav_to', that, {
                action: action,
                pre: pre,
                url: url,
                complete: complete
            });

            switch (action) {
                case 'navigateTo':
                case 'navigate':
                    wx.navigateTo({
                        url: url,
                        complete() {
                            that.__redirecting__ = false;
                            if (complete) {
                                complete();
                            }
                        }
                    });
                    return;
                case 'navigateBack':
                    that.__go_back__(1, function() {
                        that.__redirecting__ = false;
                        if (complete) {
                            complete();
                        }
                    });
                    return;
                case 'redirect':
                case 'redirectTo':
                    wx.redirectTo({
                        url: url,
                        complete() {
                            that.__redirecting__ = false;
                            if (complete) {
                                complete();
                            }
                        }
                    });
                    return;
                case 'switch':
                case 'switchTab':
                    wx.switchTab({
                        url: '/' + pre, //不是url
                        complete() {
                            that.__redirecting__ = false;
                            if (complete) {
                                complete();
                            }
                        }
                    });
                    return;
                default:
                    that.__redirecting__ = false;
                    if (complete) {
                        complete();
                    }
                    break;
            }
        },
        __common_event_tap__(event) {
            var action = event.currentTarget.dataset.action;
            var params = event.currentTarget.dataset.params;
            var url = event.currentTarget.dataset.url;
            var that = this;

            var exit = app.apply_filters('common_event_tap', false, that, {
                action: action,
                params: params,
                url: url
            });
            if (exit) {
                return;
            }

            app.do_action('common_event_tap', that, {
                action: action,
                params: params,
                url: url
            });

            if (that.__redirecting__) {
                that.__redirecting__ = false;
                return;
            }

            that.__redirecting__ = true;
            var newParams = {};
            if (params) {
                var row = params.split('&');
                for (var p in row) {
                    var items = row[p].split('=');
                    if (items.length != 2) {
                        continue;
                    }
                    newParams[items[0]] = items[1];
                }
            }

            switch (action) {
                case 'redirect':
                case 'redirectTo':
                case 'navigateTo':
                case 'navigate':
                case 'navigateBack':
                case 'switchTab':
                    that.__redirecting__ = false;
                    that.__nav_to__(action, url + (params ? ('?' + params) : ''), function() {
                        that.__redirecting__ = false;
                    });
                    return;
                case 'navigate-to-miniProgram':
                    var params = params ? params.split('|') : [];
                    var path = params.length > 1 ? params[1] : null;
                    if (path && path.startsWith('/')) {
                        path = path.substr(1);
                    }

                    if (!params.length) {
                        wx.showToast({
                            icon: 'none',
                            title: '跳转失败：跳转链接未设置APPID'
                        });
                        that.__redirecting__ = false;
                        break;
                    }

                    wx.navigateToMiniProgram({
                        appId: params[0],
                        path: path
                    });

                    break;
                case 'navigate-back-miniProgram':
                    wx.navigateBackMiniProgram({});
                    break
                case 'logout':
                    wx.showModal({
                        title: '提示',
                        content: '确认退出登录？',
                        success(res) {
                            if (res.confirm) {
                                app.logout();
                                wx.startPullDownRefresh({});
                            }
                        }
                    });
                    break;
                case 'make-phone-call':
                    wx.makePhoneCall({
                        phoneNumber: params
                    });
                    break;
                case 'scan-code':
                    wx.scanCode({
                        complete() {
                            that.__redirecting__ = false;
                        }
                    });
                    break;
                case 'check-for-update':
                    var updateManager = wx.getUpdateManager();
                    updateManager.onCheckForUpdate(function(res) {
                        // 请求完新版本信息的回调
                        if (!res.hasUpdate) {
                            wx.showToast({
                                icon: 'success',
                                title: '当前已是最新版本！'
                            });
                            return;
                        }

                        wx.showToast({
                            icon: 'none',
                            title: '您有一个新版本在下载中...'
                        });
                    });

                    updateManager.onUpdateReady(function() {
                        wx.showModal({
                            title: '更新提示',
                            content: '新版本已经准备好，是否重启应用？',
                            success(res) {
                                if (res.confirm) {
                                    updateManager.applyUpdate()
                                }
                            }
                        })
                    });
                    break;
                case 'custom-red-bag':
                    var params = {
                        id: params
                    };

                    var that = this;
                    app.Util.network.POST(that, {
                        url: app.getApi('product/v1/product/addCoupon'),
                        params: params,
                        showLoading: 1,
                        success() {
                            wx.showToast({
                                icon: 'success',
                                title: '领取成功！',
                            });
                        },
                        complete() {
                            that.__redirecting__ = false;
                        }
                    });
                    break;
                default:
                    that.__redirecting__ = false;
                    break;
            }

            setTimeout(function() {
                that.__redirecting__ = false;
            }, 500);
        },
        __bindgetphonenumber__(event) {
            var that = this;
            if (!that.__tmp_auth_code__) {
                that.__refresh_code__(function(code) {
                    that.__bindphonenumber__(event);
                });
                return;
            }
            that.__bindphonenumber__(event);
        },
        __bindphonenumber__(event) {
            if (event.detail.errMsg != 'getPhoneNumber:ok') {
                wx.showToast({
                    title: '您拒绝了授权！',
                    icon: 'none',
                    duration: 2000
                });
                return;
            }

            var that = this;
            var onSuccess = function(res) {
                app.Util.network.POST(that, {
                    showLoading: 1,
                    url: app.getApi('wp/v1/user/login2'),
                    params: {
                        encryptedData: encodeURIComponent(event.detail.encryptedData),
                        iv: encodeURIComponent(event.detail.iv),
                        code: encodeURIComponent(that.__tmp_auth_code__)
                    },
                    complete() {
                        that.__refresh_code__();
                    },
                    success: data => {
                        wx.showToast({
                            title: '登录成功！',
                            icon: 'success',
                            duration: 2000
                        });

                        wx.setStorage({
                            key: 'system:token',
                            data: data.token
                        });

                        app.tmpCart=null;
                        app.TOKEN = data.token;
                        that.__auth_back__();
                    }
                });
            };

            wx.checkSession({
                success: onSuccess,
                fail(res) {
                    that.__tmp_auth_code__ = null;
                    wx.showToast({
                        title: '登录失败:登录信息已过期',
                        icon: 'none',
                        duration: 2000
                    });
                }
            });
        },
        __bindgetuserinfo__(event) {
            var that = this;
            if (!that.__tmp_auth_code__) {
                that.__refresh_code__(function(code) {
                    that.__binduserinfo__(event);
                });
                return;
            }
            that.__binduserinfo__(event);
        },
        __binduserinfo__(event) {
            if (event.detail.errMsg != 'getUserInfo:ok') {
                wx.showToast({
                    title: '您拒绝了授权！',
                    icon: 'none',
                    duration: 2000
                });
                return;
            }

            var that = this;
            var onSuccess = function(res) {
                var type = event.currentTarget.dataset.params;
                if (!type || typeof type == 'undefined') {
                    type = '';
                }
                switch (type) {
                    case 'refresh':
                        app.Util.network.POST(that, {
                            showLoading: 1,
                            url: app.getApi('wp/v1/user/refresh'),
                            params: {
                                v: 2,
                                encryptedData: encodeURIComponent(event.detail.encryptedData),
                                iv: encodeURIComponent(event.detail.iv),
                                rawData: encodeURIComponent(event.detail.rawData),
                                signature: encodeURIComponent(event.detail.signature),
                                code: encodeURIComponent(that.__tmp_auth_code__)
                            },
                            complete() {
                                that.__refresh_code__();
                            },
                            success: data => {
                                wx.setStorage({
                                    key: 'system:token',
                                    data: data.token
                                });

                                app.tmpCart = null;
                                app.TOKEN = data.token;

                                wx.showToast({
                                    title: '资料已更新！',
                                    icon: 'success',
                                    duration: 2000
                                });
                                wx.startPullDownRefresh({});
                            }
                        });
                        break;
                    default:
                        app.Util.network.POST(that, {
                            showLoading: 1,
                            url: app.getApi('wp/v1/user/login'),
                            params: {
                                v: 2,
                                encryptedData: encodeURIComponent(event.detail.encryptedData),
                                iv: encodeURIComponent(event.detail.iv),
                                rawData: encodeURIComponent(event.detail.rawData),
                                signature: encodeURIComponent(event.detail.signature),
                                code: encodeURIComponent(that.__tmp_auth_code__)
                            },
                            complete() {
                                that.__refresh_code__();
                            },
                            success: data => {
                                wx.setStorage({
                                    key: 'system:token',
                                    data: data.token
                                });

                                app.tmpCart = null;
                                app.TOKEN = data.token;

                                var type = event.currentTarget.dataset.params;
                                if (!type || typeof type == 'undefined') {
                                    type = '';
                                }
                                wx.showToast({
                                    title: '登录成功！',
                                    icon: 'success',
                                    duration: 2000
                                });
                                that.__auth_back__();
                            }
                        });
                        break;
                }
            };

            wx.checkSession({
                success: onSuccess,
                fail(res) {
                    that.__refresh_code__();
                    wx.showToast({
                        title: '登录失败:登录信息已过期',
                        icon: 'none',
                        duration: 2000
                    });
                }
            });
        },
        __auth_back__() {
            var that = this;
            if (that.route != 'package_c/pages/login/index') {
                wx.startPullDownRefresh({});
                return;
            }

            that.__go_back__();
        },
        __go_back__(delta = 1, complete = null) {
            if (!complete) {
                complete = function() {}
            }

            var pages = getCurrentPages();
            if (pages && pages.length > 1) {
                wx.navigateBack({
                    delta: delta,
                    complete: complete
                });
            } else {
                wx.startPullDownRefresh({
                    complete: complete
                });
            }
        },
        __go_back_to_home__() {
            var menus = app.config.menus;
            if (!menus) {
                that.__go_back__(999);
                return;
            }

            for (var page in menus) {
                wx.switchTab({
                    url: "/" + page,
                });
                return;
            }
        },
        __refresh_code__: function(callback = null) {
            var that = this;
            that.__tmp_auth_code__ = null;

            wx.login({
                success(res) {
                    that.__tmp_auth_code__ = res.code;
                    if (callback) {
                        callback(res.code);
                    }
                },
                fail(res) {
                    console.error('登录失败:' + res.errMsg);
                }
            });
        },
        isTab() {
            return app.config.menus && typeof app.config.menus[this.route] != 'undefined';
        }
    };
};
module.exports = config();