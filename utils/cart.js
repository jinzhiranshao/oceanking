const app = getApp();
var config = function() {
    return {
        get: function(page, callback, refresh = false) {
            if (!app.isAuthorized()) {
                callback({
                    cart_contents_count: 0
                });
                return;
            }

            if (app.tmpCart && Object.keys(app.tmpCart).length && !refresh) {
                callback(app.tmpCart);
                return;
            }

            var current = this;
            app.Util.network.POST(page, {
                url: app.getApi('product/v1/checkout/cart/get'),
                showLoading: 0,
                params: {
                    include_product: page._cart_include_product ? 'yes' : 'no',
                    include_checkout: page._cart_include_checkout ? 'yes' : 'no'
                },
                success: data => {
                    current.show_in_navbar(data);
                    app.tmpCart = data;
                    callback(data);
                }
            });
        },
        show_in_navbar: function(cart) {
            if (!app.config.menus) {
                return;
            }
            var index = 0;
            var cart_index = -1;
            for (var page in app.config.menus) {
                if (page == 'pages/cart/index') {
                    cart_index = index;
                    break;
                }
                index++;
            }
            if (cart_index < 0) {
                return;
            }

            if (cart && cart.cart_contents_count) {
                app.config.__isCartTabBarBadgeShow__ = true;
                wx.setTabBarBadge({
                    index: cart_index,
                    text: cart.cart_contents_count.toString()
                });
            } else if (app.config.__isCartTabBarBadgeShow__) {
                wx.removeTabBarBadge({
                    index: cart_index,
                })
            }
        },
        add: function(page, product_id, variation_id, quantity, variation, callback) {
            var that = this;
            app.Util.network.POST(page, {
                url: app.getApi('product/v1/checkout/cart/add'),
                showLoading: 1,
                params: {
                    product_id: product_id,
                    variation_id: variation_id,
                    quantity: quantity,
                    variation: variation ? JSON.stringify(variation) : '',
                    formIds: JSON.stringify(app.formIds)
                },
                success: data => {
                    that.show_in_navbar(data);
                    app.tmpCart = data;
                    callback(data);
                },
                fail: function(res) {
                    wx.startPullDownRefresh({});
                }
            });
        },
        set_quantity: function(page, cart_item_key, quantity, callback) {
            var that = this;
            app.Util.network.POST(page, {
                url: app.getApi('product/v1/checkout/cart/set_quantity'),
                showLoading: 1,
                params: {
                    cart_item_key: cart_item_key,
                    quantity: quantity,
                    formIds: JSON.stringify(app.formIds)
                },
                success: data => {
                    that.show_in_navbar(data);
                    app.tmpCart = data;
                    callback(data);
                },
                fail: function(res) {
                    wx.startPullDownRefresh({});
                }
            });
        },
        remove: function(page, cart_item_key, callback) {
            var that = this;
            app.Util.network.POST(page, {
                url: app.getApi('product/v1/checkout/cart/remove'),
                showLoading: 1,
                params: {
                    cart_item_key: cart_item_key,
                    formIds: JSON.stringify(app.formIds)
                },
                success: data => {
                    that.show_in_navbar(data);
                    app.tmpCart = data;
                    callback(data);
                },
                fail: function(res) {
                    wx.startPullDownRefresh({});
                }
            });
        },
        clear(page, callback){
            var that = this;
            app.Util.network.POST(page, {
                url: app.getApi('product/v1/checkout/cart/clear'),
                showLoading: 1,
                params: {
                    formIds: JSON.stringify(app.formIds)
                },
                success: data => {
                    that.show_in_navbar(data);
                    app.tmpCart = data;
                    callback(data);
                },
                fail: function (res) {
                    wx.startPullDownRefresh({});
                }
            });
        },
    };
};

module.exports = config();