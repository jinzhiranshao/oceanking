const app = getApp();
var config = function () {
    return {
        onCopyPayLink: function () {
            var that = this;
            if (!that._payLinkUrl) {
                wx.showToast({
                    icon: 'none',
                    title: '拷贝失败：未找到支付链接！'
                });
                return;
            }

            wx.setClipboardData({
                data: that._payLinkUrl,
                success(res) {
                    wx.showToast({
                        title: '拷贝成功！'
                    });
                }
            })
        },
        __processCheckout: function (res) {
            var that = this;
            if (res.result && res.result == 'success') {
                that.__order_id__ = res.order_id;
                if (res.complete) {
                    that.onPaymentOver();
                    return;
                }

//线下付款，直接跳转支付成功页面
                if (
                    that.data.cart && that.data.cart.chosen_payment_method
                    &&
                    (
                        that.data.cart.chosen_payment_method=='cod'
                        || that.data.cart.chosen_payment_method =='bacs'
                        || that.data.cart.chosen_payment_method =='cheque'
                    )
                ){
                    that.onPaymentOver();
                    return;
                }

                //唤起微信支付
                if (res.jsapi) {
                    if (!res.jsapi.timeStamp || !res.jsapi.nonceStr || !res.jsapi.package || !res.jsapi.signType || !res.jsapi.paySign){
                        console.log(res);
                        wx.showToast({
                            icon:'none',
                            title: '支付通道异常！',
                        })
                        return;
                    }
                    
                    that.__stopOnShowEvent__ = true;
                    wx.requestPayment({
                        timeStamp: res.jsapi.timeStamp.toString(),
                        nonceStr: res.jsapi.nonceStr.toString(),
                        package: res.jsapi.package,
                        signType: res.jsapi.signType,
                        paySign: res.jsapi.paySign,
                        success() {
                            that.onPaymentOver();
                        },
                        fail(response) {
                            that.__stopOnShowEvent__ = false;
                            if (response.errMsg == 'requestPayment:fail cancel') {
                                that.__showErrorMsg('您已取消支付！');
                            } else if (response.errMsg == 'requestPayment:fail') {
                                var msg = '支付失败:' + JSON.stringify(response);
                                if (response.err_desc) {
                                    msg = '支付失败：' + response.err_desc;
                                }
                                that.__showErrorMsg(msg);
                            } else {
                                that.__showErrorMsg('支付失败：未知错误！');
                            }

                            wx.startPullDownRefresh({});
                        }
                    });
                    return;
                }

                if (res.url_paylink || res.redirect) {
                    that.__stopOnShowEvent__ = true;
                    that._payLinkUrl = res.url_paylink ? res.url_paylink : res.redirect;
                    var animation = wx.createAnimation({
                        duration: 150,
                        timingFunction: 'ease',
                        delay: 0
                    });

                    animation.height('340rpx').step();
                    var data = {
                        payDialogAnimation: animation.export(),
                        showPayDialog: true
                    };

                    that.setData(data);
                    return;
                }
            }
            
            if (res.result && res.result == 'failure') {
                wx.showToast({
                    title: res.messages && Array.isArray(res.messages) ? res.messages.join(',') : res.messages,
                    icon: 'none',
                    duration: 3000
                });
                return;
            }

            wx.showToast({
                title: '未知异常！',
                icon: 'none',
                duration: 3000
            });
        },
        onPayDialogClose: function () {
            var that = this;
            that._payLinkUrl = null;
            var animation = wx.createAnimation({
                duration: 150,
                timingFunction: 'ease',
                delay: 0
            })

            animation.height(0).step();
            that.setData({
                payDialogAnimation: animation.export()
            });

            setTimeout(function () {
                that.setData({
                    showPayDialog: false
                });
                that.onPaymentOver();
            }, 150);
        },
        __showErrorMsg: function (msg) {
            wx.showToast({
                title: msg,
                icon: 'none',
                duration: 3000
            });
        },
        onPaymentOver(){
            wx.startPullDownRefresh({
                
            });
        }
    };
};
module.exports = config();