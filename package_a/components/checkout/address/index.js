const app = getApp();
const route = require('../../../../utils/route.js');
Component({
    data: Object.assign({}, {
        apiAssets: app.config.apiAssets,
    }),
    properties: {
        cart:Object
    },
    options: {
        addGlobalClass: true
    },
    methods: Object.assign({}, route, {})
});