const app = getApp();
const route = require('../../../../utils/route.js');
Component({
    properties: {
        cart:Object
    },
    options: {
        addGlobalClass: true
    },
    data: Object.assign({},{
        apiAssets: app.config.apiAssets
    }),
    methods: Object.assign({}, route, {
        onCouponRemoveClick: function(e) {
           var coupon_code = e.currentTarget.dataset.code;
            var that = this;
            app.Util.network.POST(that,{
                url: app.getApi('product/v1/checkout/coupon/remove'),
                showLoading: 1,
                params: {
                    coupon_code: coupon_code
                },
                success: data => {
                    that.triggerEvent('couponchange', data, {
                        bubbles: true,
                        composed: true
                    });
                }
            });
        }
    })
});