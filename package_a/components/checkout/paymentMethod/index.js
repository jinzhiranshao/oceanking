const app = getApp();
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        cart: Object
    },
    options: {
        addGlobalClass: true
    },
    /**
     * 组件的方法列表
     */
    methods: {
        onPaymentMethodChange(e){
            this.__updatePaymentMethod(e.currentTarget.dataset.id);
        },
        onRadioChange: function(e) {
            this.__updatePaymentMethod(e.detail.value);
        },
        __updatePaymentMethod: function(payment_method) {
            var that = this;
            app.Util.network.POST(that,{
                url: app.getApi('product/v1/checkout/gateway/update'),
                params: {
                    payment_method: payment_method
                },
                showLoading: 1,
                success: data => {
                    that.setData({
                        ["cart.chosen_payment_method"]: payment_method
                    }, function() {
                        that.triggerEvent('paymentgatewaychange', data, {
                            bubbles: true,
                            composed: true
                        });
                    });
                }
            });
        }
    }
})