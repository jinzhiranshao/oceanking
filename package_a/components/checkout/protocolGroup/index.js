const route = require('../../../../utils/route.js');
Component({
    behaviors: ['wx://form-field'],
    properties: {
        privacy_policy:Object
    },
    options: {
        addGlobalClass: true
    },
    methods: Object.assign({}, route, {
        onSwitchChange(){
            this.setData({
                value: !this.data.value||this.data.value=='no' ? 'yes' : 'no'
            });
        },
        onChange:function(e){
            this.setData({
                value: !this.data.value || this.data.value == 'no' ? 'yes' : 'no'
            });
        }
    })
});