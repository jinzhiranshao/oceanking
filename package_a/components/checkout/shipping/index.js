// components/checkout/shipping/index.js
const app = getApp();
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        shippingInfo: Array
    },
    options: {
        addGlobalClass: true
    },
    methods: {
        onPickerChange: function(e) {
            var index = e.currentTarget.dataset.index;
            var method_index = e.detail.value;
            var that = this;
            app.Util.network.POST(that,{
                url: app.getApi('product/v1/checkout/shipping/update'),
                params: {
                    shipping_index: index,
                    shipping_method_id: this.data.shippingInfo[index].available_methods[method_index].id
                },
                showLoading: 1,
                success: data => {
                    that.setData({
                        ["shippingInfo[" + index + "].chosen_index"]: method_index
                    }, function() {
                        that.triggerEvent('shippingchange', data, {
                            bubbles: true,
                            composed: true
                        });
                    });

                }
            });
        }
    }
})