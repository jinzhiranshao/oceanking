Component({
    properties: {
        cart: Object,
        firstItemId:Number
    },
    options: {
        addGlobalClass: true
    },
    data: {
        expandAmount:true
    },
    methods: {
        onTapExpandAmountDetail: function() {
            var e = !this.data.expandAmount;
            this.setData({
                expandAmount: e
            });
        }
    }
});