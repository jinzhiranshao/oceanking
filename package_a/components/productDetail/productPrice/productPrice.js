const app = getApp();
Component({
    data: Object.assign({}, app.config.theme, {
        apiAssets: app.config.apiAssets
    }),
    options: {
        addGlobalClass: true
    },
    properties: {
        productInfo: Object
    },
    methods: {
        onShareFailed(e){
            console.log(e);
            this.setData({
                ['productInfo.shareInfo']:null
            });
        },
        favProductSize: function() {
            var that = this;
            var params = {
                id: that.data.productInfo.id
            };
            app.Util.network.POST(that,{
                url: app.getApi('product/v1/product/changeFav'),
                params: params,
                showLoading: 1,
                success: function(data) {
                    that.setData({
                        "productInfo.isFav": !that.data.productInfo.isFav
                    });
                }
            });
        }
    }
});