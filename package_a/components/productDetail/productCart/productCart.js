const app = getApp();
const cartApi = require('../../../../utils/cart.js');
const route = require('../../../../utils/route.js');
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        productInfo: Object,
        cart: Object,
        isIphoneX: Boolean
    },
    options: {
        addGlobalClass: true
    },
    data: Object.assign({}, {
        showSkuModal: false,
        apiAssets: app.config.apiAssets,
        icon_cart_cut: app.config.icons.icon_cart_cut,
        icon_cart_add: app.config.icons.icon_cart_add,
        selected_variation_atts: {}
    }),
    methods: Object.assign({}, route, {
        addNow: function(e) {
            var formId = e.detail.formId;
            if (formId && !formId.startsWith('requestFormId:fail')) {
                app.formIds[formId] = Math.round((new Date()).getTime() / 1000);
            }

            this.addToCart('add-cart');
        },
        redirectToCart: function(e) {
            var formId = e.detail.formId;
            if (formId && !formId.startsWith('requestFormId:fail')) {
                app.formIds[formId] = Math.round((new Date()).getTime() / 1000);
            }

            this.__nav_to__('navigateTo', '/pages/cart/index');
        },
        buyNow: function(e) {
            var formId = e.detail.formId;
            if (formId && !formId.startsWith('requestFormId:fail')) {
                app.formIds[formId] = Math.round((new Date()).getTime() / 1000);
            }
            this.addToCart('buy-now');
        },
        addToCart: function(addCartType) {
            var that = this;
            var product = that.data.productInfo;
            if (!product) {
                return;
            }

            if (!that.__temp_sku_dialog_ani__){
                that.__temp_sku_dialog_ani__ = wx.createAnimation({
                    duration: 150,
                    timingFunction: 'ease',
                    delay: 0
                });
            }
            
            that.__temp_sku_dialog_ani__.bottom(0).step();
            var data = {
                addCartType: addCartType,
                showSkuModal: true,
                skuAnimation: that.__temp_sku_dialog_ani__.export()
            };

            if (!that.data.currentVar) {
                data.currentVar = that.__getDefaultCurrentVar();
            }

            that.setData(data);
        },
        __getDefaultCurrentVar: function() {
            var product = this.data.productInfo;
            if (!product) {
                return null;
            }

            var quantity = 1;
            if (this.data.cart && this.data.cart.items && this.data.cart.items[product.id]) {
                quantity = this.data.cart.items[product.id].quantity;
            }
            if (quantity < product.min_qty) {
                quantity = product.min_qty;
            }
            if (product.max_qty !== '' && product.max_qty >= 0 && quantity > product.max_qty) {
                quantity = product.max_qty;
            }

            return {
                id: product.id,
                variation_id: null,
                quantity: quantity,
                sku: product.sku,
                image: product.image,
                title: product.title,
                price_html: product.price_html,
                min_qty: product.min_qty,
                max_qty: product.max_qty
            };
        },
        __getVariation(variation_id) {
            if (!this.data.productInfo ||
                this.data.productInfo.type != 'variable' ||
                !this.data.productInfo.available_variations) {
                return null;
            }

            for (var index in this.data.productInfo.available_variations) {
                if (this.data.productInfo.available_variations[index].variation_id == variation_id) {
                    return this.data.productInfo.available_variations[index];
                }
            }

            return null;
        },
        __getCartItemByAtts(atts) {
            var product = this.data.productInfo;
            if (!product) {
                return null;
            }
            if (this.data.cart && this.data.cart.items) {
                for (var index in this.data.cart.items) {
                    var cartItem = this.data.cart.items[index];
                    if (cartItem && cartItem.variation && cartItem.product_id == product.id) {
                        var compare = true;
                        for (var vk in cartItem.variation) {
                            if (!atts[vk] || cartItem.variation[vk] != atts[vk]) {
                                compare = false;
                                break;
                            }
                        }

                        if (compare) {
                            return cartItem;
                        }
                    }
                }
            }
            return null;
        },
        __getCartItemByProduct(product_id) {
            if (this.data.cart && this.data.cart.items) {
                for (var index in this.data.cart.items) {
                    var cartItem = this.data.cart.items[index];
                    if (cartItem && cartItem.product_id == product_id) {
                        return cartItem;
                    }
                }
            }
            return null;
        },
        __getVariationByAtts(atts) {
            var product = this.data.productInfo;
            if (product && product.available_variations) {
                for (var index in product.available_variations) {
                    var ava = product.available_variations[index];
                    if (!ava.attributes) {
                        continue;
                    }

                    var compare = true;
                    for (var pt in ava.attributes) {
                        if (ava.attributes[pt] === '' || ava.attributes[pt] === null) {
                            continue;
                        }

                        if (!atts[pt] || ava.attributes[pt] != atts[pt]) {
                            compare = false;
                            break;
                        }
                    }

                    if (compare) {
                        return ava;
                    }
                }
            }
            return null;
        },
        onProductAttSelectChange: function(e) {
            var selected_variation_atts = e.detail;
            if (!selected_variation_atts) {
                this.setData({
                    selected_variation_atts: selected_variation_atts,
                    currentVar: this.__getDefaultCurrentVar()
                });
                return;
            }

            var product = this.data.productInfo;
            if (product.type != 'variable') {
                return;
            }

            var title = '';
            var index = 0;
            for (var pop in selected_variation_atts) {
                var item = product.attributes1[decodeURIComponent(pop.substr(10))];
                if (!item) {
                    continue;
                }
                var option = selected_variation_atts[pop];
                if (!item.options || !item.options[option]) {
                    continue;
                }

                if (index++ != 0) {
                    title += " ";
                }
                title += '"' + item.options[option].name + '"';
            }

            var currentVar = this.__getVariationByAtts(selected_variation_atts);
            var currentCartItem = this.__getCartItemByAtts(selected_variation_atts);
            if (!currentVar) {
                return;
            }

            var quantity = 1;
            if (currentCartItem) {
                quantity = currentCartItem.quantity;
            }
            if (quantity < currentVar.min_qty) {
                quantity = currentVar.min_qty;
            }
            if (currentVar.max_qty !== '' && currentVar.max_qty >= 0 && quantity > currentVar.max_qty) {
                quantity = currentVar.max_qty;
            }

            this.setData({
                selected_variation_atts: selected_variation_atts,
                currentVar: {
                    id: this.data.productInfo.id,
                    quantity: quantity,
                    variation_id: currentVar.variation_id,
                    image: currentVar.image,
                    sku: currentVar.sku,
                    title: title,
                    price_html: currentVar.price_html,
                    min_qty: currentVar.min_qty,
                    max_qty: currentVar.max_qty
                }
            });
        },
        onSkuModalCloseClick: function() {
            var that = this;
            if (!that.__temp_sku_dialog_ani__){return;}
            that.__temp_sku_dialog_ani__.bottom(-999).step();
            that.setData({
                showSkuModal: false,
                skuAnimation: that.__temp_sku_dialog_ani__.export()
            });
        },
        onConfirmPay: function(e) {
            var formId = e.detail.formId;
            if (formId && !formId.startsWith('requestFormId:fail')) {
                app.formIds[formId] = Math.round((new Date()).getTime() / 1000);
            }

            if (!this.data.currentVar) {
                return;
            }

            if (!(this.data.productInfo.type != 'variable' || this.data.currentVar.variation_id)) {
                return
            }

            this.__onConfirmPay();
        },
        onQuantityChange: function(e) {
            var quantity = e.detail.value;
            if (!this.data.currentVar) {
                this.setData({
                    ["currentVar.quantity"]: 1
                });
                return
            }

            if (!(this.data.productInfo.type != 'variable' || this.data.currentVar.variation_id)) {
                this.setData({
                    ["currentVar.quantity"]: 1
                });
                return
            }

            if (quantity < this.data.currentVar.min_qty) {
                quantity = this.data.currentVar.min_qty;
            }

            if (this.data.currentVar.max_qty !== '' && quantity >= this.data.currentVar.max_qty && this.data.currentVar.max_qty >= 0) {
                quantity = this.data.currentVar.max_qty;
            }

            this.setData({
                ["currentVar.quantity"]: quantity
            });
        },
        onPurchaseQuantityCut: function() {
            if (!this.data.currentVar) {
                return
            }

            if (!(this.data.productInfo.type != 'variable' || this.data.currentVar.variation_id)) {
                return
            }

            var qty = this.data.currentVar.quantity;
            qty--;
            if (qty < this.data.currentVar.min_qty) {
                qty = this.data.currentVar.min_qty;
            }
            if (this.data.currentVar.max_qty !== '' && qty >= this.data.currentVar.max_qty && this.data.currentVar.max_qty >= 0) {
                qty = this.data.currentVar.max_qty;
            }

            this.setData({
                ["currentVar.quantity"]: qty
            });
        },
        onPurchaseQuantityAdd: function() {
            if (!this.data.currentVar) {
                return
            }
            if (!(this.data.productInfo.type != 'variable' || this.data.currentVar.variation_id)) {
                return
            }

            var qty = this.data.currentVar.quantity;
            qty++;
            if (qty < this.data.currentVar.min_qty) {
                qty = this.data.currentVar.min_qty;
            }
            if (this.data.currentVar.max_qty !== '' && qty >= this.data.currentVar.max_qty && this.data.currentVar.max_qty >= 0) {
                qty = this.data.currentVar.max_qty;
            }

            this.setData({
                ["currentVar.quantity"]: qty
            });
        },
        __onConfirmPay: function() {
            var that = this;

            var onSuccess = function(res) {
                that.onSkuModalCloseClick();
                that.triggerEvent('addcart', res, {
                    bubbles: true,
                    composed: true
                });

                switch (that.data.addCartType) {
                    case 'add-cart':
                    default:
                        wx.showToast({
                            title: '加入成功！',
                            icon: 'success',
                            duration: 2500
                        })
                        return;
                    case 'buy-now':
                        that.__nav_to__('navigateTo', '/pages/cart/index');
                        return;
                }
            };

            var product = this.data.productInfo;
            var currentVar = this.data.currentVar;
            if (!product || !currentVar) {
                return;
            }

            var currentCartItem = null;
            var selected_variation_atts = {};

            switch (product.type) {
                case 'variable':
                    selected_variation_atts = this.data.selected_variation_atts;
                    currentCartItem = this.__getCartItemByAtts(selected_variation_atts);
                    break;
                case 'simple':
                    currentCartItem = this.__getCartItemByProduct(product.id);
                    break;
                default:
                    wx.showToast({
                        icon: 'none',
                        title: '当前产品类型暂不支持在线购买！',
                    });
                    return false;
            }

            if (currentCartItem) {
                cartApi.set_quantity(that, currentCartItem.key, currentVar.quantity, onSuccess);
            } else {
                cartApi.add(that, product.id, currentVar.variation_id, currentVar.quantity, selected_variation_atts, onSuccess);
            }
        },
        onShareFailed(e) {
            console.log(e);
            this.setData({
                ['productInfo.shareInfo']: null
            });
        },
        favProductSize: function() {
            var that = this;
            var params = {
                id: that.data.productInfo.id
            };
            app.Util.network.POST(that, {
                url: app.getApi('product/v1/product/changeFav'),
                params: params,
                showLoading: 1,
                success: function(data) {
                    that.setData({
                        "productInfo.isFav": !that.data.productInfo.isFav
                    });
                }
            });
        }
    })
})