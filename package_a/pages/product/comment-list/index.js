const app = getApp();
const pageS = require('../../../../utils/pageS.js');
const comment = require('../../../../templates/comment/comment.js');

Page(
    Object.assign({}, comment, pageS, {
        data: Object.assign({}, comment.data, {
            apiAssets: app.config.apiAssets,
            icon_none_comment_items: app.config.icons.icon_none_comment_items
        }),
        onReady() {
            var that = this;
            wx.getStorage({
                key: 'post:' + that._options.id + ':pwd',
                success: function (res) {
                    if (res && res.data) {
                        that.setData({
                            'article.id': that._options.id,
                            'article.password': res.data,
                            'article.type': 'product',
                        }, function () {
                            that.getCommentInfo();
                        });
                    }
                },
                fail: function () {
                    that.setData({
                        'article.id': that._options.id,
                        'article.password': null,
                        'article.type': 'product',
                    }, function () {
                        that.getCommentInfo();
                    });
                }
            });
        },
        onPullDownRefresh: function() {
            this.onReady();
        }
    })
);