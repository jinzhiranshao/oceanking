const app = getApp();
const pageS = require('../../../utils/pageS.js');
Page(Object.assign({}, pageS, {
    data: {
        isPageLoading: true,
    },
    _isPageCache: false,
    __onShow: function(refresh = true) {
        var options = this._options;
        var that = this;
        that._onLoad('wp/v1/user/prebind', options, refresh, function(that, data) {
            data.isPageLoading = false;
            data.isCan = true;
            that.setData(data);
        });
    },
    bindOnConfirm() {
        var that = this;
        var options = this._options;
        app.Util.network.POST(that, {
            url: app.getApi('wp/v1/user/dobind'),
            params: options,
            showLoading: 1,
            success: data => {
                that.setData(data);
            }
        });
    },
}))