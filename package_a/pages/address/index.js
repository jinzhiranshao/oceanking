const app = getApp();
const pageS = require('../../../utils/pageS.js');
Page(Object.assign({}, pageS,  {
    _isPageCache: false,
    _isRedirectedToDetail:false,
    data:{
        isDataLoading:true,
        apiAssets: app.config.apiAssets
    },
    onPullDownRefresh: function () {
        this._load();
    },
    __onShow(){
        if (!app.isAuthorized()) {
            this.__nav_to__('redirect', '/package_c/pages/login/index');
            return;
        }
        this._load();
    },
    _load: function() {
        var options = this._options;
        var that = this;
        that._onLoad('wp/v1/user/addresslist', options, true, function(that, data) {
            if (!that._isRedirectedToDetail){
                if (!data.items || !data.items.length){
                    that._isRedirectedToDetail = true;
                    that.__nav_to__('navigateTo', '/package_a/pages/address-detail/index');
                }
            }
            data.isDataLoading = false;
            that.setData(data);
        });
    },
    onConfirmAddress(e){
        var id = e.currentTarget.dataset.id;
        var that = this;
        app.Util.network.POST(that,{
            url: app.getApi('wp/v1/user/confirmAddress'),
            params: {
                id: id
            },
            showLoading: 1,
            success: data => {
                that.__go_back__();
            }
        });
    },
    obAddressRemove(e){
        var id = e.currentTarget.dataset.id;
        var that = this;

        wx.showModal({
            title: '提示',
            content: '即将删除当前送货地址，确认执行？',
            success(res) {
                if (res.confirm) {
                    app.Util.network.POST(that,{
                        url: app.getApi('wp/v1/user/removeAddress'),
                        params: {
                            id: id
                        },
                        showLoading: 1,
                        success: data => {
                            wx.startPullDownRefresh({});
                        }
                    });
                }
            }
        });
    }
}))