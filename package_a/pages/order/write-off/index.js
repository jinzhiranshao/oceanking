
const app = getApp();
const pageS = require('../../../../utils/pageS.js');
const orderActions = require('../order-actions.js');
Page(Object.assign({}, pageS, orderActions, {
  _isPageCache: false,
  data: {
    isDataLoading: true,
    disabled: true
  },
  onReady: function (refresh = false) {
    var options = this._options;
    var that = this;
    that._onLoad('payment/v1/order/write-off', options, refresh, function (that, data) {
      data.isDataLoading = false;
      if(data){
        that.setData({
          disabled: false
        })
      }
      console.log(data)
      that.setData(data);
    });
  },
  writeOff:function(){
    var options = this._options;
    var that = this;
    app.Util.network.POST(that, {
      url: app.getApi('wp/v1/user/shopmanager/order/confirm/received'),
      params: options,
      showLoading: 0,
      success: data => {
        if(data == true){
          this.__nav_to__('navigateTo', 'pages/account/index');
        }
      },
      final: function () {
        that.__stopOnShowEvent__ = false;
      }
    });
  },
  onSkuModalCloseClick: function () {
    var that = this;
    var animation = wx.createAnimation({
      duration: 150,
      timingFunction: 'ease',
      delay: 0
    })

    animation.translateY(that._modalHeight).step();
    that.setData({
      skuAnimation: animation.export()
    });
    setTimeout(function () {
      animation.translateY(0).step();
      that.setData({
        skuAnimation: animation.export(),
        showSkuModal: false
      });
    }, 150);
  },
  __canFundLooper(data) {
    var that = this;

    if (!data.orderInfo.is_can_refund) {
      that.setData({
        is_can_refund: false
      });
      return;
    }

    that.setData({
      is_can_refund: data.orderInfo.is_can_refund.deadline >= data.orderInfo.is_can_refund.now
    });
  },
  __payExpiredLooper: function (data) {
    var that = this;

    if (that.__date_expired_loop_timer__) {
      clearInterval(that.__date_expired_loop_timer__);
      that.__date_expired_loop_timer__ = null;
    }

    if (!data.orderInfo || data.orderInfo.date_expired <= 0 || data.orderInfo.status != 'pending') {
      that.setData({
        date_expired_txt: null
      });
      return;
    }

    that.__date_expired__ = data.orderInfo.date_expired;
    that.__date_expired_loop_timer__ = setInterval(function () {
      if (that.__date_expired__ < 0 || !that.data.orderInfo || that.data.orderInfo.status != 'pending') {
        clearInterval(that.__date_expired_loop_timer__);
        that.__date_expired_loop_timer__ = null;
        that.setData({
          date_expired_txt: null
        });
        return;
      }

      var minutes = Math.floor(that.__date_expired__ / 60);
      var seconds = Math.floor(that.__date_expired__ % 60);
      that.setData({
        date_expired_txt: (minutes < 10 ? ('0' + minutes) : minutes) + '分' + (seconds < 10 ? ('0' + seconds) : seconds) + '秒'
      });

      that.__date_expired__--;


    }, 1000);
  },
  __orderLoopQuery: function () {
    var that = this;
    if (!that._options.checkorder || 'Y' != that._options.checkorder) {
      that._options.checkorder = 'N';
      return;
    }

    if (!this.data.orderInfo || !this.data.orderInfo.is_can_query_payment) {
      that._options.checkorder = 'N';
      return;
    }

    var now = new Date().getTime();
    if (that.__first_check_time && (now - that.__first_check_time) > 60 * 1000) {
      that._options.checkorder = 'N';
      return;
    }

    setTimeout(function () {
      that.onLoad(that._options);
    }, 5000);
  },
  onUnload: function () {
    this._options.checkorder = 'N';

    var that = this;
    if (that.__date_expired_loop_timer__) {
      clearInterval(that.__date_expired_loop_timer__);
      that.__date_expired_loop_timer__ = null;
    }
  }
}))