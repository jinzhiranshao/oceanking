const app = getApp();
const pageS = require('../../../../utils/pageS.js');
const utilMd5 = require('../../../../utils/md5.js');
const orderActions = require('../order-actions.js');

Page(Object.assign({}, pageS, orderActions, {
    _isPageCache: true,
    data: {
        apiAssets: app.config.apiAssets,
        isPageLoading: true,
        placeholderList: [0, 1, 2, 3, 5, 6],
        items: {},
        current_status_index: -1
    },
    __preDataLoadCacheKey(route, request) {
        var cache_key = route;
        cache_key = 'pages:' + utilMd5.hexMD5(cache_key);
        return cache_key;
    },
    onShow() {
      wx.startPullDownRefresh({})
    },
    onReady(refresh = false) {
        var options = this._options;
        var that = this;
        
        that._onLoad('payment/v1/order/statuses', options, refresh, function(that, data) {
            let orderTotal;
            let statusItems = data.items;
            data.total = 0;
            statusItems.forEach(function(value,index){
              data.total += value.count;
              console.log(data.total);
            });
            that.setData({
                isPageLoading: false,
                navHeight: data.navHeight,
                orderStatusList: data.items,
                orderTotal: data.total,
            }, function() {
                that.__get_order_list({
                    pageIndex: 1,
                    status: options.status ? options.status : (this.data.current_status ? this.data.current_status : 'wc-all')
                });
            });
        });
    },
    onBacktolast(){
        this.__nav_to__('redirect', '/pages/account/index');
    },
    onChangeStatus: function(e) {
        this._options.status = e.currentTarget.dataset.status;
        this.__get_order_list({
            status: e.currentTarget.dataset.status,
            pageIndex: 1
        });
        wx.startPullDownRefresh({})
    },
    __get_order_list: function(params, callback = null) {
        var that = this;
        if (that.data.isDataLoading) {
            return;
        }

        var current_status_index = -2;
        if (this.data.orderStatusList) {
            for (var index in this.data.orderStatusList) {
                if (this.data.orderStatusList[index].status == params.status) {
                    current_status_index = index - 1;
                    break;
                }
            }
        }

        if (current_status_index <= -2) {
            current_status_index = -1;
        } else if (current_status_index < 0) {
            current_status_index = 0;
        }

        var re = {
            current_status_index: current_status_index,
            current_status: params.status,
            pageIndex: params.pageIndex,
            isDataLoading: true,
            isNoneData: false
        };
        if (params.pageIndex == 1) {
            re.orderList = null;
        }
        that.setData(re);

        params.v = '1.0.1';
        app.Util.network.GET(that, {
            url: app.getApi('payment/v1/order/list'),
            params: params,
            showLoading: 0,
            complete: function() {
                if (that.data.isDataLoading) {
                    that.setData({
                        isDataLoading: false
                    });
                }
            },
            success: function(res) {
                if (res.page_index == 1) {
                    that.setData({
                        isDataLoading: false,
                        orderList: res.items,
                        isLoadDataAll: res.page_index >= res.page_count,
                        isNoneData: res.page_index == 1 && res.total_count == 0
                    }, callback);
                } else {
                    var data = {
                        isDataLoading: false,
                        isLoadDataAll: res.page_index >= res.page_count,
                        isNoneData: res.page_index == 1 && res.total_count == 0
                    };
                    if (res.items) {
                        var start = that.data.orderList ? that.data.orderList.length : 0
                        for (var index = 0; index < res.items.length; index++) {
                            data['orderList[' + (start + index) + ']'] = res.items[index];
                        }
                        that.setData(data, callback);
                    }
                }

            }
        });
    },
    onReachBottom: function() {
        var pageIndex = this.data.pageIndex;
        if (!this.data.isLoadDataAll) {
            this.__get_order_list({
                status: this.data.current_status,
                pageIndex: pageIndex + 1
            });
        }
    }
}))