const app = getApp();
const checkoutApi = require('../../templates/checkout/index.js');
var config = function() {
    return Object.assign({}, checkoutApi, {
        onDialogDoAction(e){
            var title = e.currentTarget.dataset.title;
            var confirm = e.currentTarget.dataset.confirm;
            var cancel = e.currentTarget.dataset.cancel;
            var action = e.currentTarget.dataset.action;
            var order_id = e.currentTarget.dataset.id;

            var params = {
                id: order_id
            };

            var that = this;

            wx.showModal({
                title:'提示',
                content: title,
                confirmText: confirm,
                cancelText: cancel,
                success(res) {
                    if (res.confirm) {
                        app.Util.network.POST(that, {
                            url: app.getApi(action),
                            params: params,
                            showLoading: 1,
                            success: data => {
                                if(data.confirm){
                                    wx.showModal({
                                        title: '提示',
                                        content: data.confirm.title,
                                        showCancel:false,
                                        confirmText: data.confirm.confirm_txt ? data.confirm.confirm_txt:'确定',
                                        success(res) {
                                            wx.startPullDownRefresh({});
                                        }
                                    });
                                    return;
                                }
                                wx.startPullDownRefresh({});
                            }
                        });
                    }
                }
            });
        },
        onRePayOrder: function(e) {
            var that = this;
            app.Util.network.POST(that, {
                url: app.getApi('payment/v1/order/repay'),
                params: {
                    id: e.currentTarget.dataset.id
                },
                showLoading: 1,
                success: data => {
                    that.__processCheckout(data);
                }
            });
        }
    })
};
module.exports = config();