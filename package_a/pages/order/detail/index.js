const app = getApp();
const pageS = require('../../../../utils/pageS.js');
const orderActions = require('../order-actions.js');
Page(Object.assign({}, pageS, orderActions,{
    _isPageCache: false,
    data: {
        isDataLoading:true,
        apiAssets: app.config.apiAssets,
        icon_status_header_bg: app.config.icons.icon_status_header_bg,
        shippingInfo: {}
    },
    onReady: function(refresh = false) {
        var options = this._options;
        var that = this;

        that._onLoad('payment/v1/order/index', options, refresh, function (that, data) {
            that.__payExpiredLooper(data);
            that.__canFundLooper(data);
            data.isDataLoading = false;
            that.setData(data);
            that.__orderLoopQuery();
            //加载物流信息
            that.onLoadShipping();
        });
    },
    //加载物流信息
    onLoadShipping: function() {
        var that = this;
        var params = {
            id: that.data.orderInfo.id
        };
        app.Util.network.POST(that, {
            url: app.getApi('product/v1/shipping'),
            params: params,
            showLoading: 0,
            success: function(data) {
                var shippingInfo = Object.assign({}, that.data.shippingInfo, data);
                if (data.detail && data.detail.length) {
                    shippingInfo.current = data.detail[data.detail.length - 1].AcceptStation;
                } else {
                    shippingInfo.current = data.status;
                }
                that.setData({
                    shippingInfo: shippingInfo
                });
            }
        });
    },
    //查看物流信息
    viewShipping: function() {
        var that = this;
        var shippingInfo = that.data.shippingInfo;
        if (shippingInfo.detail.length <= 0) {
            return;
        }

        app.getSystemInfo(function(res) {
            // 创建一个动画实例
            var animation = wx.createAnimation({
                // 动画持续时间
                duration: 150,
                // 定义动画效果，当前是匀速
                timingFunction: 'ease',
                delay: 0
            });

            var dpx = 750 / res.windowWidth;
            // 先在y轴偏移，然后用step()完成一个动画
            that._modalHeight = 400 * dpx;
            animation.translateY(that._modalHeight).step();
            var purchaseQuantity = 1;
            // 用setData改变当前动画
            var data = {
                skuAnimation: animation.export(),
                showSkuModal: true
            };

            that.setData(data);

            // 设置setTimeout来改变y轴偏移量，实现有感觉的滑动
            setTimeout(function() {
                animation.translateY(0).step()
                that.setData({
                    skuAnimation: animation.export()
                });
            }, 150);
        });

    },
    onSkuModalCloseClick: function() {
        var that = this;
        var animation = wx.createAnimation({
            duration: 150,
            timingFunction: 'ease',
            delay: 0
        })

        animation.translateY(that._modalHeight).step();
        that.setData({
            skuAnimation: animation.export()
        });
        setTimeout(function() {
            animation.translateY(0).step();
            that.setData({
                skuAnimation: animation.export(),
                showSkuModal: false
            });
        }, 150);
    },
    __canFundLooper(data) {
        var that = this;
     
        if (!data.orderInfo.is_can_refund) {
            that.setData({
                is_can_refund: false
            });
            return;
        }

        that.setData({
            is_can_refund: data.orderInfo.is_can_refund.deadline >= data.orderInfo.is_can_refund.now
        });
    },
    __payExpiredLooper: function(data) {
        var that = this;

        if (that.__date_expired_loop_timer__) {
            clearInterval(that.__date_expired_loop_timer__);
            that.__date_expired_loop_timer__ = null;
        }

        if (!data.orderInfo || data.orderInfo.date_expired <= 0 || data.orderInfo.status != 'pending') {
            that.setData({
                date_expired_txt: null
            });
            return;
        }

        that.__date_expired__ = data.orderInfo.date_expired;
        that.__date_expired_loop_timer__ = setInterval(function() {
            if (that.__date_expired__ < 0 || !that.data.orderInfo || that.data.orderInfo.status != 'pending') {
                clearInterval(that.__date_expired_loop_timer__);
                that.__date_expired_loop_timer__ = null;
                that.setData({
                    date_expired_txt: null
                });
                return;
            }

            var minutes = Math.floor(that.__date_expired__ / 60);
            var seconds = Math.floor(that.__date_expired__ % 60);
            that.setData({
                date_expired_txt: (minutes < 10 ? ('0' + minutes) : minutes) + '分' + (seconds < 10 ? ('0' + seconds) : seconds) + '秒'
            });

            that.__date_expired__--;


        }, 1000);
    },
    __orderLoopQuery: function() {
        var that = this;
        if (!that._options.checkorder || 'Y' != that._options.checkorder) {
            that._options.checkorder = 'N';
            return;
        }

        if (!this.data.orderInfo || !this.data.orderInfo.is_can_query_payment) {
            that._options.checkorder = 'N';
            return;
        }

        var now = new Date().getTime();
        if (that.__first_check_time && (now - that.__first_check_time) > 60 * 1000) {
            that._options.checkorder = 'N';
            return;
        }

        setTimeout(function() {
            that.onLoad(that._options);
        }, 5000);
    },
    onUnload: function() {
        this._options.checkorder = 'N';

        var that = this;
        if (that.__date_expired_loop_timer__) {
            clearInterval(that.__date_expired_loop_timer__);
            that.__date_expired_loop_timer__ = null;
        }
    }
}))