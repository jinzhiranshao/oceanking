const app = getApp();
const pageS = require('../../../../utils/pageS.js');
Page(Object.assign({}, pageS, {
    _isPageCache: false,
    data: {
        isDataLoading: true,
        apiAssets: app.config.apiAssets
    },
    onReady: function (refresh = false) {
        var options = this._options;
        var that = this;

        that._onLoad('payment/v1/order/refund-m', options, refresh, function (that, data) {
            data.isDataLoading = false;
            wx.setNavigationBarTitle({
                title: data.status_txt,
            });
            that.setData(data);
        });
    },
    previewImage(e){
        var index = e.currentTarget.dataset.index;
        var refund_uploads = this.data.refund_uploads;

        wx.previewImage({
            current:index,
            urls: refund_uploads,
        })
    }
}))