import {
    Index
} from './index-model.js';
var IndexModel = new Index();


const app = getApp();
const pageS = require('../../../../utils/pageS.js');
Page(Object.assign({}, pageS, {
    _isPageCache: false,
    data: {
        refund_reason: 0,
        isDataLoading: true,
        status_cancel_list: 'wrest-cancel-refund',
        apiAssets: app.config.apiAssets
    },
    onReady: function(refresh = false) {
        var options = this._options;
        var that = this;

        that._onLoad('payment/v1/order/pre-refund', options, refresh, function(that, data) {
            data.isDataLoading = false;
            that.setData(data);
        });
    },
    onRefundReasonChange(e) {
        this.setData({
            refund_reason: e.detail.value
        });
    },
    onSubmit(e) {
        var that = this;
        var form = e.detail.value;
        var formId = e.detail.formId;
        if (formId && !formId.startsWith('requestFormId:fail')) {
            app.formIds[formId] = Math.round((new Date()).getTime() / 1000);
        }
        form.id = this.data.orderInfo.id;
        
        var upload=[];
        if (that.data.uploaded){
            for (var p in that.data.uploaded){
                upload.push(that.data.uploaded[p].id);
            }
        }
        form.uploaded = upload;
        wx.showModal({
            content: that.data.dialog_title,
            confirmText: '确定退货',
            cancelText: '暂不退货',
            success(res) {
                if (res.confirm) {
                    app.Util.network.POST(that, {
                        url: app.getApi('payment/v1/order/refund'),
                        params: form,
                        showLoading: 1,
                        success: data => {
                            wx.showModal({
                                content: data.confirm.title,
                                showCancel: false,
                                confirmText: '确定',
                                success(res) {
                                    that.__nav_to__('redirect', '/package_a/pages/order/list/index?status=' + that.data.status_cancel_list);
                                }
                            });
                        }
                    });
                }
            }
        });
    },
    /**
     * 上传图片
     */
    addImg: function(e) {
        if (!this.data.orderInfo) {
            return;
        }

        var that = this;
        var imgCount = that.data.uploaded ? that.data.uploaded.length : 0;
        if (imgCount >= 9) {
            return;
        }

        wx.chooseImage({
            count: 9 - (imgCount > 9 ? 9 : imgCount),
            sizeType: ['compressed'],
            sourceType: ['album', 'camera'],
            success(_res) {
                IndexModel.uploadImg({
                    filePaths: _res.tempFilePaths,
                    url: app.getApi('payment/v1/order/refund/imageupload?id=' + that.data.orderInfo.id)
                }, (res) => {
                    if (!res.imgs) {
                        return;
                    }

                    imgCount = that.data.uploaded ? that.data.uploaded.length : 0;
                    var data = {};
                    for (var index = 0; index < res.imgs.length; index++) {
                        data['uploaded[' + (index + imgCount) + ']'] = res.imgs[index];
                    }

                    that.setData(data);
                });
            }
        });
    },
    delImg(e) {
        var that = this;
        wx.showModal({
            content: '确定删除此图片？',
            success(res) {
                if (res.confirm) {
                    var deleindex = e.currentTarget.dataset.imgindex;
                    var upload = that.data.uploaded;
                    var newUpload = [];
                    if (upload) {
                        for (var index = 0; index < upload.length; index++) {
                            if (index == deleindex) {
                                continue;
                            }
                            newUpload.push(upload[index]);
                        }
                    }

                    that.setData({
                        'uploaded': newUpload
                    });
                }
            }
        });
    }
}))