import {
    Index
} from './index-model.js';
var IndexModel = new Index();

const app = getApp();
const pageS = require('../../../../utils/pageS.js');
Page(Object.assign({}, pageS,{
    data:  {
        apiAssets: app.config.apiAssets,
        status_completed:'wrest-completed',
        isDataLoading:true,
    },
    onReady(refresh=false) {
        var that = this;
        if(that.__disable_refresh_page){
        	return;
        }
    
        var params = {
            id: that._options.id
        };

        app.Util.network.GET(that,{
            url: app.getApi('payment/v1/order/index'),
            params: params,
            showLoading: 0,
            success: function(res) {
                if (!res.orderInfo) {
                    that.__nav_to__('redirect','/pages/account/index');
                    return;
                }
                if (!res.orderInfo.is_can_comment) {
                    that.__nav_to__('redirect', '/pages/account/index');
                    return;
                }

                that.setData({
                    isIphoneX: res.isIphoneX,
                    isDataLoading:false,
                    status_completed: res.status_completed,
                    orderId: that._options.id,
                    orderInfo: res.orderInfo,
                    currency_symbol: res.orderInfo.currency_symbol,
                    products: IndexModel.setInitData(res.orderInfo.items)
                });
            }
        });
    },
    onBacktolast(){
        this.__nav_to__('redirect', '/package_a/pages/order/list/index?status=' + this.data.status_completed);
    },
    //设置星星评价
    setStar: function(e) {
        var that = this;
        var index = e.currentTarget.dataset['index']; //商品索引
        var star = e.currentTarget.dataset['star']; //星评等级
      
        this.setData({
            ["products["+index+"].star"]: star
        });
    },

    //设置文字评价内容
    setContent: function(e) {
        var that = this;
        var index = e.currentTarget.dataset['index']; //商品索引
        var content = e.detail.value; //文字内容
       
        this.setData({
            ["products["+index+"].content"]: content
        });
    },

    /**
     * 上传图片
     */
    addImg: function(e) {
        var that = this;
        if(!that.data.products){
        	return;
        }
        
        var index = e.currentTarget.dataset.index; //商品索引
        var imgCount = that.data.products[index]?that.data.products[index]['img_count']:0;
        var productId = that.data.products[index]['product']['id'];
        if (imgCount >= 9) {
            return;
        }
        that.__disable_refresh_page=true;
        wx.chooseImage({
            count: 9 - (imgCount > 9 ? 9 : imgCount),
            sizeType: ['compressed'],
            sourceType: ['album', 'camera'],
            complete:function(){
            	that.__disable_refresh_page=false;
            },
            success(_res) {
                IndexModel.uploadImg({
                    filePaths: _res.tempFilePaths,
                    url: app.getApi('wp/v1/upload_comment_img?id=' +productId )
                }, (res) => {
                	if(!res.imgs){
                		return;
                	}
                	
                	var index = e.currentTarget.dataset.index; //商品索引
                	var products = that.data.products;
                	var count = products[index]['img_count']+res.imgs.length;
                	var data = {
                		["products["+index+"].img_count"]: count,
                		["products["+index+"].img_txt"]:IndexModel.getImgTxtByImgCount(count)	
                	};
                	
                	var start = products[index]['imgs']?products[index]['imgs'].length:0;
            		for(var v=0;v<res.imgs.length;v++){
            			data["products["+index+"].imgs["+(start+v)+"]"] =res.imgs[v];
            		}
            		that.setData(data);
                });
            }
        });
    },

    /**
     * 删除图片
     */
    delImg: function(e) {
        var that = this;
        var productItemId = e.currentTarget.dataset['productItemId']; //订单商品item的id
        var imgIndex = e.currentTarget.dataset['imgIndex']; //图片在商品中的索引
        var products = that.data.products;
        
        var productIndex = 0;
        for (var i = 0; i < products.length; i++) {
            if (productItemId == products[i]['id']) {
            	productIndex = i;
            	var imgs = products[i]['imgs']?products[i]['imgs']:[];
            	imgs.splice(imgIndex, 1);
            	var img_count = products[i]['img_count']-1;
            	var data ={
            			["products["+productIndex+"].img_count"]:img_count,
                    	["products["+productIndex+"].imgs"] : imgs,
                    	["products["+productIndex+"].img_txt"]:IndexModel.getImgTxtByImgCount(img_count)
                };
            	this.setData(data);
                break;
            }
        }
    },

    //提交
    submitComment: function() {
        var that = this;
        var products = that.data.products;
        var params = {
            orderId: that.data.orderId,
            products: []
        };
        for (let i = 0; i < products.length; i++) {
            let data = {};
            if (products[i]['product']['parent_id']) {
                data['comment_post_ID'] = products[i]['product']['parent_id'];
            } else {
                data['comment_post_ID'] = products[i]['product']['id'];
            }
            data['comment'] = products[i]['content'];
            data['imgs'] = [];
            for (let j = 0; j < products[i]['imgs'].length; j++) {
                data['imgs'].push(products[i]['imgs'][j]['id']);
            }
            data['imgs'] = JSON.stringify(data['imgs']);
            data['comment_parent'] = 0;
            data['rating'] = products[i]['star'];

            params.products.push(data);
        }
        params.products = JSON.stringify(params.products);

        app.Util.network.POST(that,{
            url: app.getApi('payment/v1/order/add_comment'),
            params: params,
            showLoading: 1,
            success: function(res) {
                wx.showModal({
                    content: '评论成功！',
                    showCancel: false,
                    confirmText: '确定',
                    success(res) {
                        that.__nav_to__('redirect', '/package_a/pages/order/list/index?status=' + that.data.status_completed);
                    }
                });
               
            }
        });
    }
}))