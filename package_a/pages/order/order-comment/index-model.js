class Index{
  constructor() {

  }
  //对初始数据赋值
  setInitData(data){
    for (var i = 0; i < data.length; i++) {
      data[i]['star'] = 5;  //加入星评属性默认为5
      data[i]['content'] = ''; //内容
      data[i]['img_count'] = 0; //上传图片数量
      data[i]['img_txt'] = '添加图片'; //上传图片提示文字
      data[i]['imgs'] = []; //上传图片数组
    }
    return data;
  }

  //上传图片处的文字显示
  getImgTxtByImgCount(imgCount){
    var imgTxt = '添加图片';
    switch (imgCount){
      case 1:
        imgTxt = '1 / 9';
        break;
      case 2:
        imgTxt = '2 / 9';
        break;
      case 3:
        imgTxt = '3 / 9';
        break;
      case 4:
        imgTxt = '4 / 9';
        break;
      case 5:
        imgTxt = '5 / 9';
        break;
      case 6:
        imgTxt = '6 / 9';
        break;
      case 7:
        imgTxt = '7 / 9';
        break;
      case 8:
        imgTxt = '8 / 9';
        break;
      case 9:
        imgTxt = '9 / 9';
        break;
    }
    return imgTxt;
  }

  //上传图片
  uploadImg(data, callback) {
    var i = data.i ? data.i : 0; //当前上传的哪张图片
    var res = data.res ? data.res : {
      isOK: true,
      imgs: []
    };

    var that = this;
    var url = data.url; //服务器上传路径
    const app = getApp();
    app.Util.network.PUT(that,{
      url: url,
      filePath: data.filePaths[i],
      name: 'file',
      success(_data) {
        res.isOK = true;
        res.imgs.push({
          id: _data.imgId,
          url: _data.imgUrl
        });

        i++;
        if (i < data.filePaths.length) {
          data.i = i;
          data.res = res;
          that.uploadImg(data, callback); //迭代
        } else {
          callback && callback(res);
        }
      }
    });
  }

}

export { Index };