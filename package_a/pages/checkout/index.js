const app = getApp();
const pageS = require('../../../utils/pageS.js');
const cartApi = require('../../../utils/cart.js');
const checkoutApi = require('../../templates/checkout/index.js');

Page(Object.assign({}, pageS, checkoutApi,{
    _cart_include_checkout: true,
    _cart_include_product: true,

    data: {
        isDataLoading:true,
        apiAssets: app.config.apiAssets,
        showPayDialog:false,
        privacy_policy_checked:false
    },
    onReady: function(refresh=false) {
        if (!app.isAuthorized()) {
            this.__nav_to__('redirect', '/package_c/pages/login/index');
            return;
        }
        var that = this;
        app.getSystemInfo(function(sys){
            that.setData(sys);
            that._load(refresh);
        });
        
    },
    __onShow(){
        var that = this;
        if (that.__stopOnShowEvent__) {
            that.__stopOnShowEvent__ = false;
            return;
        }
        that._load(true);
    },
    _load(refresh){
        var that = this;
        
        that.__order_id__ = null;
        cartApi.get(that, function (data) {
            wx.stopPullDownRefresh();
            var theFirstCartItemId = false;
            for (var pid in data.items) {
                theFirstCartItemId = pid;
            }

            if (!theFirstCartItemId) {
                that.__nav_to__('redirectTo', '/pages/cart/index');
                return;
            }

            that.setData({
                isDataLoading: false,
                theFirstCartItemId: theFirstCartItemId,
                cart: data
            });
        }, refresh);
    },
    onCartChange: function(e) {
        this.setData({
            cart: e.detail
        });
    },
    onPaymentOver(){
        this.__nav_to__('redirectTo', '/package_a/pages/order/detail/index?id=' + this.__order_id__ + '&checkorder=Y');
    },
    onCheckoutSubmit: function(e) {
        var formId = e.detail.formId;
        if (formId && !formId.startsWith('requestFormId:fail')) {
            app.formIds[formId] = Math.round((new Date()).getTime() / 1000);
        }

        var form = e.detail.value;
        form.formIds = JSON.stringify(app.formIds);

        var that = this;
        that.__stopOnShowEvent__ = true;
        app.Util.network.POST(that,{
            url: app.getApi('product/v1/checkout/index'),
            params: form,
            showLoading: 1,
            success: data => {
                that.__processCheckout(data);
            },
            final:function(){
                that.__stopOnShowEvent__ = false;
            }
        });
    }
}));