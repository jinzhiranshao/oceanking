// pages/checkout/coupon/index.js
const app = getApp();
const pageS = require('../../../../utils/pageS.js');
Page(Object.assign({}, pageS,{
    data:{
        isDataLoading:true
    },
    onReady: function (refresh=false) {
        var options = this._options;
        var that = this;
        app.Util.network.GET(that,{
            url: app.getApi('product/v1/product/coupons'),
            showLoading: 0,
            success: function (data) {
                that.setData({
                    isDataLoading:false,
                    coupons:data.items
                });
            }
        });
    },
    onCouponSelect:function(e){
        var code = e.currentTarget.dataset.code;
        var isusing = e.currentTarget.dataset.isusing;
        if (isusing=='yes'){return;}
        
        var that = this;
        app.Util.network.POST(that,{
            url: app.getApi('product/v1/checkout/coupon/add'),
            params: {
                coupon_code:code
            },
            showLoading: 1,
            success: data => {
                that.__go_back__();
            }
        });
    },
    onCouponSubmit:function(e){
        var formId = e.detail.formId;
        if (formId && !formId.startsWith('requestFormId:fail')) {
            app.formIds[formId] = Math.round((new Date()).getTime() / 1000);
        }
        
        var form = e.detail.value;
        if (!form){return;}
        var coupon_code = form.coupon_code;
        if (!coupon_code){return;}

        var that = this;
        app.Util.network.POST(that,{
            url: app.getApi('product/v1/checkout/coupon/add'),
            params: form,
            showLoading:1,
            success: data => {
                that.__go_back__();
            }
        });
    }
}))