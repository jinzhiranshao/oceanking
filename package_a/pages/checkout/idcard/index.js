const app = getApp();
const pageS = require('../../../../utils/pageS.js');
Page(Object.assign({}, pageS, {
    data: {
        isDataLoading: true,
    },
    onReady: function (refresh = false) {
        var options = this._options;
        var that = this;

        that._onLoad('product/v1/wc_sinic/check_idcard/pre', options, refresh, function (that, data) {
            data.isDataLoading = false;
            that.setData(data);
        });
    },
    onIdcardSubmit: function(e) {
        var params = e.detail.value;
        var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/i;
        if ( params.username.length <= 0) {
            wx.showToast({
                title: '请填入姓名!',
                icon: 'none',
                duration: 1500
            });
            return;
        };

        if (!reg.test(params.idcard) ) {
            wx.showToast({
                title: '请填入正确的身份证信息!',
                icon: 'none',
                duration: 1500
            });
            return;
        };

        if (!e.detail.value.terms && this.data.license_page_id){
            wx.showToast({
                title: '请勾选"' + this.data.license_title+'"!',
                icon: 'none',
                duration: 1500
            });
            return;
        }

        var that = this;
        app.Util.network.POST(that, {
            url: app.getApi('product/v1/wc_sinic/check_idcard'),
            params: params,
            showLoading: 0,
            success: function(data) {
                that.__go_back__();
            }
        });
    }
}))