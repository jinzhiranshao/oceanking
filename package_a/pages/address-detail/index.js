const app = getApp();
const pageS = require('../../../utils/pageS.js');
const utilMd5 = require('../../../utils/md5.js');
Page(Object.assign({}, pageS, {
    _isPageCache: true,
    data: {
        isDataLoading: true,
        countryList: [],
        regionList: [
            [],
            [],
            []
        ],
        regions: [0, 0, 0],
        country_index: 20,
        apiAssets: app.config.apiAssets
    },
    __preDataLoadCacheKey(route, request) {
        var cache_key = route;
        cache_key = 'pages:' + utilMd5.hexMD5(cache_key);
        return cache_key;
    },
    onReady: function(refresh = false) {
        var that = this;
        var options = this._options;
        that._onLoad('wp/v1/user/addressConfig', options, refresh, function(that, data) {

            if (data.addressList) {
                var provinces = [];
                var citys = [];
                var districts = [];

                for (var p in data.addressList) {
                    provinces.push(data.addressList[p].title);
                }

                for (var p in data.addressList[0].citys) {
                    citys.push(data.addressList[0].citys[p].title);
                }

                for (var p in data.addressList[0].citys[0].districts) {
                    districts.push(data.addressList[0].citys[0].districts[p].title);
                }

                data.regionList = [provinces, citys, districts];
            }

            data.CNIndex = -1;
            var countryList = [];
            if (data.countries) {
                var index = 0;
                for (var p in data.countries) {
                    if (p === 'CN') {
                        data.CNIndex = index;
                    }
                    countryList.push(data.countries[p]);
                    index++;
                }
            }

            data.countryList = countryList;
            data.isDataLoading = false;

            if (!options.id) {
                that.setData(data);
                return;
            }

            that.setData(data, function() {
                app.Util.network.GET(that, {
                    url: app.getApi('wp/v1/user/address'),
                    params: options,
                    showLoading: 1,
                    success: function(res) {
                        that.__set_country_index(res.address ? res.address.country : null, res);
                        that.__set_regions(res.address ? res.address.region : null, res);

                        that.setData(res);
                    }
                });
            });
        });
    },
    __set_regions(region, res) {
        if (!region || !Array.isArray(region) || region.length != 3 || !this.data.addressList || !this.data.addressList.length) {
            return;
        }

        var addressList = this.data.addressList;
        var regionList = this.data.regionList;
        var regions = [0, 0, 0];
        var citys = [];
        var districts = [];

        for (var pindex in addressList) {
            if (addressList[pindex].title != region[0]) {
                continue;
            }

            regions[0] = pindex;
            for (var p in addressList[pindex].citys) {
                citys.push(addressList[pindex].citys[p].title);
            }

            for (var p in addressList[pindex].citys[0].districts) {
                districts.push(addressList[pindex].citys[0].districts[p].title);
            }

            for (var cindex in addressList[pindex].citys) {
                if (addressList[pindex].citys[cindex].title != region[1]) {
                    continue;
                }

                regions[1] = cindex;
                districts = [];
                for (var p in addressList[pindex].citys[cindex].districts) {
                    districts.push(addressList[pindex].citys[cindex].districts[p].title);
                }

                for (var dindex in addressList[pindex].citys[cindex].districts) {
                    if (addressList[pindex].citys[cindex].districts[dindex].title == region[3]) {
                        regions[3] = dindex;
                    }
                }
            }
        }

        regionList[1] = citys;
        regionList[2] = districts;
        res.regionList = regionList;
        res.regions = regions;
    },
    __set_country_index(country_code, res) {
        if (!country_code || !this.data.countries) {
            return;
        }

        var index = 0;
        for (var p in this.data.countries) {
            if (p == country_code) {
                res.country_index = index;
                return;
            }
            index++;
        }
        return;
    },
    onregionListColumnChange(e) {
        var column = e.detail.column;
        var value = e.detail.value;
        var data = this.data;

        if (column === 0) {
            var provinces = [];
            var citys = [];
            var districts = [];

            for (var p in data.addressList) {
                provinces.push(data.addressList[p].title);
            }

            for (var p in data.addressList[value].citys) {
                citys.push(data.addressList[value].citys[p].title);
            }

            for (var p in data.addressList[value].citys[0].districts) {
                districts.push(data.addressList[value].citys[0].districts[p].title);
            }

            this.setData({
                regions: [value, 0, 0],
                regionList: [provinces, citys, districts]
            });
        } else if (column == 1) {
            var provinces = [];
            var citys = [];
            var districts = [];

            var province_index = this.data.regions[0];

            for (var p in data.addressList) {
                provinces.push(data.addressList[p].title);
            }

            for (var p in data.addressList[province_index].citys) {
                citys.push(data.addressList[province_index].citys[p].title);
            }

            for (var p in data.addressList[province_index].citys[value].districts) {
                districts.push(data.addressList[province_index].citys[value].districts[p].title);
            }

            this.setData({
                regions: [province_index, value, 0],
                regionList: [provinces, citys, districts]
            });
        }
    },
    onCountrySelect: function(e) {
        this.setData({
            country_index: e.detail.value
        });
    },
    onregionListChange: function(e) {
        this.setData({
            regions: e.detail.value
        });
    },
    onBindWechatAddress: function() {
        var that = this;
        wx.chooseAddress({
            success: function(res) {
                if (res.errMsg != 'chooseAddress:ok') {
                    wx.showToast({
                        title: '您拒绝了授权或网络异常！',
                        icon: 'none'
                    });
                    return;
                }

                var data = {
                    'address.province': res.provinceName,
                    'address.city': res.cityName,
                    'address.county': res.countyName,
                    'address.country': 'CN',
                    'address.full_country': '中国',
                    'address.detailAddress': res.detailInfo,
                    'address.region[0]': res.provinceName,
                    'address.region[1]': res.cityName,
                    'address.region[2]': res.countyName,
                    'address.name': res.userName,
                    'address.phone': res.telNumber,
                    'address.postcode': res.postalCode
                };

                that.__set_country_index('CN', data);
                that.__set_regions([res.provinceName, res.cityName, res.countyName], data);
                that.setData(data);
            },
            fail: function(res) {
                wx.showToast({
                    title: '您拒绝了授权或网络异常！',
                    icon: 'none'
                });
            }
        })
    },
    onAddressSave: function(e) {
        var that = this;
        var formId = e.detail.formId;
        if (formId && !formId.startsWith('requestFormId:fail')) {
            app.formIds[formId] = Math.round((new Date()).getTime() / 1000);
        }

        var form = e.detail.value;
        form.id = typeof this._options.id == 'undefined' ? 0 : this._options.id;
        form.v = '1.0.1';

        app.Util.network.POST(that, {
            url: app.getApi('product/v1/checkout/address/save'),
            params: form,
            showLoading: 1,
            success: data => {
                that.__go_back__();
            }
        });
    }
}))