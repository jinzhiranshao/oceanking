const app = getApp();
const pageS = require('../../../utils/pageS.js');
Page( Object.assign({}, pageS, {
    data:  {
        apiAssets: app.config.apiAssets,
        items: {},
        current_status_index: 0
    },
    onReady: function (refresh = false) {
        if (!app.isAuthorized()) {
            this.__nav_to__('redirect', '/package_c/pages/login/index');
            return;
        }

        this._load(refresh);
    },
    onPullDownRefresh: function () {
        this._load(true);
    },
    _load: function (refresh) {
        this.__get_list({
            pageIndex: 1
        });
    },
    __get_list: function (params, callback = null) {
        var that = this;
        if (that.data.isDataLoading) {
            return;
        }

        that.setData({
            pageIndex: params.pageIndex,
            isDataLoading: true,
            isNoneData: false
        });

        app.Util.network.GET(that,{
            url: app.getApi('product/v1/product/wl'),
            params: params,
            showLoading: 0,
            complete: function () {
                if (that.data.isDataLoading) {
                    that.setData({
                        isDataLoading: false
                    });
                }
            },
            success: function (res) {
                if (res.page_index == 1) {
                    that.setData({
                        isDataLoading: false,
                        proList: res.items,
                        modal: res.modal,
                        isLoadDataAll: res.page_index >= res.page_count,
                        isNoneData: res.page_index==1&& res.total_count == 0
                    }, callback);
                } else {
                    var data = {
                        isDataLoading: false,
                        modal: res.modal,
                        isLoadDataAll: res.page_index >= res.page_count,
                        isNoneData: res.page_index == 1 && res.total_count == 0
                    };
                    if (res.items) {
                        var start = that.data.proList ? that.data.proList.length : 0
                        for (var index = 0; index < res.items.length; index++) {
                            data['proList[' + (start + index) + ']'] = res.items[index];
                        }
                        that.setData(data, callback);
                    }
                }

            }
        });
    },
    onReachBottom: function () {
        var pageIndex = this.data.pageIndex;
        if (!this.data.isLoadDataAll) {
            this.__get_list({
                pageIndex: pageIndex + 1
            });
        }
    }
}))