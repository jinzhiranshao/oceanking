@import "/templates/cart-items/index.wxss";

.page-container {
    display: flex;
    flex-direction: column;
    justify-content: center;
}

.order-status-content {
    display: flex;
    flex-direction: column;
    justify-content: center;
    background-color: #dafce4;
}

.order-status-header {
    width: 750rpx;
    height: 200rpx;
    align-items: center;
    display: flex;
    flex-direction: column;
    justify-content: center;
}

.order-status-title {
    display: flex;
    flex-direction: row;
    justify-content: center;
    font-size: 36rpx;
    line-height: 36rpx;
}

.order-status-image {
    width: 35rpx;
    height: 35rpx;
    margin-right: 10rpx;
}

.order-status-subtitle {
    display: flex;
    flex-direction: row;
    color: #1c2438;
    justify-content: center;
    font-size: 28rpx;
    margin-top: 10rpx;
}

.order-status-footer {
    text-align: left;
    padding: 15rpx 30rpx;
    font-size: 24rpx;
}

/*--------------pending------------------*/

.pending .order-status-header {
    background-color: #fdf6e8;
}

.pending .order-status-title {
    color: #df9f1e;
}

.pending .order-status-footer {
    border-bottom: solid 1rpx #f3f3f3;
    border-top: solid 1rpx #f3f3f3;
    text-align: center;
    background: #fff;
}

/***************processing***************/

.processing .order-status-title {
    color: #5ac51c;
}

.processing .order-status-footer {
    color: #8aae31;
}

/***************completed***************/

.completed .order-status-header {
    background-color: #afeca8;
}

.completed .order-status-title {
    color: #2b7423;
}

.completed .order-status-footer {
    color: #2b7423;
}

/***************cancelled***************/

.cancelled .order-status-header {
    background-color: #ffffe0;
}

.cancelled .order-status-title {
    color: #ee9a00;
}

.cancelled .order-status-footer {
    color: #ee9a00;
}

/***************refunded***************/

.refunded .order-status-header {
    background-color: #fdf6e8;
}

.refunded .order-status-title {
    color: #df9f1e;
}

.refunded .order-status-footer {
    background-color: #fdf6e8;
}

/***************failed***************/

.failed .order-status-header {
    background-color: #ffc1c1;
}

.failed .order-status-title {
    color: #ee2c2c;
}

.failed .order-status-footer {
    color: #ee2c2c;
}

/***************on-hold***************/

.on-hold .order-status-header {
    background-color: #afeeee;
}

.on-hold .order-status-title {
    color: #2e8b57;
}

.on-hold .order-status-footer {
    color: #2e8b57;
}

/****************address**************/

.address-content {
    padding: 40rpx 30rpx;
    background-color: #fff;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
}

.address-content .address-header {
    border-bottom: solid 1rpx #f3f3f3;
    padding-bottom: 30rpx;
}

.address-inner {
    padding-top: 20rpx;
}

.address-title {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
}

.address-subtitle {
    line-height: 34rpx;
    letter-spacing: 0.4rpx;
    display: -webkit-box;
    overflow: hidden;
    text-overflow: ellipsis;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
}

/*-------------items-content-----------------*/

.items-content {
    margin-top: 30rpx;
    background-color: #fff;
}

.items-title {
    padding: 30rpx;
    border-bottom: dashed 1rpx #f3f3f3;
}

.multi-ellipsis {
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
}

.cart-subtotal {
    padding: 30rpx;
    border-bottom: 1rpx solid #f3f3f3;
}

.cart-total {
    padding: 30rpx;
}

.cart-subtotal-line {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
}

.item-r-money {
    margin: 10rpx 0;
}

/*------------order-meta------------*/

.order-meta {
    background-color: #fff;
    margin-top: 30rpx;
    padding: 30rpx;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
}

.order-meta-value {
    max-width: 400rpx;
    line-height: 40rpx;
    display: flex;
}

/*-----------order-info----------*/

.order-info {
    margin-top: 30rpx;
    padding: 30rpx;
    background-color: #fff;
    display: flex;
    flex-direction: column;
    justify-content: center;
}

.order-info-item {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
}

.order-info-item text {
    margin: 10rpx 0rpx;
}

.order-footer {
    position: fixed;
    right: 0;
    bottom: 0;
    left: 0;
    background: #fff;
    z-index: 10;
    border-top: 1rpx solid #eee;
    text-align: right;
    height: 120rpx;
    padding: 30rpx;
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
}

.order-footer .btn {
    height: 60rpx;
    display: inline-block;
    font-size: 28rpx;
    width: 200rpx;
    line-height: 60rpx;
    margin-right: 20rpx;
    border: none;
    border-radius: 4rpx;
    outline: 0;
    box-sizing: border-box;
    letter-spacing: 2rpx;
}

.order-footer .btn-cancel {
    color: #222;
    background-color: #fff;
}

.order-footer .btn-pay {
    text-align: center;
    color: #fff;
}

/*--------------customer-note----------------*/

.customer-note {
    margin-top: 30rpx;
    background-color: #fff;
    display: flex;
    flex-direction: column;
}

/*物流弹窗*/

.sku-cover {
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.3);
    z-index: 100;
}

.skuModal {
    position: fixed;
    width: 100%;
    left: 0;
    bottom: 0;
    z-index: 101;
    background-color: #fff;
    transform: translate3d(0, 130%, 0);
}

.skuModal .prop-header {
    position: relative;
}

.skuModal .prop-header .icon-cha {
    position: absolute;
    top: 30rpx;
    right: 30rpx;
    width: 40rpx;
    height: 40rpx;
    font-size: 40rpx;
}

.skuModal .prop-header .info {
    position: absolute;
    top: 30rpx;
    left: 30rpx;
    display: flex;
    flex-direction: column;
    font-size: 24rpx;
    color: #3d3d3d;
}

.skuModal .prop-mainer-wrapper {
    position: relative;
    top: 120rpx;
    height: 900rpx;
}

.skuModal .prop-mainer {
    margin-bottom: 30rpx;
}

.skuModal .prop-mainer-wrapper .item {
    display: flex;
    border-bottom: solid 2rpx #f4f4f4;
    padding: 30rpx;
}

.skuModal .prop-mainer-wrapper .item .date {
    width: 240rpx;
}

.skuModal .prop-mainer-wrapper .item .detail {
    width: 440rpx;
    margin-left: 20rpx;
    word-wrap: break-word;
}


pro-img {
  border:none!important;
}

pro-img image {
  min-height: initial!important
}