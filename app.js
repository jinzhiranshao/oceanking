				/**
				 * Project: Wordpress小程序
				 * Author: hoter@xunhuweb.com
				 * Version: v1.0.8				 * Date :2019-11-18 14:12				 * Copyright: xunhuweb (https://www.wpweixin.net)
				 */
						const extendList = [];
				
		
		App({
		    Util: require('utils/util.js'),
		    formIds:{},
			//已废弃
			data:{ __config__:{}},
			tmpCart:{},
		    config:{
		        sdk:'1.0.4',
		        version:'v1.0.8',
			    apiMain:'https://oceanking.com.au/wp-json',
			    apiAssets:'https://oceanking.com.au/wp-content/plugins/wp-rest-api/assets',
		        theme:{"navigationBar_TextStyle":"black","theme_size":30,"theme_line_height":1.5,"theme_fontsize_sub":24,"theme_c_main":"#353535","theme_panel":"#ffffff"},
		    	menus:{"pages/index/index":[],"pages/category/index":[],"pages/cart/index":[],"pages/account/index":[]},
		    	icons:{"icon_none_comment_items":{"url":"https://oceanking.com.au/wp-content/plugins/wp-rest-api/assets/images/v2/product/empty-comment.png","width":360,"height":477},"icon_none_cat_icon":{"url":"https://oceanking.com.au/wp-content/uploads/woocommerce-placeholder-247x247.png","width":80,"height":80},"icon_none_cart_items":{"url":"https://oceanking.com.au/wp-content/plugins/wp-rest-api/assets/images/v2/product/cartsky.png","width":360,"height":477},"icon_is_sale_out":{"url":"https://oceanking.com.au/wp-content/plugins/wp-rest-api/assets/images/icon/sale-out.png","width":256,"height":256},"icon_is_on_sale":{"url":"https://oceanking.com.au/wp-content/plugins/wp-rest-api/assets/images/icon/is-promotion.png","width":74,"height":74},"icon_is_featured":{"url":"https://oceanking.com.au/wp-content/plugins/wp-rest-api/assets/images/icon/is-feature.png","width":74,"height":74},"icon_is_new":{"url":"https://oceanking.com.au/wp-content/plugins/wp-rest-api/assets/images/icon/is-new.png","width":74,"height":74},"icon_cart_more":{"width":128,"height":50,"url":"https://oceanking.com.au/wp-content/plugins/wp-rest-api/assets/images/icon/cart-more.png"},"icon_cart_add":{"width":50,"height":50,"url":"https://oceanking.com.au/wp-content/plugins/wp-rest-api/assets/images/icon/cart-add.png"},"icon_cart_cut":{"width":50,"height":50,"url":"https://oceanking.com.au/wp-content/plugins/wp-rest-api/assets/images/icon/cart-cut.png"},"icon_status_header_bg":{"url":"https://oceanking.com.au/wp-content/plugins/wp-rest-api/assets/images/v2/icon/order/status-bg.jpg","width":1024,"height":120}}		    },
		    apply_filters(action) {
		        if (arguments.length < 2) {
		            throw 'apply filters need argument 2!'
		        }
		        var args = Array.prototype.slice.call(arguments, 1);
		        for (var index in extendList) {
		            var handler = extendList[index];
		            if(!handler.filters){continue;}
                    if (typeof handler.filters[action] != 'function') {
		                continue;
		            }

                    args[0] = handler.filters[action].apply(handler, args);
		        }
		        return args[0];
		    },
		    do_action(action) {
		     	var args = Array.prototype.slice.call(arguments, 1);
		        for (var index in extendList) {
		            var handler = extendList[index];
		            if(!handler.actions){continue;}
                    if (typeof handler.actions[action] != 'function') {
		                continue;
		            }

                    handler.actions[action].apply(handler, args);
		        }
		    },
		    getApi:function(route){
		    	if (!route){
                    return this.config.apiMain;
                }
                route = route.toString();
                if (route[0]!='/'){
                    route = "/" +route;
                 }
                return this.config.apiMain + route;
		    },
        
		    logout:function(){
		    	this.TOKEN=false;
		    	wx.removeStorageSync('system:token');
          },
          // check version
          checkUpdateVersion: function (temp) {
            console.log("check update");
            //判断微信版本是否 兼容小程序更新机制API的使用
            if (wx.canIUse('getUpdateManager')) {
              //创建 UpdateManager 实例
              console.log("test");
              const updateManager = wx.getUpdateManager();
              //检测版本更新
              console.log("update:", updateManager);
              updateManager.onUpdateReady(function () {
                //TODO 新的版本已经下载好，调用 applyUpdate 应用新版本并重启 （ 此处进行了自动更新操作）
                wx.showModal({
                  title: "更新提示",
                  content: "新版本已经准备好，请重启应用",
                  success: function (res) {
                    updateManager.applyUpdate();
                  }
                })
              })
              updateManager.onUpdateFailed(function () {
                // 新版本下载失败
                wx.showModal({
                  title: '已经有新版本喽~',
                  content: '请您删除当前小程序，到微信 “发现-小程序” 页，重新搜索打开哦~',
                })
              })

            } else {
              //TODO 此时微信版本太低（一般而言版本都是支持的）
              wx.showModal({
                title: '溫馨提示',
                content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
              })
            }
        },
		    isAuthorized: function() {
		        if (typeof this.TOKEN == 'undefined') {
		            try {
		                var cached = wx.getStorageSync('system:token');
		                this.TOKEN = cached ? cached : false;
		            } catch (e) {
		                this.TOKEN = false;
		                //ignore
		            }
		        }

		        return this.TOKEN;
		    },
            onLaunch(){
                this.checkUpdateVersion();
                wx.getStorageInfo({
                    success: function(res) {
                        if (res.currentSize / res.limitSize>=0.8){
                            wx.clearStorage();
                        }
                    },
                })
            },
			__temp_sys__:false,
		    getSystemInfo: function(onSuccess) {
		        var that = this;
		        if (that.__temp_sys__) {
    		        if (onSuccess) {
                        var pages = getCurrentPages();
                        var obj = Object.assign({}, that.__temp_sys__);
                        var isTab = pages && pages.length > 0 && that.config.menus && typeof that.config.menus[pages[pages.length-1].route]!='undefined';
                        obj.isTab = isTab;
                        obj.isIphoneX = obj.isIphoneX && !isTab;
                        onSuccess(obj);
                    }
		            return;
		        }
		        
				wx.getSystemInfo({
	                success: function(res) {
                 		var isIOS = false;
		                if (res.platform && res.platform.toLowerCase() == 'ios') {
		                    isIOS = true;
		                } else if (res.platform && res.platform.toLowerCase() == 'devtools') {
		                    isIOS = res.system && res.system.toLowerCase().indexOf('ios') > -1;
		                }
		                
                        var statusBarHeight = res.statusBarHeight ? res.statusBarHeight : 20;
                        var navbarHeight = isIOS ? 40 : 48;
		                that.__temp_sys__ = {
		                	isIOS: isIOS,
		                    windowHeight: res.windowHeight,
		                    windowWidth: res.windowWidth,
		                    screenHeight: res.screenHeight,
                    		screenWidth: res.screenWidth,
		                    statusBarHeight: statusBarHeight,
		                    isIphoneX: res.model && res.model.search('iPhone X') != -1,
		                    navbarHeight: navbarHeight,
		                    //v7.0.0以上版本，才支持 自定义navbar
		                    navHeight:res.version >= '7.0.0'?(statusBarHeight+navbarHeight):0
		                };
		                if (onSuccess) {
		                    var pages = getCurrentPages();
	                        var obj = Object.assign({}, that.__temp_sys__);
	                        var isTab = pages && pages.length > 0 && that.config.menus && typeof that.config.menus[pages[pages.length-1].route]!='undefined';
	                        obj.isTab = isTab;
	                        obj.isIphoneX = obj.isIphoneX && !isTab;
	                        onSuccess(obj);
		                }
	                },
	            });
		    }
		})
		