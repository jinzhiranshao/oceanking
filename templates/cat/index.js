const app = getApp();
const pageT = require('../../utils/pageT.js');

module.exports =Object.assign({}, pageT, {
    id:null,
    onReady(refresh = false) {
        if (this.id) {
            this._options.id = this.id;
        }
        this._onLoad('wp/v1/store/category3',  refresh);
    },
    prePageLoad:function(res){
        if (!res.obj){
            return;
        }
        wx.setNavigationBarTitle({
            // title: res.obj.title,
        });
    },
    preApiLoad(opts) {
        Object.assign(opts, {
            cat_id: this._options.id
        });
    }
});