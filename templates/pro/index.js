const app = getApp();
const pageS = require('../../utils/pageS.js');
const cartApi = require('../../utils/cart.js');
const navShow = require('../../components/nav/nav-show.js');

module.exports = Object.assign({}, pageS, {
    _isPageCache:false,
    _selectedMenuAni: null,
    id: null,
    navShow: true,
    __current_menu_index__: 0,
    api: require('../../utils/apis/pro-list.js'),
    doms: [],
    data: {
        menus: app.apply_filters('product_detail_tabs', ['商品描述', '商品规格', '购物须知', '更多推荐']) ,
        current_menu_index: 0,
        apiAssets: app.config.apiAssets
    },
    onReady(refresh = true) {
        var that = this;
        if (this.id) {
            this._options.id = this.id;
        }

        if (!this._options.id) {
            this.__nav_to__('redirect', '/pages/index/index');
            return;
        }

        this._selectedMenuAni = wx.createAnimation({
            duration: 250,
            transformOrigin: "50% 50%",
            timingFunction: 'ease'
        });

        if (that.data.password) {
            that.__loadProductInfo(that.data.password);
            return;
        }

        wx.getStorage({
            key: 'post:' + that._options.id + ':pwd',
            success: function(res) {
                if (res && res.data) {
                    that.__loadProductInfo(res.data);
                }
            },
            fail: function() {
                that.__loadProductInfo();
            }
        });
    },
    __loadProductInfo: function(password = null) {
        var that = this;
        that._options.password = password;
        that._onLoad('wp/v1/store/product/detail', that._options, true, function(that, data) {
            if (password && data.productInfo) {
                wx.setStorage({
                    key: 'post:' + data.productInfo.id + ':pwd',
                    data: password
                });
            }

            // if (data.productInfo && data.productInfo.comment && data.productInfo.comment.totalCount > 0) {
            //     data['menus[2]'] = '评论 (' + data.productInfo.comment.totalCount + ')';
            // }

            data.navHeight = that.navShow ? data.navHeight : 0;
            that.setData(data, function() {
                that.api.property = 'footer';
                that.api.prepareLoad = function(options) {
                    options.sameSellsPid = data.productInfo.id;
                };

                that.api.onLoad(that);
                var callGetContentHeight = function() {
                    console.log('-----inventory:callGetContentHeight-----');
                    wx.createSelectorQuery().selectAll('.scroll-view-item').boundingClientRect().exec(function(res) {
                        if (res && res.length > 0 && res[0]) {
                            for (var i in res[0]) {
                                var dom = res[0][i];
                                if (!dom.dataset.index) {
                                    continue;
                                }
                                that.doms[dom.dataset.index] = dom.height;
                            }
                        }

                        that.doms['total'] = 0;
                        if (that.data.menus) {
                            for (var index = 0; index < that.data.menus.length; index++) {
                                if (!that.doms[index]) {
                                    that.doms[index] = 0;
                                }
                                that.doms['total'] += that.doms[index];
                            }
                        }
                    });
                };

                callGetContentHeight();
                //如果加载的是富文本内容，那么需要循环监听内容高度。
                if (data.productInfo && !(data.productInfo.content_images && data.productInfo.content_images.length)) {
                    if (that.__tmp_keep_nav_height_interval__) {
                        clearInterval(that.__tmp_keep_nav_height_interval__);
                    }
                    that.__tmp_keep_nav_height_interval_time__ = 0;
                    that.__tmp_keep_nav_height_interval__ = setInterval(function() {
                        callGetContentHeight();
                        if (that.__tmp_keep_nav_height_interval_time__++ >= 3 && that.__tmp_keep_nav_height_interval__) {
                            clearInterval(that.__tmp_keep_nav_height_interval__);
                            that.__tmp_keep_nav_height_interval__ = null;
                        }
                    }, 3000);
                }
            });
        });

    },
    onUnload() {
        var that = this;
        if (that.__tmp_keep_nav_height_interval__) {
            clearInterval(that.__tmp_keep_nav_height_interval__);
            that.__tmp_keep_nav_height_interval__ = null;
        }
    },
    onMenuSelected(e) {
        var index = e.currentTarget.dataset.index;
        var that = this;

        this.__setMenuActive(index, function() {
            var height = that.doms['header'] - that.data.navHeight;
            for (var p = 0; p <= (index - 1); p++) {
                height += that.doms[p];
            }

            that.__tmp_page_scrolling__ = true;
            wx.pageScrollTo({
                scrollTop: height,
                complete() {
                    setTimeout(function() {
                        that.__tmp_page_scrolling__ = false;
                    }, 200);
                },
                duration: 0
            });
        });
    },
    __onPageScroll(e) {
        var that = this;
        if (that.navShow) {
            navShow.onPageScroll(that, e.scrollTop);
        }
        if (!this.data.menus || this.__tmp_page_scrolling__) {
            return;
        }

        var height = this.doms['header'] - this.data.navHeight;
        var oHeight = 0;
        for (var index = this.data.menus.length - 1; index >= 0; index--) {
            if (!this.doms[index]) {
                continue;
            }

            oHeight += this.doms[index];

            if (e.scrollTop >= (this.doms['total'] - oHeight + height)) {
                this.__setMenuActive(index);
                return;
            }
        }

        this.__setMenuActive(0);
    },
    __setMenuActive(index, callback = null) {
        if (this.__current_menu_index__ == index) {
            return;
        }

        this._selectedMenuAni.translateX(index * 187 * this.data.windowWidth / 750).step();

        this.__current_menu_index__ = index;
        this.setData({
            selectedMenuAni: this._selectedMenuAni.export(),
            current_menu_index: index,
            current_menu_name: this.data.menus[index]
        }, callback);

    },
    onShareAppMessage: function(res) {
        if (this.data.productInfo) {
            return {
                title: this.data.productInfo.title
            };
        }
    },
    onPostSubmit: function(e) {
        var that = this;
        this.setData({
            password: e.detail.value && e.detail.value.password ? e.detail.value.password : ''
        }, function() {
            wx.startPullDownRefresh({});
        });
    },
    onCouponGet: function(e) {
        var params = {
            id: e.currentTarget.dataset.id,
            index: e.currentTarget.dataset.index
        };
        var that = this;
        app.Util.network.POST(that, {
            url: app.getApi('product/v1/product/addCoupon'),
            params: params,
            showLoading: 1,
            success: function(data) {
                that.setData({
                    ['productInfo.coupons[' + params.index + '].isIncluded']: data.isIncluded
                });
            }
        });
    },
    onTabChanged: function(e) {
        var tbindex = e.detail.current;
        this.setData({
            topTabIndex: tbindex,
            childViewId: "scroll-view-item-" + tbindex
        });

    },
    showSliderImg: function(e) {
        if (!this.data.productInfo || !this.data.productInfo.content_images) {
            return;
        }
        var r = e.currentTarget.dataset.idx;
        var urls = [];
        for (var index in this.data.productInfo.content_images) {
            urls.push(this.data.productInfo.content_images[index].full.url);
        }
        wx.previewImage({
            current: urls[r],
            urls: urls
        });
    },
    onReachBottom() {
        this.api.onReachBottom(this);
    }
})