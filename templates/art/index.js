const app = getApp();
const pageS = require('../../utils/pageS.js');
const comment = require('../comment/comment.js');

module.exports =Object.assign({}, comment, pageS, {
    _isPageCache: false,
    data: Object.assign({}, comment.data, {
        apiAssets: app.config.apiAssets
    }),
    id:null,
    onReady: function (refresh=false) {
        if (this.id) {
            this._options.id = this.id;
        }

        var id = this._options.id;
        var that = this;

        if (!id) {
            wx.navigateTo({
                url: '/pages/index/index',
            })
            return;
        }

        if (that.data.password) {
            var params = {
                id: id,
                password: that.data.password
            }
            that.getArtPdsById(params);
            return;
        }

        wx.getStorage({
            key: 'post:' + id + ':pwd',
            success: function (res) {
                if (res && res.data) {
                    var params = {
                        id: id,
                        password: res.data
                    }
                    that.getArtPdsById(params);
                }
            },
            fail: function () {
                var params = {
                    id: id
                }
                that.getArtPdsById(params);
            }
        });
    },

    onShareAppMessage: function (res) {
        return {
            title: this.data.article.title,
            imageUrl: this.data.article.image ? this.data.article.image.url : ''
        };
    },
    /**
     * 获取文章和相关产品
     */
    getArtPdsById: function (params) {
        var that = this;
        app.Util.network.GET(that,{
            url: app.getApi('wp/v1/article/art_pds'),
            params: params,
            showLoading: 0,
            success: function (data) {
                that.setData(data);

                that.getCommentInfo();

                if (params.password) {
                    wx.setStorage({
                        key: 'post:' + params.id + ':pwd',
                        data: params.password
                    });
                }
            }
        });
    },
    onPostSubmit: function (e) {
        var that = this;
        this.setData({
            password: e.detail.value && e.detail.value.password ? e.detail.value.password : ''
        }, function () {
            wx.startPullDownRefresh({ });
        });
    }
})