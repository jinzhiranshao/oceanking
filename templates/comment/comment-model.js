class Comment {
  constructor() {

  }

  /**
   * 上传图片处的文字显示
   * 0张：添加图片，
   */
  getImgUploadTxtByImgCount(imgCount) {
    var imgTxt = '添加图片';
    switch (imgCount) {
      case 1:
        imgTxt = '1/9';
        break;
      case 2:
        imgTxt = '2/9';
        break;
      case 3:
        imgTxt = '3/9';
        break;
      case 4:
        imgTxt = '4/9';
        break;
      case 5:
        imgTxt = '5/9';
        break;
      case 6:
        imgTxt = '6/9';
        break;
      case 7:
        imgTxt = '7/9';
        break;
      case 8:
        imgTxt = '8/9';
        break;
      case 9:
        imgTxt = '9/9';
        break;
    }
    return imgTxt;
  }

  /**
   * 上传图片，迭代上传
   * 传入的data.filePaths是一个包含图片地址的数组
   */
  uploadImg(data, callback) {
    var i = data.i ? data.i : 0; //当前上传的哪张图片
    var res = data.res ? data.res : {
      isOK: true,
      imgs: []
    }; 

    var that = this;
    var url = data.url; //服务器上传路径
    const app = getApp();
      app.Util.network.PUT(that,{
          url: url,
          filePath: data.filePaths[i],
          name: 'file',
          success(_data) {
              res.isOK=true;
              res.imgs.push({
                  id: _data.imgId,
                  url: _data.imgUrl
              });
             
              i++;
              if (i < data.filePaths.length) {
                  data.i = i;
                  data.res = res;
                  that.uploadImg(data, callback); //迭代
              } else {
                  callback && callback(res);
              }
          }
      });
  }
}

//输出Comment
export {
  Comment
};