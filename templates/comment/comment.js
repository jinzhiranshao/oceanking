import {
    Comment
} from 'comment-model.js';

var CommentModel = new Comment();

const app = getApp();
const pageS = require('../../utils/pageS.js');
var config = function() {
    return {
        _isLoadedFirst:false,
        data:  {
            page: 1,
            isLoadAll: false,
            commentInfo: {
                placeholdertList:[0,1,2,3,4],
                isLoading: false,
                loadingTxt: '',
                editEnterTxt: '写评论...',
                id: null,
                title: null,
                path: null,
                commentCount: 0,
                commentList: null
            },
            gotoNode: null,
            commentNode: null,
            editInfo: {
                isEdit: false,
                showImgs: false,
                imgCount: 0,
                imgIdArr: [],
                imgUrlArr: [],
                imgUploadTxt: '添加图片',
                placeholder: '内容',
                content: '',
                isPass: false
            },
            replyInfo: {
                showReply: false,
                mainComment: null,
                replyList: []
            },
            parent: 0
        },

        /**
         * 获取评论信息
         */
        getCommentInfo: function() {
            this.__loadComments(1);
        },

        /**
         * 上拉加载更多
         */
        onReachBottom: function() {
            this.__loadComments(this.data.commentInfo.page + 1);
        },

        __loadComments: function(page) {
            var that = this;
            //首先判断是否加载所有
            if (page != 1 && that.data.commentInfo.isLoadAll) return;

            var res = {
                'commentInfo.isLoading': true,
                'commentInfo.isLoadedFirst':false
            };
            if (!that._isLoadedFirst){
                that._isLoadedFirst = true;
                res['commentInfo.isLoadedFirst'] = true;
            }

            //可以加载更多
            that.setData(res);

            var params = {
                id: that.data.article.id,
                password: that.data.article.password,
                page: page
            };

            app.Util.network.GET(that,{
                url: app.getApi('wp/v1/comments'),
                params: params,
                showLoading: 0,
                success: function(res) {
                    var isLoadAll = false;
                    var isLoading = false;
                    var loadingTxt = '';

                    if (res.pageIndex >= res.totalPage) {
                        isLoadAll = true;
                    }

                    var data = {
                        'commentInfo.page': params.page,

                        'commentInfo.isIphoneX': res.isIphoneX,
                        'commentInfo.isLoadAll': isLoadAll,
                        'commentInfo.commentCount': res.totalCount,
                        'commentInfo.isLoading': isLoading,
                        'commentInfo.loadingTxt': loadingTxt,
                    };

                    var commentList = that.data.commentInfo.commentList ? that.data.commentInfo.commentList:[];
                    var start = commentList ? commentList.length : 0;

                    if (page == 1) {
                        data["commentInfo.commentList"] = res.items;
                        if ((!res.items || !res.items.length) && that.data.article.post_type == 'product') {
                            data["editInfo.isEdit"] = true;
                            data["editInfo.placeholder"] = '';
                            data["editInfo.parent"] = 0;
                        }

                    } else {
                        var index = 0;
                        if (res.items) {
                            for (var p in res.items) {
                                data["commentInfo.commentList[" + (index++ + start) + "]"] = res.items[p];
                            }
                        }
                    }

                    that.setData(data);
                }
            });
        },

        /**
         * 查询指定元素到顶部的距离
         */
        queryMultipleNodes: function(callback = null) {
            var that = this;
            if (that.data.commentNode) {
                callback();
                return;
            }
            var query = wx.createSelectorQuery();
            query.select('#comment-node').boundingClientRect();
            query.selectViewport().scrollOffset();
            query.exec(function(res) {
                if (res && res.length > 0 && res[0]) {
                    that.setData({
                        gotoNode: res[0].top,
                        commentNode: res[0].top
                    }, function() {
                        if (callback) {
                            callback();
                        }
                    });
                }
            })
        },

        /**
         * 跳转到顶部/评论
         */
        gotoComment: function() {
            var that = this;


            this.queryMultipleNodes(function() {
                var gotoNode = that.data.gotoNode;

                wx.pageScrollTo({
                    scrollTop: gotoNode,
                    duration: 0
                });
                if (gotoNode) gotoNode = 0;
                else gotoNode = that.data.commentNode;

                that.setData({
                    gotoNode: gotoNode
                });
            });

        },

        /**
         * 编辑评论
         */
        editComment: function(e) {
            var that = this;
            var parent = 0;
            var replyInfo = that.data.replyInfo;
            if (replyInfo.showReply) parent = replyInfo.mainComment.id; //进入查看回复
            if (e.currentTarget.dataset.parent) parent = e.currentTarget.dataset.parent; //查看回复下面的评论回复
            var placeholder = '';
            // if (replyInfo.showReply) placeholder = '回复 ' + replyInfo.mainComment.author + ' :'; //进入查看回复
            //if (e.currentTarget.dataset.placeholder) placeholder = e.currentTarget.dataset.placeholder; //查看回复下面的评论回复 
            if (!app.isAuthorized()) {
                wx.navigateTo({
                    url: '/package_c/pages/login/index',
                });
                return;
            }
            that.setData({
                'editInfo.isEdit': true,
                'editInfo.placeholder': placeholder,
                'editInfo.parent': parent
            });
        },

        /**
         * 显示上传图片
         * 是先判断是否已经存在上传的图片
         * 如果有上传图片，则不能隐藏上传图皮纳列表
         */
        showOrHideImgs: function() {
            var that = this;
            var imgCount = that.data.editInfo.imgCount;
            if (imgCount) return;
            var showImgs = !that.data.editInfo.showImgs;
            that.setData({
                'editInfo.showImgs': showImgs,
                'editInfo.imgUploadTxt': CommentModel.getImgUploadTxtByImgCount(imgCount)
            });
        },

        /**
         * 上传图片
         */
        uploadImgs: function() {
            var that = this;
            // return 0 even images uploaded
            // var imgCount = that.data.editInfo.imgCount;
            var imgCount = that.data.editInfo.imgIdArr.length;
            that.__disable_refresh_page = true;
            console.log('jy', that.data.editInfo);
            wx.chooseImage({
                count: 6 - (imgCount > 6 ? 6 : imgCount),
                sizeType: ['compressed'],
                sourceType: ['album', 'camera'],
                complete: function() {
                    that.__disable_refresh_page = false;
                },
                success(_res) {
                    var data = {
                        filePaths: _res.tempFilePaths,
                        url: app.getApi('wp/v1/upload_comment_img?id=' + that.data.article.id)
                    };

                    CommentModel.uploadImg(data, (res) => {

                        var imgIdArr = [];
                        var imgUrlArr = [];
                        var data = {
                            'editInfo.imgCount': imgCount,
                            'editInfo.imgUploadTxt': CommentModel.getImgUploadTxtByImgCount(that.data.editInfo.imgCount + imgUrlArr.length)
                        };

                        var start = that.data.editInfo.imgIdArr ? that.data.editInfo.imgIdArr.length : 0;
                        if (res.imgs) {
                            for (var index = 0; index < res.imgs.length; index++) {
                                var img = res.imgs[index];
                                data["editInfo.imgIdArr[" + (start + index) + "]"] = img.id;
                                data["editInfo.imgUrlArr[" + (start + index) + "]"] = img.url;
                            }
                        }

                        that.setData(data);
                        //处理是否通过
                        that.handleIsPass();
                    });
                }
            });
        },

        /**
         * 预览评论图片
         */
        previewCommentImage: function(e) {
            var that = this;
            var type = e.currentTarget.dataset.type;
            var imgUrl = e.currentTarget.dataset.imgUrl;
            var imgUrlArr = [];
            switch (type) {
                case 'level-0':
                    var id = e.currentTarget.dataset.commentId;
                    var commentList = that.data.commentInfo.commentList;
                    if (!commentList) { commentList=[];}
                    for (let i = 0; i < commentList.length; i++) {
                        if (id == commentList[i]['id']) {
                            imgUrlArr = commentList[i]['imgs'];
                            break;
                        }
                    }
                    break;
                case 'reply-main':
                    imgUrlArr = that.data.replyInfo.mainComment.imgs;
                    break;
                case 'reply-list':
                    var id = e.currentTarget.dataset.commentId;
                    var replyList = that.data.replyInfo.replyList;
                    for (let i = 0; i < replyList.length; i++) {
                        if (id == replyList[i]['id']) {
                            imgUrlArr = replyList[i]['imgs'];
                            break;
                        }
                    }
                    break;
            }
            wx.previewImage({
                current: imgUrl,
                urls: imgUrlArr
            });
            return;
        },

        /**
         * 预览图片
         */
        previewImage: function(e) {
            var that = this;
            var imgIndex = e.currentTarget.dataset.imgIndex;
            var imgUrlArr = that.data.editInfo.imgUrlArr;
            var currentImgUrl = imgUrlArr[imgIndex];
            wx.previewImage({
                current: currentImgUrl,
                urls: imgUrlArr
            });
        },
        /**
         * 删除上传图片
         */
        delCommentImg: function(e) {
            var that = this;
            var imgIndex = e.currentTarget.dataset.imgIndex;
            var imgCount = ((that.data.editInfo.imgCount) - 1);
            imgCount = imgCount >= 0 ? imgCount : 0;
            var imgIdArr = that.data.editInfo.imgIdArr;
            var imgUrlArr = that.data.editInfo.imgUrlArr;
            imgIdArr.splice(imgIndex, 1);
            imgUrlArr.splice(imgIndex, 1);
            // if (imgCount) {
            //   var imgIdArr = that.data.editInfo.imgIdArr;
            //   var imgUrlArr = that.data.editInfo.imgUrlArr;
            //   imgIdArr = imgIdArr.splice(imgIndex,1);
            //   imgUrlArr = imgUrlArr.splice(imgIndex,1);
            // } else {
            //   imgIdArr = [];
            //   imgUrlArr = [];
            // }
            var imgUploadTxt = CommentModel.getImgUploadTxtByImgCount(imgCount);
            that.setData({
                'editInfo.imgCount': imgCount,
                'editInfo.imgIdArr': imgIdArr,
                'editInfo.imgUrlArr': imgUrlArr,
                'editInfo.imgUploadTxt': imgUploadTxt
            });
            //处理是否通过
            that.handleIsPass();
        },

        /**
         * 关闭编辑框
         */
        closeEdit: function() {
            var that = this;
            this.setData({
                'editInfo.showImgs': false,
                'editInfo.isEdit': false,
                'editInfo.imgCount': 0,
                'editInfo.imgUploadTxt': '',
                'editInfo.imgIdArr': [],
                'editInfo.imgUrlArr': []
            });
        },

        /**
         * 设置评论的文字内容
         */
        setContent: function(e) {
            var that = this;
            var content = e.detail.value;
            that.setData({
                'editInfo.content': content
            });
            //处理是否通过
            that.handleIsPass();
        },

        /**
         * 发布评论
         */
        publishComment: function(e) {
            var formId = e.detail.formId;
            if (formId && !formId.startsWith('requestFormId:fail')) {
                app.formIds[formId] = Math.round((new Date()).getTime() / 1000);
            }

            var that = this;
            var editInfo = that.data.editInfo;
            if (!editInfo.isPass) return;
            var id = that.data.article.id;
            var params = {
                comment_post_ID: id,
                comment: editInfo.content,
                imgs: JSON.stringify(editInfo.imgIdArr),
                comment_parent: that.data.editInfo.parent,
                password: that.data.article.password,
                formIds: JSON.stringify(app.formIds)
            };

            app.Util.network.POST(that,{
                url: app.getApi('wp/v1/add_comment'),
                params: params,
                showLoading: 1,
                success: function(data) {
                    that.handleComment(data);
                    that.closeEdit();
                }
            });
        },

        /**
         * 处理提交的评论
         */
        handleComment: function(data) {
            var that = this;
            //判断是否进入到评论回复页面
            var replyInfo = that.data.replyInfo;
            if (replyInfo.showReply) {
                if (that.data.editInfo.parent == replyInfo.mainComment.id) {
                    this.__loadCommentReply(1);
                }
            } else {
                this.__loadComments(1);
            }
            return;
        },

        /**
         * 判断是否存在代发布的内容
         * 图片/文字，二取一
         */
        handleIsPass: function() {
            var that = this;
            var editInfo = that.data.editInfo;
            var isPass = false;
            if (editInfo.imgCount || editInfo.content.length >= 1) isPass = true;
            that.setData({
                'editInfo.isPass': isPass
            });
        },

        /**
         * 查看回复
         */
        viewReply: function(e) {
            var that = this;
            var type = e.currentTarget.dataset.type;
            var commentIndex = e.currentTarget.dataset.commentIndex;
            var list = [];
            if (type == 'level-0') {
                list = that.data.commentInfo.commentList ? that.data.commentInfo.commentList:[];
            } else if (type == 'reply-list') {
                list = that.data.replyInfo.replyList;
            }

            var mainComment = list[commentIndex];

            app.getSystemInfo(function(res) {
                // 创建一个动画实例
                var animation = wx.createAnimation({
                    // 动画持续时间
                    duration: 150,
                    // 定义动画效果，当前是匀速
                    timingFunction: 'ease',
                    delay: 0
                });
                // 先在y轴偏移，然后用step()完成一个动画
                var dpx = 750 / res.windowWidth;
                that._modalHeight = 200 * dpx;
                animation.translateY(that._modalHeight).step();

                // 用setData改变当前动画
                var page = 1;
                var data = {
                    'replyInfo.skuAnimation': animation.export(),
                    'replyInfo.page': page,
                    'commentInfo.editEnterTxt': '回复' + mainComment.author,
                    'replyInfo.showReply': true,
                    'replyInfo.isCommentLoadAll': false,
                    'replyInfo.commentReplayLoading': true,
                    'replyInfo.mainComment': mainComment,
                    'replyInfo.parent': mainComment.id
                };

                that.setData(data);

                // 设置setTimeout来改变y轴偏移量，实现有感觉的滑动
                setTimeout(function() {
                    animation.translateY(0).step()
                    that.setData({
                        'replyInfo.skuAnimation': animation.export()
                    });
                }, 150);

                that.__loadCommentReply(1);
            });


        },
        __loadCommentReply(page) {
            var that = this;
            if (page != 1 && that.data.replyInfo.isCommentLoadAll) {
                return;
            }

            var data = {
                'replyInfo.page': page,
                'replyInfo.commentReplayLoading': true
            };

            that.setData(data);

            app.Util.network.GET(that,{
                url: app.getApi('wp/v1/comments_child'),
                params: {
                    id: that.data.replyInfo.parent,
                    password: that.data.article.password,
                    page: page
                },
                showLoading: 0,
                success: function(res) {
                    var data = {
                        'replyInfo.commentReplayLoading': false,
                        'replyInfo.isCommentLoadAll': res.pageIndex >= res.totalPage
                    };

                    if (page == 1) {
                        data["replyInfo.replyList"] = res.items;
                    } else {
                        var start = that.data.replyInfo.replyList ? that.data.replyInfo.replyList.length : 0;
                        if (res.items) {
                            for (var index = 0; index < res.items.length; index++) {
                                data["replyInfo.replyList[" + (start + index) + "]"] = res.items[index];
                            }
                        }
                    }



                    that.setData(data);
                },
                complete: function() {
                    if (that.data.replyInfo.commentReplayLoading) {
                        that.setData({
                            'replyInfo.commentReplayLoading': false
                        });
                    }
                }
            });
        },
        onCommentReplayLoadEnd: function() {
            var that = this;
            var page = that.data.replyInfo.page;
            if (page < 1) {
                page = 1;
            }

            page++;
            this.__loadCommentReply(page);

        },
        /**
         * 关闭回复
         */
        closeReply: function() {
            var that = this;
            var animation = wx.createAnimation({
                duration: 150,
                timingFunction: 'ease',
                delay: 0
            })

            animation.translateY(that._modalHeight).step();
            that.setData({
                'editInfo.showImgs': false,
                'replyInfo.skuAnimation': animation.export(),
                'commentInfo.editEnterTxt': '写评论...',
                'replyInfo.replyList': null,
                'editInfo.isEdit': false
            });
            setTimeout(function() {
                animation.translateY(0).step();
                that.setData({
                    'replyInfo.skuAnimation': animation.export(),
                    'replyInfo.showReply': false,
                })
            }, 150);
        },
        /**
         * 删除评论
         */
        delComment: function(e) {
            var that = this;
            var commentId = e.currentTarget.dataset.commentId;
            var params = {
                id: commentId
            };
            wx.showModal({
                title: '提示',
                content: '确认删除这条评论？',
                success(res) {
                    if (res.confirm) {
                        app.Util.network.POST(that,{
                            url: app.getApi('wp/v1/del_comment'),
                            params: params,
                            showLoading: 0,
                            success: function(data) {
                                if (data.res) {
                                    that.setData({
                                        'replyInfo.showReply': false,
                                    });
                                    wx.startPullDownRefresh({});
                                }
                            }
                        });
                    }
                }
            });
        }
    };
};
module.exports = config();