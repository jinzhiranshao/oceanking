const app = getApp();
const pageT = require('../../utils/pageT.js');

module.exports = Object.assign({}, pageT, {
    id:null,
    onReady( refresh = false) {
        var that = this;
        if (this.id) {
            this._options.id = this.id;
        }
        this._onLoad('wp/v1/store/page',  refresh);
    },
    prePageLoad:function(res){
        wx.setNavigationBarTitle({
            title: res.page_title,
        });
    },
    preApiLoad(opts) {
      let pageIDAndCategoryId = this._options.id;
      if (pageIDAndCategoryId.includes('/')) {
        let index = pageIDAndCategoryId.indexOf('/');
        console.log('jy',index);
        let categoryID = pageIDAndCategoryId.substring(++index);
        console.log('jy', categoryID);
        Object.assign(opts, {
          cat_id: categoryID
        });
      }
    }
});
