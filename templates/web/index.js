const app = getApp();
const pageS = require('../../utils/pageS.js');
module.exports =Object.assign({}, pageS,  {
    url:null,
	onReady(refresh=true) {
        var url = this.url;
        var that = this;
        if (!url){
            url = this._options.id ? this._options.id : this._options.url;
        }
        
        if (!url){
            this.__go_back_to_home__();
            return;
        }

        if (!url.startsWith('http://') &&!url.startsWith('https://')){
            url = decodeURIComponent(url);
        }

        if(url.indexOf('?')>=0){
            url += '&--http-wrest-token--='+app.TOKEN;
        }else{
            url += '?--http-wrest-token--='+app.TOKEN;
        }

        this.setData({
            url: url
        });
    }
});  