const app = getApp();
const pageS = require('../../../../utils/pageS.js');
Page(Object.assign({}, pageS,  {
    _isPageCache: false,
    data: {
        isDataLoading: true,
        apiAssets: app.config.apiAssets
    },
    onPullDownRefresh: function () {
        this._load();
    },
    __onShow() {
        if (!app.isAuthorized()) {
            this.__nav_to__('redirect', '/package_c/pages/login/index');
            return;
        }
        this._load();
    },
    _load: function () {
        var options = this._options;
        var that = this;
        that._onLoad('wp/v1/user/shoplist', options, true, function (that, data) {
            data.isDataLoading = false;
            that.setData(data);
        });
    },
    onConfirm(e) {
        var id = e.currentTarget.dataset.id;
        var that = this;
        app.Util.network.POST(that, {
            url: app.getApi('wp/v1/user/confirmShop'),
            params: {
                id: id
            },
            showLoading: 1,
            success: data => {
                that.__go_back__();
            }
        });
    }
}))