const app = getApp();
const pageS = require('../../../../utils/pageS.js');
const cartApi = require('../../../../utils/cart.js');

Page(Object.assign({}, pageS, {
    navShow: true,
    data: {
        apiAssets: app.config.apiAssets
    },
    onReady(refresh = true) {
        var that = this;
        that._onLoad('wp/v1/user/shopDetail', that._options, refresh, function (that, data) {          
            that.setData(data);
        });
    },
    __onPageScroll(e) {
        var that = this;
        if (that.navShow) {
            that.__onNavbarChange__(e.scrollTop, function (ani) {
                that.setData({
                    navbarShowAni: ani
                });
            });
        }
    },
    __onNavbarChange__(scrollTop, callback, refresh = false) {
        var that = this;

        if (that.__tmp_is_navbar_show__ && scrollTop >= 200) {
            return;
        }

        app.getSystemInfo(function (sys) {
            if (scrollTop >= (sys.navbarHeight + sys.statusBarHeight)) {
                if (that.__tmp_is_navbar_show__ && !refresh) {
                    return;
                }
                that.__tmp_is_navbar_show__ = true;
                callback(true);
            } else {
                if (!that.__tmp_is_navbar_show__ && !refresh) {
                    return;
                }

                that.__tmp_is_navbar_show__ = false;
                callback(false);
            }
        });
    }
}))